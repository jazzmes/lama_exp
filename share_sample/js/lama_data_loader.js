/**
 * Created by tiago on 6/14/16.
 */

function LamaDataLoader(server, args) {
    args = args  || {};
    this.maxPoints = args.maxPoints || 100;
    this.server = args.server; // || "10.1.1.31";
    this.port = args.port || "8400";
    this.charts = {};
    this.paused = true;
    this.timeout = null;
    this.minMaxTs = null;
}

LamaDataLoader.prototype.allData = [
    "response_time",
    "cpu",
    "disk",
    "interface",
    "virt_cpu_total",
    "memory",
    "disk_octets",
    "if_octets",
    "virt_vcpu",
    "disk_ops",
    "if_packets",
    "if_dropped",
    "if_errors"
];

LamaDataLoader.prototype.getMonitorData = function(callbacks, min_ts) {
    // get minimum timestamp
    var params = {
        min_ts: this.minMaxTs || min_ts || -1,
        max_points: this.maxPoints
    };

    console.log("get request with ts =", params.min_ts);
    $.get("http://" + this.server + ":" + this.port + "/metrics", params,
            function(data, status, jqxhr){
//                console.log("Success:Data: ", jqxhr.responseText);
                console.log("Success:Data Size", jqxhr.responseText.length);
                this.minMaxTs = -1;
                if (callbacks.hasOwnProperty('success')) {
                    callbacks.success(data);
                }
            }.bind(this),
            "json"
    ).fail(
        function(jqXHR, textStatus, errorThrown){
            if (callbacks.hasOwnProperty('fail')) {
                callbacks.fail(jqXHR, textStatus, errorThrown);
            }
        }.bind(this)
    );
    console.log("started request");
};

LamaDataLoader.prototype.updateMinMaxTs = function(ts) {
//    console.log("minMax = ", this.minMaxTs, ", ts = ", ts);
    this.minMaxTs = ts && (!this.minMaxTs || this.minMaxTs < 0 || ts < this.minMaxTs) ? ts : this.minMaxTs;
//    console.log("minMax = ", this.minMaxTs);
};