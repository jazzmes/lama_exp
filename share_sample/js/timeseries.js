/**
 * Created by tiago on 7/15/16.
 */


function TimeSeries(container, conditions, options, live) {

    this.container = container;
    this.conditions = conditions || [];
    this.extra_options = options;
    this.dataRefreshInterval = 10;
    this.startTime =(new Date()).getTime();
    this.active = true;
    this.countFailures = 0;
    this.maxConsecutiveFailures = 3;

    this.seriesMap = {};
    if (options.hasOwnProperty('series')) {
        for (var i = 0; i < options.series.length; i++){
            this.seriesMap[options.series[i].id || options.series[i].name] = i;
        }
    }

    this.lastIndices = {};

    this.live = live || false;
    this.chart = null;
    this.ySteps = 4;

    this.rejectedSeries = {};
}


TimeSeries.prototype.getObjectByName = function(arr, value) {

    for (var i=0, iLen=arr.length; i<iLen; i++) {
        if (arr[i].name == value) return arr[i];
    }
};

TimeSeries.prototype.addSeries = function(id, data, name, yAxisIndex) {
    if (!id) {
        console.error("No id specified");
        return;
    }
    data = data || [null];
    name = name || id;
    yAxisIndex = yAxisIndex || 0;
    this.seriesMap[id] = this.chart.series.length;
    console.log("add series", id, data, name);
    this.chart.addSeries({
        name: name,
        data: data,
        id: id,
        yAxis: yAxisIndex
    });
};

TimeSeries.prototype.updateYAxis = function(ymin, ymax) {
    if (this.chart) {
        // console.log("ymin, ymax", ymin, ymax);
        // console.log("ymin, ymax", this.chart.yAxis[0].min, this.chart.yAxis[0].max);
        this.chart.yAxis[0].setExtremes(
            ymin || this.chart.yAxis[0].min,
            ymax || this.chart.yAxis[0].max
        );
        this.chart.yAxis[0].isDirty = true;
        this.chart.redraw();
    }
};

TimeSeries.prototype.checkConditions = function(name, conditions) {
    conditions = conditions || this.conditions;
    console.log("Check " + name + " with conditions", conditions);
    if (conditions instanceof Array) {
        for (var j = 0; j < conditions.length; j++) {
            var index = this.checkConditions(name, conditions[j]);
            if (index != null) {
                return j;
            }
        }
        return null;
    } else {
        for (var condition in conditions) {
            if (conditions.hasOwnProperty(condition)) {
                console.log('check', name, condition, conditions[condition]);
                if ((name.search(condition) >= 0) != conditions[condition]) {
                    console.log("nope");
                    return null;
                }
            }
        }
        // console.log("yeah");
        return 0;
    }
};

TimeSeries.prototype.addData = function(data) {
    this.data = data;
};

TimeSeries.prototype.getSeries = function() {
    if (this.data) {
        var series = [];

        if (this.extra_options.hasOwnProperty('series')) {
            series = this.extra_options.series;
        }

        if (data.hasOwnProperty('series')) {
            var seriesName;
            for (seriesName in data.series) {
                if (data.series.hasOwnProperty(seriesName)) {
                    var conditionIndex = this.checkConditions(seriesName);
                    console.log("Return of conditions", conditionIndex);
                    if (conditionIndex != null) {
                        console.log("Series", seriesName, "accepted with index", conditionIndex);
                        var s;
                        if (this.seriesMap.hasOwnProperty(seriesName)) {
                            s = series[this.seriesMap[seriesName]];
                        } else {
                            this.seriesMap[seriesName] = this.seriesMap.length;
                            s = {
                                name: seriesName,
                                data: [],
                                yAxis: conditionIndex
                            };
                            series.push(s);
                        }
                        var j, dp;
                        for (j = 0; j < data.series[seriesName].length; j++) {
                            dp = data.series[seriesName][j];
                            s.data.push(dp)
                        }
                    }
                }
            }
        }
        if (data.hasOwnProperty('events')) {
            data.events.sort(function(a, b){
                if (a.x < b.x) return -1;
                if (a.x == b.x) return 0;
                return 1
            });

            for (var i = 0; i < data.events.length; i++) {
                var ev = data.events[i];
                var seriesIndex = this.seriesMap[ev.name];
                var s;
                if (seriesIndex) {
                    s = series[seriesIndex];
                } else {
                    this.seriesMap[ev.name] = this.seriesMap.length;
                    s = {
                        name: ev.name,
                        data: []
                    };
                    series.push(s);
                }
                s.data.push(ev);
            }
        }
        return series;
    }
    return null;
};

TimeSeries.prototype.loadData = function() {
    if (!this.chart) {
        return;
    }

    var chartSeries = this.chart.series;

    $.ajax({
        type: 'POST',
        url: 'http://127.0.0.1:40900/get',
        dataType: 'json',
        data: JSON.stringify(this.lastIndices),
        success: function(data, textStatus, jqXHR){
            var seriesName;
            for (seriesName in this.seriesMap) {
                if (this.seriesMap.hasOwnProperty(seriesName)) {
                    var seriesIndex = this.seriesMap[seriesName];
                    if (data.series.hasOwnProperty(seriesName)) {
                        var j, dp;
                        for (j = 0; j < data.series[seriesName].length; j++) {
                            dp = data.series[seriesName][j];
                            chartSeries[seriesIndex].addPoint(dp);
                        }
                    } else {
                        if (chartSeries[seriesIndex].type != 'flags') {
                            dp = [(new Date()).getTime(), 0];
                            chartSeries[seriesIndex].addPoint(dp);
                        }
                    }
                }
            }

            for (seriesName in data.series) {
                if (data.series.hasOwnProperty(seriesName)) {
                    if (!this.seriesMap.hasOwnProperty(seriesName) && !this.rejectedSeries.hasOwnProperty(seriesName)) {
                        var conditionIndex = this.checkConditions(seriesName);
                        console.log("Return of conditions", conditionIndex);
                        if (conditionIndex != null) {
                            console.log("Series", seriesName, "accepted with index", conditionIndex);
                            this.addSeries(seriesName, data.series[seriesName], seriesName, conditionIndex);
                        } else {
                            this.rejectedSeries[seriesName] = true;
                        }
                    }
                }
            }

            if (data.hasOwnProperty('events')) {
                for (var i = 0; i < data.events.length; i++) {
                    var ev = data.events[i];
                    var s = this.getObjectByName(this.chart.series, ev.name);
                    if (s) {
                        s.addPoint(ev, false);
                    }
                }
            }

            if (data.hasOwnProperty('indices')) {
                this.lastIndices = data.indices;
            }
            this.chart.redraw();
        }.bind(this),
        error: function(jqXHR, textStatus, errorThrown) {
            this.countFailures += 1;
            console.log("Increased count_failures", this.countFailures);
        }.bind(this)
    });
};

TimeSeries.prototype.getData = function(){
    // set up the updating of the chart each second
    this.loadData();
    setInterval(function () {
        if (this.countFailures < this.maxConsecutiveFailures) {
            this.loadData();
        }
    }.bind(this), this.dataRefreshInterval * 1000);
};

TimeSeries.prototype.initializeData = function () {
    // generate an array of random data
    var data = [], time = (new Date()).getTime(), i;

    var min_value = -999 * this.dataRefreshInterval;
    for (i = min_value; i <= 0; i += this.dataRefreshInterval) {
        data.push([
            time + i * 1000,
            0
        ]);
    }
    //console.log(data[0]);
    return data;
};

TimeSeries.prototype.computeTicks = function(min, max){
    min = Math.max(Math.floor(min), 0);
    max = Math.max(Math.ceil(max), 0);
    var interval = Math.round((max - min) / 4);

    var act = min, ticks = [];
    while (act < max && ticks.length < 5) {
        ticks.push(Math.round(act));
        act += interval;
    }
    ticks.push(act);
    return ticks;
};

TimeSeries.prototype.start = function(){

    // data = get initital data

    Highcharts.setOptions({
        global : {
            useUTC : false
        }
    });

    // Create the chart
    var properties = {
        chart: {
            zoomType: 'xy'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            ordinal: false
        },
        yAxis: [],
        rangeSelector: {
            buttons: [{
                count: 1,
                type: 'minute',
                text: '1M'
            }, {
                count: 5,
                type: 'minute',
                text: '5M'
            }, {
                type: 'all',
                text: 'All'
            }],
            inputEnabled: false,
            selected: 2
        },
        // series: [
        //     {
        //         name: 'main',
        //         data: this.initializeData()
        //     }
        // ],
        exporting: {
            enabled: true
        },
        legend: {
            // layout: 'horizontal',
            // align: 'center',
            // verticalAlign: 'bottom',
            // floating: true,
            // borderWidth: 0,
            // y: -25
        }
    };

    if (this.live) {
        properties['chart']['events'] = {
            load: this.getData.bind(this)
        };
    } else {
        this.getSeries();
    }

    // add tick positioner
    // console.log(properties);
    // console.log($.extend(true, {}, properties, this.extra_options));
    var extended_properties = $.extend(true, {}, properties, this.extra_options);
    if (extended_properties.hasOwnProperty('yAxis')) {
        if (extended_properties.yAxis instanceof Array) {
            for (var k = 0; k < extended_properties.yAxis.length; k++) {
                extended_properties.yAxis[k].tickPositioner = this.computeTicks;
            }
        } else {
            extended_properties.yAxis.tickPositioner = this.computeTicks;
        }
    } else {
        extended_properties.yAxis = {
            tickPositioner: this.computeTicks
        }
    }

    $(this.container).highcharts('StockChart', extended_properties);
    this.chart = $(this.container).highcharts();

};
