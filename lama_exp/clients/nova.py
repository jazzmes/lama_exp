import sys
import logging
import traceback
from openstack.connection import Connection
from openstack import exceptions as os_exceptions
from keystoneauth1 import exceptions as ks_exceptions
from openstack import utils
from time import sleep


__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

# utils.enable_logging(True, stream=sys.stdout)
logger = logging.getLogger('lama_exp')


class CreateInstanceException(Exception):
    pass


class OSClient(object):

    def __init__(self, username, password, auth_url='http://controller:5000/v3', user_domain_id='default'):
        # auth_args = lama_auth_args
        self.conn = Connection(
            username=username,
            password=password,
            auth_url=auth_url,
            user_domain_id=user_domain_id
        )
        # conn.authorize()

    def create_instance(self, name, flavor, image, network, keypair):
        # TODO] make sure you catch relevant errors
        try:
            server = self.conn.compute.find_server(name)
            if server:
                raise CreateInstanceException("Server name exists: %s" % server.name)

            f = self.conn.compute.find_flavor(flavor)
            if not f:
                raise CreateInstanceException("Flavor does not exist: %s" % flavor)

            i = self.conn.compute.find_image(image)
            if not i:
                raise CreateInstanceException("Image does not exist: %s" % image)

            n = self.conn.network.find_network(network)
            if not n:
                raise CreateInstanceException("Network does not exist: %s" % network)

            k = self.conn.compute.find_keypair(keypair)
            if not k:
                raise CreateInstanceException("Key pair does not exist: %s" % keypair)

            server = self.conn.compute.create_server(
                name=name,
                flavor_id=f,
                image_id=i,
                networks=[{"uuid": n.id}],
                key_name=k
            )
            return server
        except os_exceptions.HttpException as e:
            logger.warning("e=%s, cause=%s, details=%s", e, e.cause, e.details)
            raise CreateInstanceException(str(e))
        except ks_exceptions.http.BadRequest as e:
            logger.warning("e=%s, msg=%s, resp=%s, details=%s", e, e.message, e.response, e.details)
            raise CreateInstanceException(str(e))
        except Exception as e:
            logger.error("%s", traceback.format_exc())
            logger.error("SHOULD CATCH THIS EXCEPTION: %s", e)
            raise CreateInstanceException(str(e))

    def list_instances(self):
        return self.conn.compute.servers()

    def delete_instance(self, server):
        return self.conn.compute.delete_server(server)

# for server in conn.compute.servers():
#     print(server.name)
#
# server = conn.compute.find_server("test")
# print(server)
#
# flavor = conn.compute.find_flavor('m1.tiny')
# print(flavor)
#
# image = conn.compute.find_image('cirros')
# print(image)
#
# network = conn.network.find_network('private')
# print(network)
#
# keypair = conn.compute.find_keypair('LAMA default')
# print(keypair)
#
# # 'nova --debug boot ' \
# # '--flavor m1.tiny --image cirros --nic net-id= --security-group default --key-name "LAMA default" lama-pub-0001'
#
# sleep(5)
# server = conn.compute.create_server(
#     **{
#         "name": "test2",
#         "flavor_id": flavor,
#         "image_id": image,
#         "networks": [{"uuid": network.id}],
#         "key_name": keypair
#         # "nic": [{'net-id': '6f4e1d04-1b40-4f51-b9b8-b7b2fc417a80'}],
#         # "key-name": "LAMA default"
#
#     }
# )
# print(server)
# for flavor in conn.compute.flavors():
#     print(flavor)


if __name__ == "__main__":
    try:
        server = OSClient(username='lama', password='fmG-qd4-ykT-rEp').create_instance(
            name='test3',
            flavor='m1.tiny',
            image='cirros',
            network='private',
            keypair='LAMA default'
        )
        print("New server: %s" % server)
    except CreateInstanceException as e:
        print("Failed to create: %s" % e)
