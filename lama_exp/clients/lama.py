import traceback
import logging

import requests

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.2'
__email__ = 'tcarvalho@cmu.edu'


logger = logging.getLogger(__name__)


class Agent(object):

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def url(self):
        return "http://%s:%s" % (self.ip, self.port)

    def request(self, options):
        url = self.url()
        response = None
        try:
            logger.debug("Posting to %s options=%s", url, options)
            response = requests.post(url, json=options)
            logger.debug("Raw response: %s", response)
            response = response.json()
        except requests.ConnectionError:
            logger.error("Error connecting to server: %s", traceback.format_exc())
        except:
            logger.error("Unexpected exception: %s", traceback.format_exc())
        return response

    def __str__(self):
        return "%s:%s" % (self.ip, self.port)


class NoProviderYet(Exception):
    pass


class LamaClient(object):

    def __init__(self, app_name, host='10.1.1.12', dispatcher_port=20001, experiment_port=21000, app_type='cirros-single'):
        logger.debug("At LAMA Client")
        self._app_name = app_name
        self._app_created = None

        self._dispatcher = Agent(host, dispatcher_port)
        self._experiment_server = Agent(host, experiment_port)
        self._provider = None

        self._app_type = app_type
        self._final_app_name = app_name

        self._check_status()

    def get_app_provider(self):
        response = self._dispatcher.request(
            {
                'action': 'app_info',
                'app_name': self._app_name
            }
        )
        logger.debug("response: %s", response)
        self._provider = Agent(
            ip=response.get('app', {}).get('provider'),
            port=response.get('app', {}).get('provider_port')
        )
        logger.info("Provider: %s", self._provider)
        return self._provider

    def get_instance_info(self, instance_name):
        if not self._provider:
            self.get_app_provider()

        response = self._provider.request({
            'action': 'instance_info',
            'app_name': self._app_name,
            'instance_name': instance_name
        })
        logger.info("Instance info: %s", response)
        return response.get('msg')

    def get_app_info(self):
        options = {
            'action': 'app_info',
            'app_name': self._app_name
        }
        response = self._dispatcher.request(options)
        logger.info("App info: %s", response)
        if response and response.get('result'):
            self._app_created = True
            if response.get('app'):
                self._provider = Agent(
                    ip=response.get('app', {}).get('provider'),
                    port=response.get('app', {}).get('provider_port')
                )

        return response

    def _check_status(self):
        options = {
            'action': 'check-status',
            'appname': self._app_name
        }
        response = self._experiment_server.request(options)
        logger.info("Check status response: %s", response)
        if response and response.get('result'):
            self._app_created = True
        else:
            self._app_created = False

        if self._app_created and not self._provider:
            self.get_app_info()

    def create_app(self, flavor=None, make_unique=False, config=None):
        if config is None:
            config = {}
        if self._app_created:
            logger.info("App already created: name=%s", self._app_name)
            return {'result': False, 'app_name': self._app_name, 'msg': 'Duplicate app'}

        logger.info("Create app: name=%s type=%s", self._app_name, self._app_type)
        options = {
            'action': 'start-app',
            'appname': self._app_name,
            'apptype': self._app_type,
            'make_unique': make_unique,
            'config': config
        }
        if flavor:
            options['flavor'] = flavor
        response = self._experiment_server.request(options)
        if response and response.get('result'):
            self._app_created = True
            self._final_app_name = response['appname']

        return response

    def create_instance(self, service_name='cirros', flavor=None):
        if not self._app_created:
            self.create_app(flavor=flavor)

        if not self._provider:
            self.get_app_provider()

        instance = {}
        response = self._provider.request({
            'action': 'add_instance',
            'app_name': self._app_name,
            'service_name': service_name
        })
        logger.info("Create instance response: %s", response)
        instance['name'] = response['msg']
        return instance

    def launch_client(self, config):
        if not self._provider:
            raise NoProviderYet()

        d = {
            'action': 'change_workload',
            'app_name': self._app_name,
            'kwargs': {
                'config': config
            }
        }
        logger.debug("Dict for request to launch instance: %s", d)
        response = self._provider.request(d)
        logger.info("Launch instance response: %s", response)
        return response


def main():
    logger.setLevel(logging.DEBUG)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(
        logging.Formatter("%(asctime)s : %(levelname)5.5s : %(module)14.14s [%(lineno)3.3s] : %(message)s")
    )
    logger.addHandler(stream_handler)

    lc = LamaClient('test')
    # lc.get_app_info()
    logger.info("Create 1st instance")
    lc.create_instance(flavor='m1.tiny')
    logger.info("Create 2nd instance")
    lc.create_instance(flavor='m1.tiny')


if __name__ == '__main__':
    main()

