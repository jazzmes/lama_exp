import traceback
import logging

import requests

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.2'
__email__ = 'tcarvalho@cmu.edu'


logger = logging.getLogger(__name__)

Port = 58229


class Remote(object):

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    @property
    def base_url(self):
        return "http://%s:%s" % (self.ip, self.port)

    def exec(self, url, **options):
        url = self.base_url + url
        response = None
        try:
            logger.debug("Posting to %s options=%s", url, options)
            response = requests.post(url, json=options)
            logger.debug("Raw response: %s", response)
            response = response.json()
        except requests.ConnectionError:
            logger.error("Error connecting to server: %s", traceback.format_exc())
        except:
            logger.error("Unexpected exception: %s", traceback.format_exc())
        return response

    def __str__(self):
        return "%s:%s" % (self.ip, self.port)


class ThanatosClient(object):

    @staticmethod
    def generate_instance_crash(ip, instance_name, duration=0):
        r = Remote(ip, Port)
        resp = r.exec('/fail/instance/{}'.format(instance_name), duration=duration)
        return resp

    @staticmethod
    def generate_host_disk_failure(ip, duration, **options):
            r = Remote(ip, Port)
            logger.debug("options: %s", options)
            resp = r.exec('/fail/host/disk', duration=duration, **options)
            return resp

    @staticmethod
    def generate_host_crash(ip, duration=0):
        r = Remote(ip, Port)
        resp = r.exec('/fail/host', duration=duration)
        return resp
