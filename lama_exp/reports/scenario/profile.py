from csv import DictReader
from json import encoder
from datetime import datetime
from argparse import ArgumentParser
import json

from lama_exp.reports.scenario.base import ScenarioReport
from lama_exp.utils.arg import ArgTypes


__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

encoder.FLOAT_REPR = lambda o: format(o, '.2f')


class ProfileReport(ScenarioReport):

    EventTypeTitles = {
        'ExperimentStart': 'S',
        'ExperimentEnd': 'E',
        'InstanceActive': 'IA',
        'AppAPIRequest': 'AR'
    }

    EventTypeCategories = {
        'ExperimentStart': 'Experiment',
        'ExperimentEnd': 'Experiment',
        'InstanceActive': 'Instance',
        'AppAPIRequest': 'App'
    }

    CategoriesConfig = {
        'Experiment': {'shape': 'circlepin'},
        'Instance': {'shape': 'squarepin'},
        'App': {'shape': 'squarepin'}
    }

    def __init__(self, name, prefix, results_folder='results', **kwargs):
        super().__init__(name, prefix, results_folder, **kwargs)

    def generate_timeline(self, value=None):
        file_type = 'events'
        if file_type not in self.files:
            print("No '%s' files available" % file_type)
            return

        agg_values = {}
        total_samples = len(self.files.get(file_type).get(value, {}))
        filenames = self.files.get(file_type).get(value, {})
        if not filenames:
            print("No filenames for type=%s, value=%s" % (file_type, value))
            return

        events = {}
        filename = filenames[0]['filename']

        print("Chart 'events_timeline' from file '%s'" % filename)
        with open(filename, 'r') as f:
            reader = DictReader(f)
            for r in reader:
                events.setdefault(self.EventTypeCategories[r['event_type']], []).append({
                    # 'x': datetime.strptime(r['timestamp'], "%Y-%m-%d %H:%M:%S.%f"),
                    'x': int(datetime.strptime(r['timestamp'], "%Y-%m-%d %H:%M:%S.%f").timestamp() * 1000),
                    'title': self.EventTypeTitles[r['event_type']],
                    'text': "%s - %s" % (r['event_type'], ', '.join(r['details'].split('|')))
                })

        series = []
        for category, config in self.CategoriesConfig.items():
            s = {'type': 'flags', 'data': events[category]}
            s.update(config)
            series.append(s)

        return {
            'name': 'timeline_%s' % value,
            'live': False,
            'title': 'Scenario Timeline%s' % (' (%s)' % value if value else ''),
            'json': json.dumps({
                # 'title': 'Total Requests (n) = %s' % value,
                # 'xAxis': [{
                #     'categories': agg_values['categories'],
                #     'title': {'text': '# Instances'},
                #     'crosshair': True
                # }],
                'yAxis': [
                    {
                        'labels': {
                            'format': '{value}s'
                        },
                        'title': {'text': 'Time (s)'}
                    }
                ],
                'series': series
                # 'tooltip': {'shared': True}
            })
        }

    def get_charts(self):
        charts = {
            'timelines': [self.generate_timeline()],
        }

        return charts


if __name__ == "__main__":
    ap = ArgumentParser(description="Generate Provisioning report from data.")
    ap.add_argument('prefix', type=str,
                    help="Prefix of files to use (normally name of yaml file used to configure the experiment)")
    # ap.add_argument('values', type=ArgTypes.int_list,
    #                 help="Variable to vary")
    ap.add_argument('--name', dest='name', type=str, default=None,
                    help="Name of the experiment.")

    ap.add_argument('-o', dest='report_file', type=ArgTypes.writeable_filename, default=None,
                    help="Filename to save the report.")
    ap.add_argument('--results-folder', dest='results_folder', type=str,
                    help="Path for the results", default='results')

    args = ap.parse_args()
    if not args.name:
        args.name = args.prefix
    if not args.report_file:
        args.report_file = 'reports/scenario_%s.html' % args.name

    print(args)

    pr = ProfileReport(name=args.name, prefix=args.prefix, results_folder=args.results_folder)
    pr.generate(args.report_file)
    # pr = ProvisioningFrameworkReport(args.prefix, 'defaults.n', args.values, results_folder=args.results_folder)
    # pr.generate(args.report_file)
