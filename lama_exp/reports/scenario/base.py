from operator import itemgetter

from lama_exp.reports.base import Report
import json
import traceback
from json import encoder
from statistics import mean
from csv import DictReader
from os import walk, path

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.2'
__email__ = 'tcarvalho@cmu.edu'

encoder.FLOAT_REPR = lambda o: format(o, '.2f')
HighChartsColors = ['#7CB4EB', '#AA4744', '#90ED7D']
DarkerHighChartsColors = ['#1446B0', '#5A000C', '#3D9A29']
HighChartsColorsRGB = [['124', '180', '235'], ['170', '71', '68'], ['144', '237', '125']]
DarkerHighChartsColorsRGB = [['28', '111', '230'], ['110', '3', '12'], ['61', '154', '41']]


class ScenarioReport(Report):

    def __init__(self, name, prefix, results_folder, template_name='scenario_template.html', num_of_results=1,
                 parameter=None, values=()):
        super().__init__(template_name=template_name)

        self.name = name
        self.prefix = prefix
        self.num_of_results = num_of_results
        self.parameter = parameter
        self.values = values

        self.files = self.load_files(results_folder)
        print("Files: %s" % json.dumps(self.files, sort_keys=True, indent=4))

    def load_files(self, results_folder="results", include_file_types=('events',)):
        files = {
        }
        file_prefix = self.name
        # print(path.join(results_folder, file_prefix))
        for d in walk(results_folder):
            # print("d[0]: %s" % (d[0],))
            if d[0].startswith(path.join(results_folder, file_prefix)):
                # print("\tstarts with prefix: %s" % (d[0],))
                for file in d[2]:
                    try:
                        # print("\tfile: %s" % file)
                        parts = file.split('.')
                        # print("\tparts: %s" % parts)
                        file_type = parts[-2]
                        # print("\tfile_type: %s" % file_type)
                        preamble = '.'.join(parts[:-2])
                        # print("\tpreamble: %s" % preamble)

                        exp_details = preamble.split('-')
                        if len(exp_details) == 3:
                            prefix, params, ts = exp_details
                            # print("\tparts: prefix=%s, params=%s, ts=%s" % (prefix, params, ts))
                            params = params.split('_')
                        else:
                            prefix, ts = exp_details
                            params = []
                            # print("\tparts: prefix=%s, params=%s, ts=%s" % (prefix, params, ts))
                    except:
                        print("Trace: %s" % traceback.format_exc())
                        if not file.startswith("monitor"):
                            print("Unsupported filename: %s" % file)
                        continue
                    value = None
                    # print("chosen_parameter: %s" % self.parameter)
                    for p in params:
                        # print("\tp: %s" % p)
                        if p.startswith(self.parameter):
                            value = int(p[len(self.parameter):])
                            break

                    # print("\tfile_type: %s" % file_type)
                    # print("\tvalue: %s" % value)
                    # par, time, type1, type2, ext = file[len(self.name):].split('_', maxsplit=1)[1].split('.')
                    # print(par, time, type1, type2, ext)
                    # value = int(par.strip(self.parameter))
                    # file_type = '.'.join([type1, type2])
                    file_entries = files.setdefault(file_type, {}).setdefault(value, [])

                    if len(file_entries) < self.num_of_results:
                        file_entries.append(
                            {
                                'filename': path.join(d[0], file),
                                'time': ts
                            }
                        )
                    elif file_entries[0].get('time') < ts:
                        sorted(file_entries, key=itemgetter('time'))
                        file_entries[0] = {
                            'filename': path.join(d[0], file),
                            'time': ts
                        }

        return files

    def get_charts(self):
        raise NotImplementedError("Implement get_charts to generate report charts.")

    def load_variables(self):
        self.variables = {
            'title': "Experiment: %s" % self.name,
            'charts': self.get_charts()
        }

