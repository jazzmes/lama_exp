import json
import logging
import subprocess
import sys
from argparse import ArgumentParser, ArgumentTypeError
from datetime import datetime
from json import encoder
from os import mkdir, path

import requests

from lama_exp.reports.report_proxy import ReportServerBlocking, ReportHandler
from lama_exp.reports.scenario.base import ScenarioReport
from lama_exp.subscribers import LamaSubscriberThread, FrameworkEventTypes, BareLamaSubscriberThread, \
    FieldHelpers

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

logger = logging.getLogger(__name__)

encoder.FLOAT_REPR = lambda o: format(o, '.2f')

ReportProxyPort = 40900

ValidMetrics = {'vcpu', 'vdisk', 'vnet', 'vmem', 'cpu', 'disk', 'network', 'memory', 'rt'}


class LiveProfileReport(ScenarioReport):

    EventTypeKey = {
        FrameworkEventTypes.allocated_instance: 'InstanceAllocated',
        FrameworkEventTypes.active_instance: 'InstanceActive',
        FrameworkEventTypes.ready_instance: 'InstanceReady',
        FrameworkEventTypes.removed_instance: 'InstanceRemoved',
        FrameworkEventTypes.active_app: 'AppActive',
        FrameworkEventTypes.experiment_start: 'ExperimentStart',
        FrameworkEventTypes.experiment_end: 'ExperimentEnd',
        FrameworkEventTypes.experiment_app_requested: 'AppAPIRequest',
        FrameworkEventTypes.experiment_client_launched: 'AppClientLaunched',
        FrameworkEventTypes.experiment_failure: 'AppFailure',
        FrameworkEventTypes.experiment_note: 'ExpNote'
    }

    EventTypeTitles = {
        'ExperimentStart': 'Start',
        'ExperimentEnd': 'End',
        'InstanceActive': 'IOn',
        'InstanceReady': 'IRdy',
        'InstanceRemoved': 'IDel',
        'InstanceAllocated': 'IAlloc',
        'AppAPIRequest': 'AReq',
        'AppActive': 'AAct',
        'AppClientLaunched': 'ACli',
        'AppFailure': 'Fail',
        'ExpNote': '*'

    }

    EventTypeCategories = {
        'ExperimentStart': 'Experiment',
        'ExperimentEnd': 'Experiment',
        'InstanceActive': 'Instance',
        'InstanceAllocated': 'Instance',
        'InstanceRemoved': 'Instance',
        'InstanceReady': 'Instance',
        'AppAPIRequest': 'App',
        'AppActive': 'App',
        'AppClientLaunched': 'App',
        'AppFailure': 'App',
        'ExpNote': 'Experiment'
    }

    CategoriesConfig = {
        'Experiment': {'shape': 'circlepin'},
        'Instance': {'shape': 'squarepin'},
        'App': {'shape': 'squarepin'}
    }

    def __init__(self, name, prefix=None, results_folder='results', report_dir_path=None, metrics=None, **kwargs):
        super().__init__(name, prefix, results_folder, **kwargs)
        self._report_proc = None
        self._report_dir_path = report_dir_path
        self._active = True
        self._handler = None
        self._metrics = metrics or ()
        # self._server = ReportServer(
        #     port=40900,
        #     **{
        #         'get_data': data_request_callback
        #     }
        # )
        # self._server.start()
        # logger.debug("Launching live report server")
        # self._report_proc = subprocess.Popen(['python3', '-m', 'lama_exp.reports.report_proxy', str(ReportProxyPort)])
        # logger.debug("Launched live report server")

    def generate_live_timeline(self, ref_page=0):
        # file_type = 'events'
        # if file_type not in self.files:
        #     print("No '%s' files available" % file_type)
        #     return
        #
        # agg_values = {}
        # total_samples = len(self.files.get(file_type).get(value, {}))
        # filenames = self.files.get(file_type).get(value, {})
        # if not filenames:
        #     print("No filenames for type=%s, value=%s" % (file_type, value))
        #     return
        #
        # events = {}
        # filename = filenames[0]['filename']
        #
        # print("Chart 'events_timeline' from file '%s'" % filename)
        # with open(filename, 'r') as f:
        #     reader = DictReader(f)
        #     for r in reader:
        #         events.setdefault(self.EventTypeCategories[r['event_type']], []).append({
        #             # 'x': datetime.strptime(r['timestamp'], "%Y-%m-%d %H:%M:%S.%f"),
        #             'x': int(datetime.strptime(r['timestamp'], "%Y-%m-%d %H:%M:%S.%f").timestamp() * 1000),
        #             'title': self.EventTypeTitles[r['event_type']],
        #             'text': "%s - %s" % (r['event_type'], ', '.join(r['details'].split('|')))
        #         })
        #
        number_of_values = 1000
        ref = int(datetime.now().timestamp() * 1000)
        data = [[ref - (number_of_values - i) * 1000, 0] for i in range(0, number_of_values)]
        series = [
            {
                'name': 'Response Time (Avg)',
                'data': data,
                'id': 'ts_ref_{}_avg'.format(ref_page)
            },
            {
                'name': 'Response Time (95th Perc)',
                'data': data,
                'id': 'ts_ref_{}_95perc'.format(ref_page)
            },
            {
                'name': 'Response Time (Avg 95th Perc)',
                'data': data,
                'id': 'ts_ref_{}_95avg'.format(ref_page)
            },
        ]
        ts_series_list = [s['id'] for s in series]
        for category, config in self.CategoriesConfig.items():
            s = {'name': category, 'type': 'flags', 'data': [], 'onSeries': series[0]['id']}
            s.update(config)
            series.append(s)

        logger.debug("Series: %s", series)
        return {
            'name': 'timeline',
            'live': True,
            'title': 'Scenario Timeline',
            'json': json.dumps({
                'yAxis': [
                    {
                        'labels': {
                            'format': '{value}ms'
                        },
                        'title': {'text': 'Time (ms)'},
                        'min': 0
                    }
                ],
                'series': series,
            }),
            'series_list': ts_series_list
        }

    def generate_live_timeseries(self, metric, name=None, title=None, conditions=None, parameters=None):
        name = name or '{}_timeline'.format(metric)
        title = title or 'Scenario - {} Timeline'.format(metric)
        conditions = conditions or {metric: True}

        series = [
            {
                'name': 'Events (base)',
                'data': [(int(datetime.now().timestamp() * 1000), 0)],
                'id': 'events'
            },
        ]
        logger.debug("Series: %s", series)

        # series = []
        for category, config in self.CategoriesConfig.items():
            s = {'name': category, 'type': 'flags', 'data': [], 'onSeries': series[0]['id']}
            s.update(config)
            series.append(s)

        default_parameters = {
            'legend': {
                'enabled': True
            },
            'series': series,
        }
        if parameters:
            default_parameters.update(parameters)

        return {
            'name': name,
            'title': title,
            'live': True,
            'conditions': json.dumps(conditions),
            'json': json.dumps(default_parameters)
        }

    def generate_live_vdisk_timeseries(self):
        return self.generate_live_timeseries(
            'vdisk',
            conditions=[
                {
                    'vdisk': True,
                    'octets': False
                },
                {
                    'vdisk': True,
                }

            ],
            parameters={
                'yAxis': [
                    {
                        'title': {'text': 'Disk Usage (Ops)'},
                        'min': 0
                    },
                    {
                        'title': {'text': 'Disk Usage (Bps)'},
                        'min': 0
                    }
                ],
            }
        )

    def generate_live_cpu_timeseries(self, sub_metric=None):
        conditions = {
            'cpu': True,
            'virt_': False
        }
        name = 'cpu{}_timeline'.format('_' + sub_metric.capitalize() if sub_metric else '')
        title = 'Scenario CPU{} Timeline'.format(' ' + sub_metric.capitalize() if sub_metric else '')
        if sub_metric:
            conditions[sub_metric] = True

        return self.generate_live_timeseries(
            'cpu',
            name=name,
            title=title,
            conditions=conditions,
            parameters={
                'yAxis': [
                    {
                        'labels': {
                            'format': '{value}%'
                        },
                        'title': {'text': 'CPU Usage (%)'},
                        'min': 0,
                        'max': 100
                    }
                ],
            }
        )

    def generate_live_disk_timeseries(self, sub_metric=None):
        conditions = {
            'disk': True,
            'vdisk': False
        }
        y_axis = [
            {
                'labels': {
                    'format': '{value}'
                },
                'title': {'text': 'Disk Usage'}
            }
        ]

        name = 'disk{}_timeline'.format('_' + sub_metric.capitalize() if sub_metric else '')
        title = 'Scenario Disk{} Timeline'.format(' ' + sub_metric.capitalize() if sub_metric else '')

        if sub_metric:
            conditions[sub_metric] = True
            y_axis[0]['title']['text'] += ' - {}'.format(sub_metric)
        else:
            conditions = [conditions.copy(), conditions.copy()]
            conditions[0].update({'_octets': True})
            conditions[1].update({'_octets': False})
            y_axis[0]['title']['text'] += ' - Octets'
            y_axis.append(
                {
                    'labels': {
                        'format': '{value}'
                    },
                    'title': {'text': 'Disk Usage'}
                }
            )
        return self.generate_live_timeseries(
            'disk',
            # name='cpu_timeline',
            # title='Scenario CPU Timeline',
            name=name,
            title=title,
            conditions=conditions,
            parameters={
                'yAxis': y_axis,
            }
        )

    def generate_live_vcpu_timeseries(self):
        return self.generate_live_timeseries(
            'virt_cpu',
            # name='cpu_timeline',
            # title='Scenario CPU Timeline',
            # conditions={
            #     'virt_cpu': True
            # },
            parameters={
                'yAxis': [
                    {
                        'labels': {
                            'format': '{value}%'
                        },
                        'title': {'text': 'CPU Usage (%)'},
                        'min': 0,
                        'max': 100
                    }
                ],
            }
        )
        # series = [
        #     {
        #         'name': 'Events (base)',
        #         'data': [(int(datetime.now().timestamp() * 1000), 0)],
        #         'id': 'events'
        #     },
        # ]
        # logger.debug("Series: %s", series)
        #
        # # series = []
        # for category, config in self.CategoriesConfig.items():
        #     s = {'name': category, 'type': 'flags', 'data': [], 'onSeries': series[0]['id']}
        #     s.update(config)
        #     series.append(s)
        #
        # return {
        #     'name': 'cpu_timeline',
        #     'title': 'Scenario CPU Timeline',
        #     'live': True,
        #     'conditions': json.dumps(
        #         {
        #             'virt_cpu': True
        #         }
        #     ),
        #     'json': json.dumps({
        #         'yAxis': [
        #             {
        #                 'labels': {
        #                     'format': '{value}%'
        #                 },
        #                 'title': {'text': 'CPU Usage (%)'},
        #                 'min': 0,
        #                 'max': 100
        #             }
        #         ],
        #         'legend': {
        #             'enabled': True
        #         },
        #         'series': series,
        #     }),
        # }

    def generate_live_rt_timeseries(self, ref_page=0):
        return self.generate_live_timeseries(
            metric='response_time',
            name='rt_timeline_ref_{}'.format(ref_page),
            title='Scenario Response Time Timeline (Page: {})'.format(ref_page),
            conditions=[
                {
                    'ts_ref_{}'.format(ref_page): True,
                    'error': False,
                    'requests': False
                },
                {
                    'ts_ref_{}'.format(ref_page): True
                }
            ],
            parameters={
                'yAxis': [
                    {
                        'labels': {
                            'format': '{value}ms'
                        },
                        'title': {'text': 'Time (ms)'},
                        'min': 0
                    },
                    {
                        'title': {'text': 'Errors/Requests'},
                        'min': 0
                    },

                ],
            }
        )

        # series = [
        #     {
        #         'name': 'Events (base)',
        #         'data': [(int(datetime.now().timestamp() * 1000), 0)],
        #         'id': 'events'
        #     },
        # ]
        # logger.debug("Series: %s", series)
        #
        # # series = []
        # for category, config in self.CategoriesConfig.items():
        #     s = {'name': category, 'type': 'flags', 'data': [], 'onSeries': series[0]['id']}
        #     s.update(config)
        #     series.append(s)
        #
        # return {
        #     'name': 'rt_timeline',
        #     'title': 'Scenario Response Time Timeline',
        #     'live': True,
        #     'conditions': json.dumps(
        #         [
        #             {
        #                 'ts_': True,
        #                 'error': False,
        #                 'requests': False
        #             },
        #             {
        #                 'ts_': True
        #             }
        #         ]
        #     ),
        #     'json': json.dumps({
        #         'yAxis': [
        #             {
        #                 'labels': {
        #                     'format': '{value}ms'
        #                 },
        #                 'title': {'text': 'Time (ms)'},
        #                 'min': 0
        #             },
        #             {
        #                 'title': {'text': 'Errors/Requests'},
        #                 'min': 0
        #             },
        #
        #         ],
        #         'legend': {
        #             'enabled': True
        #         },
        #         'series': series,
        #     }),
        # }

    def generate_event_timeseries(self):
        series = [
            {
                'name': 'Events (base)',
                'data': [(int(datetime.now().timestamp() * 1000), 0)],
                'id': 'events'
            },
        ]

        for category, config in self.CategoriesConfig.items():
            s = {'name': category, 'type': 'flags', 'data': [], 'onSeries': series[0]['id']}
            s.update(config)
            series.append(s)

        return {
            'name': 'rt_timeline',
            'title': 'Scenario Response Time Timeline',
            'live': True,
            'conditions': json.dumps([]),
            'json': json.dumps({
                'legend': {
                    'enabled': True
                },
                'series': series,
            }),
        }

    def load_variables(self):
        self.variables = {
            'title': "Experiment: %s" % self.name,
            'live': self._active,
            'charts': self.get_charts()
        }
        if not self._active and self._handler:
            self.variables['data_file'] = 'data.json'
            self.variables['data'] = json.dumps(self._handler.get_data())
            self.variables['relative_path'] = path.relpath("reports", self._report_dir_path) if not self._active else '.'

    def get_charts(self):
        timeseries = []
        if 'rt' in self._metrics:
            timeseries.append(self.generate_live_rt_timeseries(ref_page=0))
            timeseries.append(self.generate_live_rt_timeseries(ref_page=9))
        if 'vcpu' in self._metrics:
            timeseries.append(self.generate_live_vcpu_timeseries())
        if 'vdisk' in self._metrics:
            timeseries.append(self.generate_live_vdisk_timeseries())
        if 'cpu' in self._metrics:
            for sub_metric in ['idle', 'system', 'user', 'wait', 'interrupt', 'nice', 'softirq']:
                timeseries.append(self.generate_live_cpu_timeseries(sub_metric=sub_metric))
        if 'disk' in self._metrics:
            for sub_metric in ['octets', 'ops']:
                timeseries.append(self.generate_live_disk_timeseries(sub_metric=sub_metric))

        charts = {
            'timeseries': timeseries
        }

        return charts

    def stop(self):
        logger.debug("Stop server")
        # if self._server:
        #     logger.debug("Stop server...")
        #     self._server.shutdown()
        #     logger.debug("Joining....")
        #     self._server.join()

        # # not CTRL+C
        res = requests.post("http://127.0.0.1:{}/shutdown".format(ReportProxyPort))
        logger.info("Result of shutdown: %s %s", res.status_code, res.reason)

    def start(self, blocking=True):
        logger.debug("Launching live report server")
        if blocking:
            self._handler = LiveProfileReportHandler(report_dir_path=self._report_dir_path, metrics=self._metrics)
            ReportServerBlocking(ReportProxyPort, handler=self._handler).start()
            self._active = False
            self._handler.save_data()
            logger.debug("Report server shutdown")
            self._handler.clean()
        else:
            self._report_proc = subprocess.Popen(['python3', '-m', 'lama_exp.reports.report_proxy', str(ReportProxyPort)])
            logger.debug("Launched live report server")


class LiveProfileReportHandler(ReportHandler):

    InstanceAvailableMetrics = {
        'vcpu': {'channels': ('lama:metrics:virt_cpu_total',)},
        'vdisk': {'patterns': ('lama:metrics:disk_*',)},
        'vnet': {'patterns': ('lama:metrics:if_*',)},
        'vmem': {'channels': ('lama:metrics:memory:actual_balloon', 'lama:metrics:memory:swap_in', 'lama:metrics:memory:rss')},
    }

    HostAvailableMetrics = {
        'cpu': {'patterns': ('lama:metrics:cpu:*',)},
        'disk': {'patterns': ('lama:metrics:disk:*',)},
        'network': {'patterns': ('lama:metrics:interface:*',)},
        'memory': {'patterns': ('lama:metrics:memory:*',)},
    }

    def __init__(self, *args, metrics=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.subscriber = LamaSubscriberThread(host='10.1.1.12', callback=self._process_notification,
                                               channels={
                                                   "lama_events:active_instances",
                                                   "lama_events:allocated_instances",
                                                   "lama_events:removed_instances",
                                                   "lama_events:ready_instances",
                                                   "lama_events:active_apps",
                                                   "lama_events:experiments"
                                               })
        self.subscriber.start()

        self._metrics = metrics or ()

        self._dependent_subscriptions = {}
        if 'rt' in self._metrics:
            self._dependent_subscriptions.setdefault(FrameworkEventTypes.experiment_app_requested, []).append(
                {
                    'conditions': lambda fields: True,
                    'host': lambda fields: fields.get('provider'),
                    'channel': lambda fields: (),
                    'pattern': lambda fields: ('lama:metrics:response_time:ref_0*',
                                               'lama:metrics:response_time:ref_9*'),
                }
            )

        instance_channels = []
        instance_patterns = []
        for m, subs in LiveProfileReportHandler.InstanceAvailableMetrics.items():
            if m in self._metrics:
                instance_channels.extend(subs.get('channels', ()))
                instance_patterns.extend(subs.get('patterns', ()))

        if instance_channels or instance_patterns:
            self._dependent_subscriptions.setdefault(FrameworkEventTypes.active_instance, []).append(
                {
                    'conditions': lambda fields: True,
                    'host': lambda fields: fields.get('agent_ip'),
                    'channel': lambda fields: instance_channels,
                    'pattern': lambda fields: instance_patterns,
                    # 'pattern': lambda fields: ('libvirt-lama_{app_name}_{instance_name}/virt_cpu_total*'.format(**fields), ),
                }
            )

        host_channels = []
        host_patterns = []
        for m, subs in LiveProfileReportHandler.HostAvailableMetrics.items():
            if m in self._metrics:
                host_channels.extend(subs.get('channels', ()))
                host_patterns.extend(subs.get('patterns', ()))

        if host_channels or host_patterns:
            self._dependent_subscriptions.setdefault(FrameworkEventTypes.active_instance, []).append(
                {
                    'conditions': lambda fields: True,
                    'host': lambda fields: fields.get('agent_ip'),
                    'channel': lambda fields: host_channels,
                    'pattern': lambda fields: host_patterns,
                    # 'pattern': lambda fields: ('libvirt-lama_{app_name}_{instance_name}/virt_cpu_total*'.format(**fields), ),
                }
            )
        self._sec_subscribers = {}

    def _process_notification(self, event_type, **fields):
        logger.debug("Received event: [%s] %s", event_type, ', '.join(['%s=%s' % (k,v) for k, v in fields.items()]))
        timestamp = fields.get('remove_ts') or fields.get('active_ts') or fields.get('ready_ts')\
                    or fields.get('allocated_ts') or fields.get('timestamp') or fields.get('allocated_ts')
        timestamp = int(timestamp.timestamp() * 1000)
        event_type_name = LiveProfileReport.EventTypeKey[event_type]
        self.post_data('events', {
            'x': timestamp,
            'name': LiveProfileReport.EventTypeCategories[event_type_name],
            'title': LiveProfileReport.EventTypeTitles[event_type_name],
            'text': "%s%s" % (
                event_type_name,
                ' - %s' % ', '.join(["%s=%s" % (k, v) for k, v in fields.items()]) if fields else ''
            )
        })

        self.check_dependencies(event_type, **fields)

    def _process_raw_notification(self, channel, msg):
        key = None
        channel_fields = channel.split(':')
        msg_fields = msg.split('|')
        if 'rt' in self._metrics and channel in {
                "lama:metrics:response_time:ref_0:avg",
                "lama:metrics:response_time:ref_0:95perc",
                "lama:metrics:response_time:ref_0:95avg",
                "lama:metrics:response_time:ref_0:errors",
                "lama:metrics:response_time:ref_0:requests",
                "lama:metrics:response_time:ref_9:avg",
                "lama:metrics:response_time:ref_9:95perc",
                "lama:metrics:response_time:ref_9:95avg",
                "lama:metrics:response_time:ref_9:errors",
                "lama:metrics:response_time:ref_9:requests"
        }:
            key = "ts_{}_{}".format(channel_fields[-2], channel_fields[-1])

        if 'vcpu' in self._metrics and channel.startswith("lama:metrics:virt_cpu"):
            key = "virt_cpu_{}".format(msg_fields[2])

        elif 'vdisk' in self._metrics and channel.startswith("lama:metrics:disk_"):
            key = "vdisk_{}_{}".format(msg_fields[2], '_'.join(channel_fields[-3:]).strip('-None'))

        elif 'cpu' in self._metrics and channel.startswith("lama:metrics:cpu:"):
            key = "cpu_{}_{}_{}".format(msg_fields[2], channel_fields[3], channel_fields[4])

        elif 'disk' in self._metrics and channel.startswith("lama:metrics:disk:"):
            key = "disk_{}_{}_{}".format(msg_fields[2], channel_fields[3], channel_fields[4])

        else:
            logger.debug("Not processed RAW message: [%s] %s", channel, msg)

        if key:
            # msg = '2016-06-15 23:34:41|Instance|client-0001|response_time:ref_0:avg|15.8387'
            try:
                timestamp = datetime.strptime(msg_fields[0], "%Y-%m-%d %H:%M:%S")
            except ValueError:
                timestamp = datetime.strptime(msg_fields[0], "%Y-%m-%d %H:%M:%S.%f")

            timestamp = FieldHelpers.convert_time(timestamp)
            timestamp = int(timestamp.timestamp() * 1000)

            value = float(msg_fields[4])
            logger.debug("Posting...")
            self.post_data(key, [timestamp, value])

    def check_dependencies(self, event_type, **fields):
        logger.debug("Fields: %s", fields)
        for deps in self._dependent_subscriptions.get(event_type, []):
            if deps.get('conditions')(fields):
                host = deps.get('host')(fields)
                channel = deps.get('channel')(fields)
                pattern = deps.get('pattern')(fields)

                if host not in self._sec_subscribers:
                    logger.debug("Subscribing: [%s] %s %s", host, channel, pattern)
                    sub = BareLamaSubscriberThread(
                        host=host,
                        callback=self._process_raw_notification,
                        channels=channel,
                        channel_patterns=pattern
                    )
                    self._sec_subscribers[host] = sub
                    sub.start()
                else:
                    logger.debug("Subscribing (existing host): [%s] %s %s", host, channel, pattern)
                    sub = self._sec_subscribers.get(host)
                    sub.subscribe(channels=channel, patterns=pattern)
                    # logger.debug("Not subscribing! Already exists...")

    def clean(self):
        self.subscriber.stop()
        for host, sub in self._sec_subscribers.items():
            sub.stop()
        logger.debug("out of clean")


def metric_list(value):

    try:
        metrics = set(v for v in value.split(',') if v)
        invalid = []
        for m in metrics:
            if m not in ValidMetrics:
                invalid.append(m)

        if invalid:
            raise ArgumentTypeError("Invalid metrics: '{}'".format("', '".join(invalid)))

        if not metrics:
            raise ArgumentTypeError("Specify ate least one metric")

        return metrics
    except Exception as e:
        raise ArgumentTypeError("Bad value: {}".format(e))


if __name__ == "__main__":
    logger.setLevel(logging.DEBUG)
    stream_handler = logging.StreamHandler(stream=sys.stdout)
    stream_handler.setFormatter(
        logging.Formatter("%(asctime)s : %(levelname)5.5s : %(module)14.14s [%(lineno)3.3s] : %(message)s")
    )
    logging.root.addHandler(stream_handler)
    logging.root.setLevel(logging.DEBUG)

    ap = ArgumentParser(description="Generate Provisioning report from data.")
    ap.add_argument('prefix', type=str,
                    help="Prefix of files to use (normally name of yaml file used to configure the experiment)")
    # ap.add_argument('values', type=ArgTypes.int_list,
    #                 help="Variable to vary")
    ap.add_argument('--name', dest='name', type=str, default=None,
                    help="Name of the experiment.")

    # ap.add_argument('-o', dest='report_file', type=ArgTypes.writeable_filename, default=None,
    #                 help="Filename to save the report.")
    ap.add_argument('--results-folder', dest='results_folder', type=str,
                    help="Path for the results", default='results')

    ap.add_argument('-m', '--metrics', dest='metrics', type=metric_list,
                    help='Which metrics to plot (events are always collected)', default=[])

    args = ap.parse_args()
    if not args.name:
        args.name = args.prefix

    logger.debug("Args: %s", args)

    # create folder for final report
    report_dir = 'reports/scenario_{}_{}'.format(args.name, datetime.now().strftime('%Y%m%d%H%M%S'))
    if path.isdir('reports'):
        try:
            mkdir(report_dir)
        except:
            pass
    else:
        logger.error("No reports folder")
        exit()

    pr = LiveProfileReport(name=args.name, prefix=args.prefix, results_folder=args.results_folder,
                           report_dir_path=report_dir, metrics=args.metrics)

    pr.generate('reports/scenario_live_profile.html')
    subprocess.call(['open', '/Users/tiago/Projects/PycharmProjects/lama_exp/reports/scenario_live_profile.html'])

    pr.start()
    pr.generate(path.join(report_dir, 'scenario_profile.html'))
    # subprocess.call(['open', path.abspath(path.join(report_dir, 'scenario_profile.html'))])
    logger.info("To see the report run:\nopen %s", path.abspath(path.join(report_dir, 'scenario_profile.html')))


    # pr.generate(args.report_file)
    # pr = ProvisioningFrameworkReport(args.prefix, 'defaults.n', args.values, results_folder=args.results_folder)
    # pr.generate(args.report_file)
