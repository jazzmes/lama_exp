from operator import itemgetter

from lama_exp.reports.base import Report
import json
from json import encoder
from statistics import mean
from csv import DictReader
from os import walk, path
from datetime import datetime


__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.2'
__email__ = 'tcarvalho@cmu.edu'

encoder.FLOAT_REPR = lambda o: format(o, '.2f')
HighChartsColors = ['#7CB4EB', '#AA4744', '#90ED7D']
DarkerHighChartsColors = ['#1446B0', '#5A000C', '#3D9A29']
HighChartsColorsRGB = [['124', '180', '235'], ['170', '71', '68'], ['144', '237', '125']]
DarkerHighChartsColorsRGB = [['28', '111', '230'], ['110', '3', '12'], ['61', '154', '41']]


class ProvisioningReport(Report):

    def __init__(self, name, parameter, values, template_name, results_folder="results", print_names=None,
                 num_of_results=1):
        super().__init__(template_name=template_name)

        if print_names is None:
            print_names = {}
        self.name = name
        self.print_names = print_names
        self.parameter = parameter
        self.values = values
        self.num_of_results = num_of_results

        self.files = self.load_files(results_folder)
        # print("Loaded files: %s" % self.files)

    def load_files(self, results_folder="results"):
        files = {
        }
        file_prefix = self.name
        # print(path.join(results_folder, file_prefix))
        for d in walk(results_folder):
            # print("d[0]: %s" % (d[0],))
            if d[0].startswith(path.join(results_folder, file_prefix)):
                # print("starts with prefix: %s" % (d[0],))
                for file in d[2]:
                    try:
                        # print("file: %s" % file)
                        parts = file.split('.')
                        file_type = '.'.join(parts[-3:-1])
                        preamble = '.'.join(parts[:-3])

                        prefix, params, ts = preamble.split('-')
                        # print("parts: prefix=%s, params=%s, ts=%s" % (prefix, params, ts))
                        params = params.split('_')
                    except:
                        if not file.startswith("monitor"):
                            print("Unsupported filename: %s" % file)
                        continue
                    value = None
                    # print("chosen_parameter: %s" % self.parameter)
                    for p in params:
                        # print("\tp: %s" % p)
                        if p.startswith(self.parameter):
                            value = int(p[len(self.parameter):])
                            break

                    # print("\tfile_type: %s" % file_type)
                    # print("\tvalue: %s" % value)
                    # par, time, type1, type2, ext = file[len(self.name):].split('_', maxsplit=1)[1].split('.')
                    # print(par, time, type1, type2, ext)
                    # value = int(par.strip(self.parameter))
                    # file_type = '.'.join([type1, type2])
                    file_entries = files.setdefault(file_type, {}).setdefault(value, [])
                    if len(file_entries) < self.num_of_results:
                        file_entries.append(
                            {
                                'filename': path.join(d[0], file),
                                'time': ts
                            }
                        )
                    elif file_entries[0].get('time') < ts:
                        sorted(file_entries, key=itemgetter('time'))
                        file_entries[0] = {
                            'filename': path.join(d[0], file),
                            'time': ts
                        }

        return files

    def generate_call_and_active_lines(self, value):
        file_type = 'response_times.raw'
        if file_type not in self.files:
            print("No '%s' files available" % file_type)
            return

        # files = self.files.get(file_type)
        # keys = sorted(files.keys())
        agg_values = {}
        total_samples = len(self.files.get(file_type).get(value, {}))
        bad_samples = {}
        filenames = self.files.get(file_type).get(value, {})
        if not filenames:
            print("No filenames for type=%s, value=%s" % (file_type, value))
            return
        for entry in filenames:
            filename = entry.get('filename')
            # filename = self.files.get(file_type).get(value, {}).get('filename')

            print("Chart 'call_and_active_lines' from file '%s'" % filename)
            values = {
                'categories': [],
                'call': [],
                'active': [],
                'hosts': []
            }
            host_set = set()
            with open(filename, 'r') as f:
                reader = DictReader(f)
                for i, r in enumerate(reader):
                    values['categories'].append(i+1)
                    values['call'].append(float(r['rt_call']) if r.get('rt_call') else None)
                    values['active'].append(float(r['rt_active']) if r.get('rt_active') else None)
                    host_set.add(r['host'])
                    values['hosts'].append(len(host_set))

            for k, arr in values.items():
                agg_values.setdefault(k, [0] * len(arr))
                for i, v in enumerate(arr):
                    # TODO: handle the case where v is None
                    if v:
                        agg_values[k][i] += v / total_samples

        series = [
            {'name': 'RT Call', 'data': agg_values['call'], 'tooltip': {'valueSuffix': 's'}},
            {'name': 'RT Active', 'data': agg_values['active'], 'tooltip': {'valueSuffix': 's'}},
            {'name': 'Num. Hosts', 'data': agg_values['hosts'], 'yAxis': 1},
        ]

        return {
            'name': 'call_and_active_%s' % value,
            'title': 'Resp times: %s instances' % value,
            'json': json.dumps({
                # 'title': 'Total Requests (n) = %s' % value,
                'xAxis': [{
                    'categories': agg_values['categories'],
                    'title': {'text': '# Instances'},
                    'crosshair': True
                }],
                'yAxis': [
                    {
                        'labels': {
                          'format': '{value}s'
                        },
                        'title': {'text': 'Time (s)'}
                    },
                    {
                        'title': {'text': '# Hosts'},
                        'opposite': True
                    },
                ],
                'series': series,
                'tooltip': {'shared': True}
            })
        }

    def generate_with_all(self, field='rt_active', title='Time to become active'):
        file_type = 'response_times.raw'
        if file_type not in self.files:
            print("No '%s' files available" % file_type)
            return

        # files = self.files.get(file_type)
        # keys = sorted(files.keys())
        series = []
        categories = []
        for v in self.values:

            agg_values = {}
            total_samples = len(self.files.get(file_type).get(v, {}))
            filenames = self.files.get(file_type).get(v, {})
            if not filenames:
                print("No filenames for type=%s, value=%s" % (file_type, v))
                continue
            for entry in filenames:
                filename = entry.get('filename')

                values = {
                    's': []
                }
                with open(filename, 'r') as f:
                    reader = DictReader(f)
                    for i, r in enumerate(reader):
                        if i >= len(categories):
                            categories.append(i + 1)
                        values['s'].append(float(r[field]) if r[field] else None)

                for k, arr in values.items():
                    agg_values.setdefault(k, [0] * len(arr))
                    for i, val in enumerate(arr):
                        # TODO: handle the case where v is None
                        if val:
                            agg_values[k][i] += val / total_samples

            series.append({
                'name': '%s=%s' % (self.parameter, v),
                'data': agg_values['s'],
                'tooltip': {'valueSuffix': 's'}
            })

        return {
            'name': '%s_all' % field,
            'title': title,
            'json': json.dumps({
                # 'title': 'Total Requests (n) = %s' % value,
                'xAxis': [{
                    'categories': categories,
                    'title': {'text': '# Instances'},
                    'crosshair': True
                }],
                'yAxis': [
                    {
                        'labels': {
                          'format': '{value}s'
                        },
                        'title': {'text': 'Time (s)'}
                    }
                ],
                'series': series,
                'tooltip': {'shared': True}
            })
        }

    def generate_agg_with_error(self):
        file_type = 'response_times.raw'
        if file_type not in self.files:
            print("No '%s' files available" % file_type)
            return

        categories = sorted(self.values)
        sline_active = []
        serror_active = []
        sline_call = []
        serror_call = []
        sline_diff = []
        for v in sorted(self.values):

            agg_values = {}
            total_samples = len(self.files.get(file_type).get(v, {}))
            filenames = self.files.get(file_type).get(v, {})
            if not filenames:
                print("No filenames for type=%s, value=%s" % (file_type, v))
                continue
            for entry in filenames:
                filename = entry.get('filename')

                # filename = self.files.get(file_type).get(v, {}).get('filename')
                if not filename:
                    print("No filename for type=%s, value=%s" % (file_type, v))
                    continue
                values = {
                    's_active': [],
                    's_call': [],
                }
                with open(filename, 'r') as f:
                    reader = DictReader(f)
                    for i, r in enumerate(reader):
                        if i > len(categories):
                            categories.append(i+1)
                        values['s_active'].append(round(float(r['rt_active']), 2) if r['rt_active'] else None)
                        values['s_call'].append(round(float(r['rt_call']), 2) if r.get('rt_call') else None)

                values['s_active'] = [r for r in values['s_active'] if r is not None]
                values['s_call'] = [r for r in values['s_call'] if r is not None]

                for k, arr in values.items():
                    agg_values.setdefault(k, [0] * len(arr))
                    for i, val in enumerate(arr):
                        # TODO: handle the case where v is None
                        if val:
                            agg_values[k][i] += val / total_samples

            sline_active.append(round(mean(agg_values['s_active']), 2) if agg_values['s_active'] else None)
            serror_active.append([min(agg_values['s_active']), max(agg_values['s_active'])] if agg_values['s_active'] else None)
            sline_call.append(round(mean(agg_values['s_call']), 2) if agg_values['s_call'] else None)
            serror_call.append([min(agg_values['s_call']), max(agg_values['s_call'])])
            sline_diff.append(round(sline_active[-1] - sline_call[-1], 2) if agg_values['s_active'] else None)

        # set yaxis max
        ym = int(max(x[1] for v in [serror_active, serror_call] for x in v if x) / 10 + 1) * 10
        series = [
            {
                'name': 'RT Call AVG',
                'data': sline_call
            },
            {
                'name': 'RT Call Error',
                'data': serror_call,
                'type': 'errorbar',
            },
            {
                'name': 'RT Active AVG',
                'data': sline_active
            },
            {
                'name': 'RT Active Error',
                'data': serror_active,
                'type': 'errorbar',
            },
            {
                'name': 'Diff AVG',
                'data': sline_diff
            }
        ]
        return {
            'name': 'resp_time_agg',
            'title': 'Average Resp. Time with MinMax',
            'json': json.dumps({
                # 'title': 'Total Requests (n) = %s' % value,
                'xAxis': [{
                    'categories': categories,
                    'title': {'text': '# Instances'},
                    'crosshair': True
                }],
                'yAxis': [
                    {
                        'labels': {
                          'format': '{value}s'
                        },
                        'title': {'text': 'Time (s)'},
                        'max': ym,
                        'min': 0
                    }
                ],
                'series': series,
                'tooltip': {'shared': True, 'valueSuffix': 's'}
            })
        }

    def generate_hosts_used_all(self):
        file_type = 'response_times.raw'
        if file_type not in self.files:
            print("No '%s' files available" % file_type)
            return

        series = []
        categories = []
        for v in sorted(self.values):

            agg_values = {}
            total_samples = len(self.files.get(file_type).get(v, {}))
            filenames = self.files.get(file_type).get(v, {})
            if not filenames:
                print("No filenames for type=%s, value=%s" % (file_type, v))
                continue
            for entry in filenames:
                filename = entry.get('filename')
                # filename = self.files.get(file_type).get(v, {}).get('filename')
                if not filename:
                    print("No filename for type=%s, value=%s" % (file_type, v))
                    continue

                values = {
                    'hosts': [],
                }
                host_set = set()
                with open(filename, 'r') as f:
                    reader = DictReader(f)
                    for i, r in enumerate(reader):
                        if i >= len(categories):
                            categories.append(i + 1)

                        host_set.add(r['host'])
                        values['hosts'].append(len(host_set))

                for k, arr in values.items():
                    agg_values.setdefault(k, [0] * len(arr))
                    for i, val in enumerate(arr):
                        # TODO: handle the case where v is None
                        if val:
                            agg_values[k][i] += val / total_samples

            series.append({
                'name': '%s=%s' % (self.parameter, v),
                'data': agg_values['hosts']
            })

        return {
            'name': 'n_hosts_used_all',
            'title': 'Number of PMs with allocated VMs',
            'json': json.dumps({
                # 'title': 'Total Requests (n) = %s' % value,
                'xAxis': [{
                    'categories': categories,
                    'title': {'text': '# Instances'},
                    'crosshair': True
                }],
                'yAxis': [
                    {
                        'labels': {
                          'format': '{value}s'
                        },
                        'title': {'text': 'User Servers'}
                    }
                ],
                'series': series,
                'tooltip': {'shared': True}
            })
        }

    def generate_errors_all(self):
        file_type = 'response_times.raw'
        if file_type not in self.files:
            print("No '%s' files available" % file_type)
            return

        categories = []
        series = []
        for v in sorted(self.values):

            agg_values = {}
            total_samples = len(self.files.get(file_type).get(v, {}))
            filenames = self.files.get(file_type).get(v, {})
            if not filenames:
                print("No filenames for type=%s, value=%s" % (file_type, v))
                continue
            for entry in filenames:
                filename = entry.get('filename')

                values = {
                    'errors': []
                }
                n_errors = 0
                with open(filename, 'r') as f:
                    reader = DictReader(f)
                    for i, r in enumerate(reader):
                        if i >= len(categories):
                            categories.append(i + 1)

                        if not r['success'] or r['success'] != 'True':
                            n_errors += 1
                        values['errors'].append(n_errors)

                for k, arr in values.items():
                    agg_values.setdefault(k, [0] * len(arr))
                    for i, val in enumerate(arr):
                        # TODO: handle the case where v is None
                        if val:
                            agg_values[k][i] += val / total_samples

                series.append({
                    'name': '%s=%s' % (self.parameter, v),
                    'data': agg_values['errors']
                })

        return {
            'name': 'errors_all',
            'title': 'Number of errors (VM not allocated)',
            'json': json.dumps({
                # 'title': 'Total Requests (n) = %s' % value,
                'xAxis': [{
                    'categories': categories,
                    'title': {'text': '# Instances'},
                    'crosshair': True
                }],
                'yAxis': [
                    {
                        'labels': {
                          'format': '{value}'
                        },
                        'title': {'text': 'Num. Error'}
                    }
                ],
                'series': series,
                'tooltip': {'shared': True}
            })
        }

    def get_charts(self):
       raise NotImplementedError("Implement get_charts to generate report charts.")

    def load_variables(self):
        self.variables = {
            'title': "Experiment: %s" % self.name,
            'charts': self.get_charts()
        }

