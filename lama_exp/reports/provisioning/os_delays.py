import json
from json import encoder
from argparse import ArgumentParser

from lama_exp.utils.arg import ArgTypes
from os import walk, path
from operator import itemgetter
from csv import DictReader

from lama_exp.reports.provisioning.base import ProvisioningReport

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

encoder.FLOAT_REPR = lambda o: format(o, '.2f')


class ProvisioningOSDelaysReport(ProvisioningReport):

    def __init__(self, prefixes, parameter, values, results_folder='results'):
        self.prefixes = prefixes
        super().__init__('OpenStack Delay Analysis', parameter, values, 'provisioning_os_delays_report_template.html', results_folder)

    def load_files(self, results_folder="results"):
        files = {}
        for prefix in self.prefixes:
            file_prefix = prefix
            # print("file_prefix=%s" % file_prefix)
            # print(path.join(results_folder, file_prefix))
            for d in walk(results_folder):
                # print(d[0])
                if d[0].startswith(path.join(results_folder, file_prefix)):
                    # print("starts with prefix: %s" % (d[0],))
                    for file in d[2]:
                        try:
                            parts = file.split('.')
                            file_type = '.'.join(parts[-2:-1])
                            preamble = '.'.join(parts[:-2])

                            prefix, params, ts = preamble.split('-')
                            # print("parts: prefix=%s, params=%s, ts=%s" % (prefix, params, ts))
                            params = params.split('_')
                        except:
                            print("Unsupported filename: %s" % file)
                            continue
                        value = None
                        # print("chosen_parameter: %s" % self.parameter)
                        for p in params:
                            # print("\tp: %s" % p)
                            if p.startswith(self.parameter):
                                value = int(p[len(self.parameter):])
                                break

                        # print("\tfile_type: %s" % file_type)
                        # print("\tvalue: %s" % value)
                        # par, time, type1, type2, ext = file[len(self.name):].split('_', maxsplit=1)[1].split('.')
                        # print(par, time, type1, type2, ext)
                        # value = int(par.strip(self.parameter))
                        # file_type = '.'.join([type1, type2])
                        # file_entry = files.get(file_type, {}).get(file_prefix, {}).get(value)
                        # if not file_entry or file_entry.get('time') < ts:
                        #     files.setdefault(file_type, {}).setdefault(file_prefix, {})[value] = {
                        #         'filename': path.join(d[0], file),
                        #         'time': ts
                        #     }
                        # print(path.join(d[0], file))
                        file_entries = files.setdefault(file_type, {}).setdefault(file_prefix, {}).setdefault(value, [])
                        if len(file_entries) < self.num_of_results:
                            file_entries.append(
                                {
                                    'filename': path.join(d[0], file),
                                    'time': ts
                                }
                            )
                        elif file_entries[0].get('time') < ts:
                            sorted(file_entries, key=itemgetter('time'))
                            file_entries[0] = {
                                'filename': path.join(d[0], file),
                                'time': ts
                            }
        return files

    def generate_delay_times(self, prefix, n, aggregate=False):
        all_series = []
        file_type = 'os_delays'
        print("Processing: %s %s" % (prefix, n))
        if file_type not in self.files or not self.files[file_type].get(prefix):
            print("No '%s' files available (prefix=%s, val=%s)" % (prefix, file_type, n))
            return

        prefix_files = self.files.get(file_type, {}).get(prefix)
        # print("prefix=%s, exp=%s" % (prefix, experiment_type))
        filename = prefix_files.get(n, [{}])[-1].get('filename')
        if not filename:
            print("No filename for type=%s, value=%s" % (file_type, n))
            return

        if aggregate:
            series = []
            avg = {}
            with open(filename, 'r') as f:
                reader = DictReader(f)
                categories = reader.fieldnames
                for i, r in enumerate(reader):
                    for c in categories:
                        avg[c] = avg.get(c, 0) + float(r[c]) / n

            series.append(
                {
                    'name': 'avg_times',
                    'data': [avg[c] for c in categories],
                }
            )
        else:
            series = []
            with open(filename, 'r') as f:
                reader = DictReader(f)
                categories = reader.fieldnames
                for i, r in enumerate(reader):
                    series.append(
                        {
                            'name': 'instance-%s' % (i + 1),
                            'data': [float(r[c]) for c in categories],
                        }
                    )

        r = {
            'chart': {
                'type': 'area'
            },
            'xAxis': [{
                'categories': categories,
                'title': {'text': 'OpenStack Provisioning Phases'},
                'crosshair': True
            }],
            'yAxis': {
                'title': {'text': 'Time (s)'},
                'min': 0
            },
            'tooltip': {
                'crosshairs': True,
                'shared': True,
                'split': True,
                'valueSuffix': 's'
            },
            'series': series,
            'plotOptions': {
                'area': {
                    'stacking': 'normal',
                }
            },
            'legend': {
                'enabled': False
            }
        }
        # if n == 5:
        #     print(json.dumps(r, indent=2))
        return {
            'name': '%s_%sdelays_%s' % (prefix, 'agg_' if aggregate else '', n),
            'title': 'OpenStack Delays: %s, n: %s' % (prefix, n),
            'json': json.dumps(r)
        }

    def generate_delay_evolution(self, prefix, stacked=True):
        file_type = 'os_delays'
        if file_type not in self.files or not self.files[file_type].get(prefix):
            print("No '%s' files available (prefix=%s)" % (prefix, file_type))
            return

        prefix_files = self.files.get(file_type, {}).get(prefix)
        series = []
        # print("prefix=%s, exp=%s" % (prefix, experiment_type))

        for n in self.values:
            filename = prefix_files.get(n, [{}])[-1].get('filename')
            if not filename:
                print("No filename for type=%s, value=%s" % (file_type, n))
                return

            avg = {}
            with open(filename, 'r') as f:
                reader = DictReader(f)
                categories = reader.fieldnames
                for i, r in enumerate(reader):
                    for c in categories:
                        avg[c] = avg.get(c, 0) + float(r[c]) / n

            for i, c in enumerate(categories):
                if i >= len(series):
                    series.append(
                        {
                            'name': c,
                            'data': [],
                        }
                    )
                series[i]['data'].append(avg[c])

        r = {
            'xAxis': [{
                'categories': self.values,
                'title': {'text': 'OpenStack Provisioning Phases'},
                'crosshair': True
            }],
            'yAxis': {
                'title': {'text': 'Time (s)'},
                'min': 0
            },
            'tooltip': {
                'crosshairs': True,
                'shared': True,
                'split': True,
                'valueSuffix': 's'
            },
            'series': series,
        }
        if stacked:
            r['chart'] = {
                'type': 'column'
            }
            r['plotOptions'] = {
                'column': {
                    'stacking': 'normal',
                    'groupPadding': 0.1,
                }
            }
        # else:
        #     r['chart'] = {
        #         'type': 'column'
        #     }
        # if n == 5:
        #     print(json.dumps(r, indent=2))
        return {
            'name': '%s_%sdelays_evol' % (prefix, 'stacked_' if stacked else ''),
            'title': 'OpenStack Delays: %s' % (prefix),
            'json': json.dumps(r)
        }

    def generate_reversed_delay_times(self, prefix, n):
        all_series = []
        file_type = 'os_delays'
        print("Processing: %s %s" % (prefix, n))
        if file_type not in self.files or not self.files[file_type].get(prefix):
            print("No '%s' files available (prefix=%s)" % (prefix, file_type))
            return

        prefix_files = self.files.get(file_type, {}).get(prefix)
        # print("prefix=%s, exp=%s" % (prefix, experiment_type))
        filename = prefix_files.get(n, [{}])[-1].get('filename')
        if not filename:
            print("No filename for type=%s, value=%s" % (file_type, n))
            return

        # series = []
        # with open(filename, 'r') as f:
        #     reader = DictReader(f)
        #     categories = reader.fieldnames
        #     for i, r in enumerate(reader):
        #         series.append(
        #             {
        #                 'name': 'instance-%s' % (i + 1),
        #                 'data': [float(r[c]) for c in categories],
        #             }
        #         )

        series = []
        with open(filename, 'r') as f:
            reader = DictReader(f)
            # categories = lsit(range(1, n + 1))
            for fn in reader.fieldnames:
                series.append({
                    'name': fn,
                    'data': [],
                })
            for r in reader:
                for i, fn in enumerate(reader.fieldnames):
                    series[i]['data'].append(float(r[fn]))

        r = {
            'chart': {
                'type': 'area'
            },
            'xAxis': [{
                # 'categories': categories,
                # 'title': {'text': 'OpenStack Provisioning Phases'},
                'crosshair': True
            }],
            'yAxis': {
                'title': {'text': 'Time (s)'},
                'min': 0
            },
            'tooltip': {
                'crosshairs': True,
                'shared': True,
                'split': True,
                'valueSuffix': 's'
            },
            'series': series,
            'plotOptions': {
                'area': {
                    'stacking': 'normal',
                }
            },
            # 'legend': {
            #     'enabled': False
            # }
        }
        # if n == 5:
        #     print(json.dumps(r, indent=2))
        return {
            'name': '%s_reversed_delays_%s' % (prefix, n),
            'title': 'OpenStack Delays: %s, n: %s' % (prefix, n),
            'json': json.dumps(r)
        }

    def get_charts(self):
        charts = {
            # 'errors_all': [self.generate_agg_error(prefix) for prefix in self.Prefixes],
            'agg_delays': [self.generate_delay_times(prefix, v, aggregate=True) for v in self.values for prefix in self.prefixes],
            'agg_delays_evol': [self.generate_delay_evolution(prefix, stacked=stacked) for stacked in [True, False] for prefix in self.prefixes],
            'delays': [self.generate_delay_times(prefix, v) for v in self.values for prefix in self.prefixes],
            'reversed_delays': [self.generate_reversed_delay_times(prefix, v) for v in self.values for prefix in self.prefixes],
        }

        return charts


if __name__ == "__main__":
    ap = ArgumentParser(description="Generate OpenStack delay analysis report from data.")
    ap.add_argument('values', type=ArgTypes.int_list,
                    help="Variable to vary")
    ap.add_argument('--prefixes', type=ArgTypes.str_list, default=['sequential_os', 'parallel_os', 'parallel_multiuser_os'],
                    help="Prefix of files to use (normally name of yaml file used to configure the experiment)")
    ap.add_argument('-o', dest='report_file', type=ArgTypes.writeable_filename, default=None,
                    help="Filename to save the report.")
    ap.add_argument('--results-folder', dest='results_folder', type=str,
                    help="Path for the results", default='results')

    args = ap.parse_args()
    if not args.report_file:
        args.report_file = 'reports/prov_os_delays.html'
    pr = ProvisioningOSDelaysReport(args.prefixes, 'defaults.n', args.values, results_folder=args.results_folder)
    pr.generate(args.report_file)
