from json import encoder
from argparse import ArgumentParser

from lama_exp.utils.arg import ArgTypes

from lama_exp.reports.provisioning.base import ProvisioningReport

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

encoder.FLOAT_REPR = lambda o: format(o, '.2f')


class ProvisioningFrameworkReport(ProvisioningReport):

    def __init__(self, name, parameter, values, results_folder='results'):
        super().__init__(name, parameter, values, 'provisioning_report_template.html', results_folder)

    def get_charts(self):
        charts = {
            'resp_time_per_n': [self.generate_call_and_active_lines(v) for v in self.values],
            'resp_time_all_n': [
                self.generate_with_all(field='rt_call', title='API call resp. time'),
                self.generate_with_all(field='rt_active', title='Time to become active'),
                self.generate_hosts_used_all(),
                self.generate_agg_with_error(),
                self.generate_errors_all()
            ]

        }

        return charts


if __name__ == "__main__":
    ap = ArgumentParser(description="Generate Provisioning report from data.")
    ap.add_argument('prefix', type=str,
                    help="Prefix of files to use (normally name of yaml file used to configure the experiment)")
    ap.add_argument('values', type=ArgTypes.int_list,
                    help="Variable to vary")
    ap.add_argument('-o', dest='report_file', type=ArgTypes.writeable_filename, default=None,
                    help="Filename to save the report.")
    ap.add_argument('--results-folder', dest='results_folder', type=str,
                    help="Path for the results", default='results')

    args = ap.parse_args()
    if not args.report_file:
        args.report_file = 'reports/prov_%s.html' % args.prefix
    pr = ProvisioningFrameworkReport(args.prefix, 'defaults.n', args.values, results_folder=args.results_folder)
    pr.generate(args.report_file)
