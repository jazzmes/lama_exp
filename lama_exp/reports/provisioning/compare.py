import json
from json import encoder
from argparse import ArgumentParser, ArgumentTypeError

from lama_exp.utils.arg import ArgTypes
from operator import itemgetter
from statistics import mean
from csv import DictReader

from lama_exp.reports.provisioning.base import ProvisioningReport, HighChartsColors, DarkerHighChartsColors, \
    HighChartsColorsRGB
from os import walk, path, access, W_OK
from datetime import datetime

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

encoder.FLOAT_REPR = lambda o: format(o, '.2f')


class ProvisioningCompareReport(ProvisioningReport):

    Prefixes = [
        'sequential',
        'parallel',
        'parallel_multiuser'
    ]

    def __init__(self, names, parameter, values, results_folder="results", print_names=None):
        self.names = names
        super().__init__(names, parameter, values, 'provisioning_compare_report_template.html', results_folder, print_names)

        self.print_names = print_names
        self.parameter = parameter
        self.values = values

        # self.files = self.load_files(results_folder)

    def load_files(self, results_folder="results"):
        files = {}
        for name in self.names:
            for prefix in self.Prefixes:
                file_prefix = "%s_%s" % (prefix, name)
                # print("file_prefix=%s" % file_prefix)
                # print(path.join(results_folder, file_prefix))
                for d in walk(results_folder):
                    # print(d[0])
                    if d[0].startswith(path.join(results_folder, file_prefix)):
                        # print("starts with prefix: %s" % (d[0],))
                        for file in d[2]:
                            try:
                                parts = file.split('.')
                                file_type = '.'.join(parts[-3:-1])
                                preamble = '.'.join(parts[:-3])

                                prefix, params, ts = preamble.split('-')
                                # print("parts: prefix=%s, params=%s, ts=%s" % (prefix, params, ts))
                                params = params.split('_')
                            except:
                                print("Unsupported filename: %s" % file)
                                continue
                            value = None
                            # print("chosen_parameter: %s" % self.parameter)
                            for p in params:
                                # print("\tp: %s" % p)
                                if p.startswith(self.parameter):
                                    value = int(p[len(self.parameter):])
                                    break

                            # print("\tfile_type: %s" % file_type)
                            # print("\tvalue: %s" % value)
                            # par, time, type1, type2, ext = file[len(self.name):].split('_', maxsplit=1)[1].split('.')
                            # print(par, time, type1, type2, ext)
                            # value = int(par.strip(self.parameter))
                            # file_type = '.'.join([type1, type2])
                            # file_entry = files.get(file_type, {}).get(file_prefix, {}).get(value)
                            # if not file_entry or file_entry.get('time') < ts:
                            #     files.setdefault(file_type, {}).setdefault(file_prefix, {})[value] = {
                            #         'filename': path.join(d[0], file),
                            #         'time': ts
                            #     }
                            # print(path.join(d[0], file))
                            file_entries = files.setdefault(file_type, {}).setdefault(file_prefix, {}).setdefault(value, [])
                            if len(file_entries) < self.num_of_results:
                                file_entries.append(
                                    {
                                        'filename': path.join(d[0], file),
                                        'time': ts
                                    }
                                )
                            elif file_entries[0].get('time') < ts:
                                sorted(file_entries, key=itemgetter('time'))
                                file_entries[0] = {
                                    'filename': path.join(d[0], file),
                                    'time': ts
                                }
        return files

    def generate_agg_compare_with_error(self, experiment_type, include_failures=False):
        all_series = []
        categories = sorted(self.values)
        file_type = 'response_times.raw'
        if file_type not in self.files:
            print("No '%s' files available" % file_type)
            return

        ym = 0
        for j, framework in enumerate(self.names):
            prefix = "%s_%s" % (experiment_type, framework)
            prefix_files = self.files.get(file_type, {}).get(prefix)
            # print("prefix=%s, exp=%s" % (prefix, experiment_type))
            # print("prefix_files=%s" % prefix_files)
            if not prefix_files:
                continue

            sline_active = []
            serror_active = []
            sline_call = []
            serror_call = []
            sline_diff = []
            if include_failures:
                errors = []

            for v in sorted(self.values):

                agg_values = {}
                total_samples = len(prefix_files.get(v, {}))
                for entry in prefix_files.get(v, {}):
                    filename = entry.get('filename')
                    # filename = prefix_files.get(v, {}).get('filename')
                    print("Processing filename=%s" % filename)
                    if not filename:
                        print("No filename for type=%s, value=%s" % (file_type, v))
                        continue
                    values = {
                        's_active': [],
                        's_call': [],
                    }
                    n_errors = 0
                    with open(filename, 'r') as f:
                        reader = DictReader(f)
                        for i, r in enumerate(reader):
                            values['s_active'].append(round(float(r['rt_active']), 2) if r['rt_active'] else None)
                            values['s_call'].append(round(float(r['rt_call']), 2) if r.get('rt_call') else None)
                            if include_failures and (not r['success'] or r['success'] != 'True'):
                                    n_errors += 1
                    # values['s_active'] = [r for r in values['s_active'] if r is not None]
                    # values['s_call'] = [r for r in values['s_call'] if r is not None]

                    for k, arr in values.items():
                        agg_values.setdefault(k, [0] * len(arr))
                        for i, val in enumerate(arr):
                            # TODO: handle the case where v is None
                            if val:
                                agg_values[k][i] += val / total_samples

                    agg_values['errors'] = agg_values.setdefault('errors', 0) + n_errors / total_samples

                sline_active.append(round(mean(agg_values['s_active']), 2) if agg_values.get('s_active') else None)
                serror_active.append([min(agg_values['s_active']), max(agg_values['s_active'])] if agg_values.get('s_active') else None)
                sline_call.append(round(mean(agg_values['s_call']), 2) if agg_values.get('s_call') else None)
                serror_call.append([min(agg_values['s_call']), max(agg_values['s_call'])] if agg_values.get('s_call') else None)
                sline_diff.append(round(sline_active[-1] - sline_call[-1], 2) if agg_values.get('s_active') else None)
                if include_failures:
                    errors.append(agg_values['errors'])

            # set yaxis max
            ym = int(max(x[1] for v in [serror_active, serror_call] for x in v if x) / 10 + 1) * 10
            series = [
                # {
                #     'name': 'RT Call AVG (%s)' % prefix,
                #     'data': sline_call
                # },
                # {
                #     'name': 'RT Call Error (%s)' % prefix,
                #     'data': serror_call,
                #     'type': 'errorbar',
                # },
                {
                    'type': 'column',
                    'name': 'RT Active AVG (%s)' % self.print_names.get(framework, framework),
                    'data': sline_active,
                    'color': HighChartsColors[j]
                },
                {
                    'type': 'errorbar',
                    'name': 'RT Active Error (%s)' % self.print_names.get(framework, framework),
                    'data': serror_active,
                },
                # {
                #     'name': 'Diff AVG (%s)' % prefix,
                #     'data': sline_diff
                # }
            ]

            if include_failures:
                series.append({
                    'type': 'scatter',
                    'name': 'Errors (%s)' % self.print_names.get(framework, framework),
                    'data': errors,
                    'yAxis': 1,
                    'color': DarkerHighChartsColors[j],
                    # 'marker': {
                        # 'lineWidth': 2,
                        # 'lineColor': HighChartsColors[j],
                    # },
                })

            all_series += series
        chart = {
            'chart': {
                'zoomType': 'xy'
            },
            # 'title': 'Total Requests (n) = %s' % value,
            'xAxiscategories': [{
                '': categories,
                'title': {'text': '# Instances'},
                'crosshair': True
            }],
            'yAxis': [
                {
                    'labels': {
                      'format': '{value}s'
                    },
                    'title': {'text': 'Time (s)'},
                    'max': ym,
                    'min': 0
                },
            ],
            'series': all_series,
            'tooltip': {'shared': [True, True], 'valueSuffix': 's'}
        }
        if include_failures:
            chart['yAxis'].append(
                {
                    'title': {'text': '# Failed VMs'},
                    'opposite': True
                }
            )
        return {
            'name': 'resp_time_agg_%s' % experiment_type,
            'title': 'Time to VM Active (%s)' % experiment_type,
            'json': json.dumps(chart)
        }

    def generate_agg_error(self, experiment_type, include_failures=False):
        all_series = []
        categories = sorted(self.values)
        file_type = 'response_times.raw'
        if file_type not in self.files:
            print("No '%s' files available" % file_type)
            return

        ym = 0
        for framework in self.names:
            prefix = "%s_%s" % (experiment_type, framework)
            prefix_files = self.files.get(file_type, {}).get(prefix)
            if not prefix_files:
                continue

            errors = []
            for v in sorted(self.values):

                agg_values = {}
                total_samples = len(prefix_files.get(v, {}))
                for entry in prefix_files.get(v, {}):
                    filename = entry.get('filename')
                    # filename = prefix_files.get(v, {}).get('filename')
                    print("Processing filename=%s" % filename)
                    if not filename:
                        print("No filename for type=%s, value=%s" % (file_type, v))
                        continue

                    n_errors = 0
                    n_count = 0
                    with open(filename, 'r') as f:
                        reader = DictReader(f)
                        for i, r in enumerate(reader):
                            if not r['success'] or r['success'] != 'True':
                                n_errors += 1
                            n_count += 1
                        # print("v=%s, errors=%s, count=%s" % (v, n_errors, n_count))
                        n_errors += v - n_count
                    agg_values['errors'] = agg_values.setdefault('errors', 0) + n_errors / total_samples
                # errors.append(n_errors)
                errors.append(agg_values['errors'])
            # set yaxis max
            series = [
                {
                    'name': 'Errors (%s)' % self.print_names.get(framework, framework),
                    'data': errors
                }
            ]

            all_series += series
        return {
            'name': 'error_agg_%s' % experiment_type,
            'title': 'Failure to Allocate (%s)' % experiment_type,
            'json': json.dumps({
                # 'title': 'Total Requests (n) = %s' % value,
                'chart': {
                    'type': 'column',
                    'zoomType': 'xy'
                },
                'xAxis': [{
                    'categories': categories,
                    'title': {'text': '# Instances'},
                    'crosshair': True
                }],
                'yAxis': [
                    {
                        'title': {'text': '# Errors'},
                        'min': 0
                    }
                ],
                'series': all_series,
                'tooltip': {'shared': [True, True], 'valueSuffix': 's'}
            })
        }

    def generate_agg_compare_times(self, experiment_type, n):
        all_series = []
        file_type = 'response_times.raw'
        if file_type not in self.files:
            print("No '%s' files available" % file_type)
            return
        ymax = 0
        for j, framework in enumerate(self.names):
            prefix = "%s_%s" % (experiment_type, framework)
            prefix_files = self.files.get(file_type, {}).get(prefix)
            # print("prefix=%s, exp=%s" % (prefix, experiment_type))
            # print("prefix_files=%s" % prefix_files)
            if not prefix_files:
                continue

            ranges = []
            averages = []
            filename = prefix_files.get(n, {})[-1].get('filename')
            if not filename:
                print("No filename for type=%s, value=%s" % (file_type, n))
                continue

            starts = []
            calls = []
            actives = []
            with open(filename, 'r') as f:
                reader = DictReader(f)
                for i, r in enumerate(reader):
                    starts.append(datetime.strptime(r['ts_start'], "%Y-%m-%d %H:%M:%S.%f")) # 2016-04-21 16:41:05.517637
                    calls.append(round(float(r['rt_call']), 2) if r['rt_active'] else None)
                    actives.append(round(float(r['rt_active']), 2) if r['rt_active'] else None)

            exp_start = min(starts)
            # assume storted: TODO: sort
            i = 1
            for start, call, active in zip(starts, calls, actives):
                # ts = round(start.timestamp() * 1000)
                req_start = (start - exp_start).total_seconds()
                ranges.append([i, req_start, req_start + active if active else (req_start + call if call else req_start)])
                averages.append([i, req_start + call if call else req_start])
                i += 1

            # set yaxis max
            ymax = max(ymax, max(x[2] for x in ranges))
            # noinspection PyTypeChecker
            series = [
                {
                    'name': '%s' % self.print_names[framework],
                    'data': averages,
                    'zIndex': 1,
                    'marker': {
                        'fillColor': 'white',
                        'lineWidth': 2,
                        'lineColor': HighChartsColors[j],
                        'enabled': True
                    },
                    'color': HighChartsColors[j],
                    'dashStyle': 'dash'
                },
                {
                    'name': '%s  Start-Active' % self.print_names[framework],
                    'data': ranges,
                    'type': 'arearange',
                    'lineWidth': 1,
                    'linkedTo': ':previous',
                    'color': HighChartsColors[j],
                    'lineColor': HighChartsColors[j],
                    'fillColor': "rgba(%s)" % ','.join(HighChartsColorsRGB[j] + ['0.3']),
                    # 'fillOpacity': 0.3,
                    'zIndex': 0
                },
            ]
            all_series += series

        return {
            'name': 'resp_time_range_%s_%s' % (experiment_type, n),
            'title': 'Experiment: %s, n: %s' % (experiment_type, n),
            'json': json.dumps({
                'chart': {
                    'zoomType': 'xy'
                },
                # 'title': 'Total Requests (n) = %s' % value,
                # 'xAxiscategories': [{
                #     'title': {'text': '# Instances'},
                #     'crosshair': True
                # }],
                #
                'xAxis': {
                    'title': {'text': 'Instances No.'},
                    # 'type': 'datetime'
                },
                'yAxis': {
                    'title': {'text': 'Start-Call-Active Time (s)'},
                    'max': ymax,
                    'min': 0
                },
                'tooltip': {
                    'crosshairs': True,
                    'shared': True,
                    'split': True,
                    'valueSuffix': 's'
                },
                'series': all_series,
            })
        }

    def get_charts(self):
        charts = {
            # 'resp_time_per_n': [self.generate_call_and_active_lines(v) for v in self.values],
            'resp_time_all': [self.generate_agg_compare_with_error(prefix, include_failures=False) for prefix in self.Prefixes],
            'errors_all': [self.generate_agg_error(prefix) for prefix in self.Prefixes],
            'resp_time_range': [self.generate_agg_compare_times(prefix, v) for v in self.values[1:] for prefix in self.Prefixes]
        }

        return charts

    def load_variables(self):
        self.variables = {
            'title': "Framework comparison (%s)" % ', '.join(self.names),
            'charts': self.get_charts()
        }

    # TODO] include a graph with number of hosts...


if __name__ == "__main__":
    ap = ArgumentParser(description="Generate Provisioning report from data.")
    ap.add_argument('names', type=ArgTypes.str_list,
                    help="Names of the frameworks to compare")
    ap.add_argument('values', type=ArgTypes.int_list,
                    help="Variable to vary")
    ap.add_argument('-o', dest='report_file', type=ArgTypes.writeable_filename, default=None,
                    help="Filename to save the report.")
    ap.add_argument('--results-folder', dest='results_folder', type=str,
                    help="Path for the results", default='results')

    args = ap.parse_args()
    if not args.report_file:
        args.report_file = 'reports/prov_compare_%s.html' % '_'.join(args.names)
    pr = ProvisioningCompareReport(args.names, 'defaults.n', args.values, results_folder=args.results_folder, print_names={
        'lama': 'LAMA',
        'os': 'OpenStack'
    })
    pr.generate(args.report_file)
