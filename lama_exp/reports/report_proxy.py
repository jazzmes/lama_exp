import logging
import traceback
import sys
from argparse import ArgumentParser
from pandas import json
from os import path

from flask import Flask, request, jsonify
from threading import Thread
from lama_exp.utils.json import EnhancedJSONDecoder, EnhancedJSONEncoder

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


logger = logging.getLogger(__name__)


def report_server(handler, port):
    logger.info("Starting webserver!")

    app = Flask(__name__)
    app.json_encoder = EnhancedJSONEncoder
    app.json_decoder = EnhancedJSONDecoder

    def shutdown_server():
        func = request.environ.get('werkzeug.server.shutdown')
        logger.debug("Func, the whole func and nothing but the func: %s", func)
        if func is None:
            raise RuntimeError('Not running with the Werkzeug Server')
        try:
            func()
        except:
            logger.error("Traceback: %s", traceback.format_exc())

    @app.route('/get', methods=['POST'])
    def get_data():
        logger.debug("Request: %s", request)
        logger.debug("Json: %s", request.get_json(force=True))
        indices = request.get_json(force=True)
        logger.debug("Indices: %s", indices)
        res = handler.get_data(indices)
        logger.debug("Get res: %s", res)
        res = res or {}
        return jsonify(**res)

    @app.route('/data', methods=['POST'])
    def post_data():
        data = request.get_json(silent=True)
        logger.debug("Post data: %s", data)
        return jsonify(handler.post_data(**data))

    @app.route('/shutdown', methods=['POST', 'GET'])
    def shutdown():
        try:
            logger.debug("Shutting down!")
            shutdown_server()
        except:
            logger.error("Traceback: %s", traceback.format_exc())
        return 'Server shutting down...'

    app.run(host='0.0.0.0', port=port)
    logger.debug("out of flask stuff...")


class ReportHandler(object):

    def __init__(self, report_dir_path=None):
        self._all_data = self._last_data = {}
        self._report_dir_path = report_dir_path

    @staticmethod
    def _check_conditions(series_name, conditions):
        for condition in conditions:
            if all((key in series_name) == result for key, result in condition.items()):
                return True
        return False

    def get_data(self, request_data=None):
        indices = request_data.get('indices', {}) if request_data else {}
        conditions = request_data.get('conditions', []) if request_data else []
        if isinstance(conditions, dict):
            conditions = [conditions]
        logger.debug("Indices: %s, Conditions: %s", indices, conditions)
        d = {'indices': {}}
        d_indices = d['indices']
        d_series = {}
        d['series'] = d_series
        for data_type, data in self._all_data.items():
            tmp = d if data_type == 'events' else d_series

            if indices and indices.get(data_type):
                tmp[data_type] = data[indices[data_type]:]
                d_indices[data_type] = len(data)
            elif data_type == 'events':
                tmp[data_type] = data
                d_indices[data_type] = len(data)
            else:
                res = self._check_conditions(data_type, conditions)
                # logger.debug("checked conditions: name=%s, conditions=%s, result=%s", data_type, conditions, res)
                if res:
                    tmp[data_type] = data
                    d_indices[data_type] = len(data)
        return d

    def post_data(self, data_type, data):
        logger.debug("Post data: %s - %s", data_type, data)
        sec = self._all_data.setdefault(data_type, [])
        sec.append(data)
        return {"result": True}

    def save_data(self):
        logger.debug("Saving data. Directory: %s", self._report_dir_path)
        if self._report_dir_path:
            data_file = path.join(self._report_dir_path, 'data.json')
            logger.debug("Saving data. File: %s", data_file)
            with open(data_file, 'w') as f:
                f.write(json.dumps(self._all_data))


class ReportServer(Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition."""

    def __init__(self, port):
        super().__init__(target=report_server, args=(ReportHandler(), port))

    # def shutdown(self):
    #     logger.debug("Sending post request...")
    #     res = requests.post("http://127.0.0.1:40900/shutdown")
    #     logger.info("Result of shutdown: %s %s", res.status_code, res.reason)


class ReportServerBlocking(object):

    def __init__(self, port, handler=None):
        self._port = port
        self._handler = handler or ReportHandler()

    def start(self):
        report_server(self._handler, self._port)


if __name__ == "__main__":
    logger.setLevel(logging.DEBUG)
    stream_handler = logging.StreamHandler(stream=sys.stdout)
    stream_handler.setFormatter(
        logging.Formatter("%(asctime)s : %(levelname)5.5s : %(module)14.14s [%(lineno)3.3s] : %(message)s")
    )
    logging.root.addHandler(stream_handler)
    logging.root.setLevel(logging.DEBUG)

    ap = ArgumentParser(description="Report Proxy")
    ap.add_argument("port", type=int, help="Proxy port")
    args = ap.parse_args()

    rs = ReportServer(port=args.port)
    logger.debug("Start Report proxy")
    rs.start()
    logger.debug("Stop report proxy")
