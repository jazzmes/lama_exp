import logging
import traceback

import requests
from jinja2 import Environment, PackageLoader
from threading import Thread
from flask import Flask, request, jsonify
from lama_exp.utils.json import EnhancedJSONDecoder, EnhancedJSONEncoder

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


logger = logging.getLogger(__name__)


class Report(object):
    env = Environment(loader=PackageLoader('lama_exp', 'templates'))

    def __init__(self, template_name="report_template.html"):
        self.html = None
        self.template_name = template_name
        self.variables = {}

    def base_html(self):
        t = Report.env.get_template(self.template_name)
        self.load_variables()
        # print("variables: %s" % self.variables)
        self.html = t.render(**self.variables)

    def generate(self, output_file):
        self.base_html()
        print("Writing report file: %s" % output_file)
        with open(output_file, 'w') as f:
            f.write(self.html)

    def load_variables(self):
        raise NotImplementedError()


def report_server(handler, port):
    logger.info("Starting webserver!")

    app = Flask(__name__)
    app.json_encoder = EnhancedJSONEncoder
    app.json_decoder = EnhancedJSONDecoder

    def shutdown_server():
        func = request.environ.get('werkzeug.server.shutdown')
        logger.debug("Func, the whole func and nothing but the func: %s", func)
        if func is None:
            raise RuntimeError('Not running with the Werkzeug Server')
        try:
            func()
        except:
            logger.error("Traceback: %s", traceback.format_exc())

    @app.route('/get_data', methods=['POST'])
    def get_data():
        res = handler.get_data()
        res = res or {}
        return jsonify(**res)

    @app.route('/shutdown', methods=['POST'])
    def shutdown():

        shutdown_server()
        return 'Server shutting down...'

    app.run(host='0.0.0.0', port=port)
    logger.info("out of flask stuff...")


class ReportServer(Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition."""

    def __init__(self, port, **handlers):
        super().__init__(target=report_server, args=(self, port))
        self._handlers = handlers

    def get_data(self):
        h = self._handlers.get('get_data')
        if h:
            try:
                return h()
            except:
                logger.error("Error running 'get_data' handler: %s", traceback.format_exc())

    def shutdown(self):
        logger.debug("Sending post request...")
        res = requests.post("http://127.0.0.1:40900/shutdown")
        logger.info("Result of shutdown: %s %s", res.status_code, res.reason)
