#! /usr/bin/env python3
import logging
import sys
import traceback
import csv
import subprocess
import json
from enum import Enum
from datetime import datetime
from os import path, getcwd, mkdir
from argparse import ArgumentParser

import requests

from lama_exp.experiments.base import load_yaml_filename, apply_var, creatable_or_existing_dir, vars_dict, yaml_filename, \
    LamaModule, LamaExpController, ExperimentReport
from lama_exp.reports.scenario.live_profile import LiveProfileReport, ReportProxyPort

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.2'
__email__ = 'tcarvalho@cmu.edu'


logger = logging.getLogger(__name__)
# use the main experiment module

# add specific collectors


class ExperimentException(Exception):
    pass


class Event(object):

    class Type(Enum):
        ExperimentStart = 1
        ExperimentEnd = 2
        AppAPIRequest = 3
        AppActive = 4
        InstanceActive = 5
        InstanceReady = 6
        AppClientLaunched = 7

    def __init__(self, event_type, timestamp, **fields):
        self.event_type = event_type
        self.timestamp = timestamp
        self.fields = fields

    @property
    def fields_str(self):
        return "|".join(["%s=%s" % (k, v) for k, v in self.fields.items()])

    def __str__(self):
        return "%s@%s [%s]" % (
            self.event_type,
            self.timestamp,
            ", ".join(["%s=%s" % (k, v) for k, v in self.fields.items()])
        )


class LamaEventCollector(object):

    def __init__(self, name=None, ref_start=None):
        # noinspection PyTypeChecker
        self._name = name or "exp-%s" % datetime.strftime(datetime.now(), "%Y%m%d%H%M%S")
        self._ref_start = ref_start
        self._ref_end = None

        self._events = []
        self._apps = {}

        self._report_proxy = None

    def write_to_disk(self, folder_path=getcwd(), ref_end=None):
        # create folder with name of experiment
        folder_path = path.join(folder_path, self._name)
        try:
            mkdir(folder_path)
        except OSError as e:
            logger.debug("OSError on mkdir: %s", e)

        # create event file:
        filename = "%s.events.csv" % self._name
        filepath = path.join(folder_path, filename)
        # logger.debug("Events: %s", self._events)
        logger.debug("Events timestamps: %s", [str(e) for e in self._events])

        sorted_events = sorted(self._events, key=lambda x: x.timestamp)
        with open(filepath, "w") as f:
            w = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
            w.writerow(['timestamp', 'event_type', 'details'])
            w.writerows([(e.timestamp, e.event_type.name, e.fields_str) for e in sorted_events])
        logger.info("wrote: %s", filename)

    def register_active_instance(self, id, name, node, timestamp, success=True):
        event = Event(
            event_type=Event.Type.InstanceActive,
            timestamp=timestamp,
            id=id,
            name=name,
            host=node,
            success=success
        )
        logger.debug("Register event: %s", event)
        self._events.append(event)

        if self._report_proxy:
            self._report_proxy.post_event(event)

    def register_ready_instance(self, id, name, node, timestamp, success=True):
        event = Event(
            event_type=Event.Type.InstanceReady,
            timestamp=timestamp,
            id=id,
            name=name,
            host=node,
            success=success
        )
        logger.debug("Register event: %s", event)
        self._events.append(event)

        if self._report_proxy:
            self._report_proxy.post_event(event)

    def register_requested_app(self, app_name, success):
        event = Event(
            event_type=Event.Type.AppAPIRequest,
            timestamp=datetime.now(),
            name=app_name,
            success=success
        )
        logger.debug("Register event: %s", event)
        self._events.append(event)

        self._apps[app_name] = {
            'success': True
        }

        if self._report_proxy:
            self._report_proxy.post_event(event)

    def register_active_app(self, app_name, timestamp):
        event = Event(
            event_type=Event.Type.AppActive,
            timestamp=timestamp,
            name=app_name
        )
        logger.debug("Register event: %s", event)
        self._events.append(event)

        self._apps.setdefault(app_name, {})['active'] = timestamp

        if self._report_proxy:
            self._report_proxy.post_event(event)

    def register_end(self):

        self._ref_end = datetime.now()

        event = Event(
            event_type=Event.Type.ExperimentEnd,
            timestamp=self._ref_end
        )

        self._events.append(event)

        self._report_proxy and self._report_proxy.post_event(event)

    def register_start(self):
        self._ref_start = self._ref_start or datetime.now()

        event = Event(
            event_type=Event.Type.ExperimentStart,
            timestamp=self._ref_start
        )
        self._events.append(event)

        if self._report_proxy:
            self._report_proxy.post_event(event)

    def register_client_launched(self):
        event = Event(
            event_type=Event.Type.AppClientLaunched,
            timestamp=datetime.now()
        )
        self._events.append(event)

        if self._report_proxy:
            self._report_proxy.post_event(event)

    def register_event(self, **args):
        event = Event(**args)
        if self._report_proxy:
            self._report_proxy.post_event(event)

    def set_live_report(self, report_proxy):
        self._report_proxy = report_proxy

    @property
    def events(self):
        return self._events

    @property
    def wait_on_instances(self):
        return False


class MonitoringLiveReport(ExperimentReport):

    LaunchServer = True

    def __init__(self, module):
        super(MonitoringLiveReport, self).__init__(module)
        self.report = LiveProfileReport(name='live_profile', data_request_callback=self.get_data)
        self._events_next_index = 0

    def before_start(self):
        logger.debug("Report - before start - create base report")
        try:
            self.report.generate('reports/scenario_live_profile.html')
        except:
            logger.error("Error generating report: %s", traceback.format_exc())

        subprocess.call(['open', '/Users/tiago/Projects/PycharmProjects/lama_exp/reports/scenario_live_profile.html'])

    def after_stop(self):
        logger.debug("Report - after stop - wrap up")
        self.report.stop()
        logger.debug("out of join")

    def post_event(self, event):
        logger.debug("Event date: %s", event.timestamp)
        logger.debug("Event timestamp: %s", event.timestamp.timestamp())
        tmps = datetime.strptime(str(event.timestamp), "%Y-%m-%d %H:%M:%S.%f")
        logger.debug("Event ts through str: %s -> %s", tmps, tmps.timestamp())
        timestamp = int(event.timestamp.timestamp() * 1000)
        logger.debug("Post timestamp: %s", timestamp)
        requests.post("http://127.0.0.1:%s/data" % ReportProxyPort, json={
            'data_type': 'events',
            'data': {
                'x': timestamp,
                'name': LiveProfileReport.EventTypeCategories[event.event_type.name],
                'title': LiveProfileReport.EventTypeTitles[event.event_type.name],
                'text': "%s%s" % (
                    event.event_type.name,
                    ' - %s' % ', '.join(["%s=%s" % (k, v) for k, v in event.fields.items()]) if event.fields else ''
                )
            }
        })

    def get_data(self):
        try:
            min_index = self._events_next_index
            max_index = len(self.module.collector.events)
            events = []
            for event in self.module.collector.events[min_index:max_index]:
                try:
                    events.append({
                        'x': int(event.timestamp.timestamp() * 1000),
                        'name': LiveProfileReport.EventTypeCategories[event.event_type.name],
                        'title': LiveProfileReport.EventTypeTitles[event.event_type.name],
                        'text': "%s%s" % (
                            event.event_type.name,
                            ' - %s' % ', '.join(["%s=%s" % (k, v) for k, v in event.fields.items()]) if event.fields else ''
                        )
                    })
                except KeyError:
                    logger.error("Not supported event: %s", traceback.format_exc())
            data = {}
            if events:
                data['events'] = events
            logger.debug("Data to return: %s", data)
            self._events_next_index = max_index
            return data
        except:
            logger.error("Error getting data: %s", traceback.format_exc())


class LamaMonitoringExpModule(LamaModule):
    logger.debug("Set CUSTOM collector")
    EventCollector = LamaEventCollector
    # ExperimentReport = MonitoringLiveReport

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.collector.set_live_report(self.report)

    def preprocessing(self):
        logger.debug("Bypassing preprocessing")

    def process_result(self, results):
        logger.debug("Process results: %s", results)
        if self.collector:
            for result in results.get('results'):
                action = result.get('action')
                if not action:
                    logger.error("Result does not indicate action: %s", result)
                    continue
                elif action == 'create_app':
                    self.collector.register_requested_app(
                        result.get('app_name'),
                        result.get('success')
                    )
                    self.event_publisher.publish(
                        'lama_events:experiments',
                        'app_requested',
                        datetime.now(),
                        result.get('app_name'),
                        result.get('provider'),
                        result.get('success')
                    )
                elif action == 'launch_client':
                    self.event_publisher.publish(
                        'lama_events:experiments',
                        'client_launched',
                        datetime.now(),
                        result.get('app_name'),
                        result.get('success')
                    )
                elif action == 'failure':
                    self.event_publisher.publish(
                        'lama_events:experiments',
                        'failure',
                        datetime.now(),
                        result.get('timestamp_trigger'),
                        result.get('type'),
                        result.get('app_name'),
                        result.get('host'),
                        result.get('instance_name'),
                        result.get('success'),
                        json.dumps(result.get('details'))
                    )
                else:
                    logger.error("Not implemented for specified action - process_result: %s", result)


# if self._event_publisher:
#     self._event_publisher.publish('lama_events:experiments', 'client_launched', datetime.now())
#
# if self._event_publisher:
#     self._event_publisher.publish('lama_events:experiments', 'failure', datetime.now(),
#                                   "Instance Crash: {}".format(instance_name))


# class LamaExpController(object):
#
#     def __init__(self, config):
#         self.name = 'monitoring'
#
#         self._exp_module = LamaMonitoringExpModule(
#             expconfig=config.expconfig,
#             clients_file=config.clients_filename,
#             collect=config.collect,
#             listener=config.listener,
#             name=self.name,
#             results_dir=config.results_dir,
#             monitor_period=config.monitor_period
#         )
#
#     def start(self):
#         self._exp_module.start()


def main():
    logging.root.setLevel(logging.DEBUG)
    logger.setLevel(logging.DEBUG)

    stream_handler = logging.StreamHandler(stream=sys.stdout)
    stream_handler.setFormatter(
        logging.Formatter("%(asctime)s : %(levelname)5.5s : %(module)14.14s [%(lineno)3.3s] : %(message)s")
    )
    logging.root.addHandler(stream_handler)

    ap = ArgumentParser(description="Script to run an experiment in eithter OpenStack or LAMA environments. "
                                    "According to configuration will: start the clients, monitoring services, "
                                    "collect the results and/or generate a report.")
    ap.add_argument('-d', dest='clients_filename', type=str,
                    help="Distributed clients: it will launch clients spread out "
                         "through the network instead of using a single central client."
                         "Hosts must ssh-accessible using key for user 'lama'")
    # workflow options
    ap.add_argument('-m', dest='monitor', action='store_true',
                    help='Start up monitoring scripts (with CSV option and listener).')
    ap.add_argument('-t', dest='trace', action='store_true',
                    help='Raise exception for debug')
    ap.add_argument('--vars', dest='yaml_vars', type=vars_dict,
                    help='Define variables to be used in the yaml file.')
    ap.add_argument('--res_dir', dest='results_dir', type=creatable_or_existing_dir,
                    default=path.join(getcwd(), 'results'),
                    help='Results folder.')
    group = ap.add_mutually_exclusive_group()
    group.add_argument('-l', dest='listener', action='store_true',
                       help='Run as client/listener (can receive requests from server)')
    group.add_argument('-s', dest='config_filename', type=yaml_filename,
                       help='Path for the YAML filename containing configuration options '
                            '(schedule of events, credentials, instance options, etc.)')
    # group.add_argument('-p', dest='monitor_period', type=int,
    #                    help='Monitor period in seconds', default=0)
    # experiment type
    # will use for several types of experiments
    args = ap.parse_args()

    args.expconfig = None
    if args.config_filename:
        args.expconfig = load_yaml_filename(args.config_filename)
        if args.yaml_vars:
            for k, v in args.yaml_vars.items():
                apply_var(args.expconfig, k, v)

    # logger.warn(args)
    try:
        logger.info("Args: %s", args)
        lef = LamaExpController(args, module_cls=LamaMonitoringExpModule, module='lama', collect=True)
        if lef:
            lef.start()
    except ExperimentException as e:
        if args.trace:
            print("Error during execution of the experiment")
            for line in traceback.format_exc().splitlines():
                print("\tERROR\t%s" % line)
        else:
            print("Error: %s" % e)


if __name__ == '__main__':
    main()
