#! /usr/bin/env python3
import logging
import netifaces
import signal
import traceback
import socket
import random
from argparse import ArgumentParser, ArgumentTypeError
from ast import literal_eval
from datetime import datetime, timedelta
from multiprocessing import Pool, Lock, current_process, Manager
# noinspection PyUnresolvedReferences
from socket import error as socket_error, gethostname
from threading import current_thread
from time import sleep
import sys
import uuid

from functools import partial
from random import randint, sample
from os import getpid, path, getcwd, mkdir
import string

import requests
import yaml
from paramiko.client import SSHClient, AutoAddPolicy
from paramiko.ssh_exception import BadHostKeyException, SSHException, AuthenticationException
from paramiko.buffered_pipe import PipeTimeout
from copy import deepcopy
from netaddr import IPAddress, IPSet
from netaddr.core import AddrFormatError

from lama_exp.clients.lama import LamaClient
from lama_exp.clients.nova import OSClient, CreateInstanceException
from lama_exp.clients.thanatos import ThanatosClient
from lama_exp.collectors import Collector, OSCollector
from lama_exp.listener import listener_server
from lama_exp.scheduler import EventScheduler
from lama_exp.subscribers import OSSubscriberThread, LamaSubscriberThread, FrameworkEventTypes, LamaSubscriber
from lama_exp.utils.json import EnhancedJSONDecoder
from lama_monitor.engine import Engine
from lama_monitor.proc_plugins import AllProcMonitorPlugin
from lama_monitor.output_plugins import CSVOutput
# from logging_tree import printout

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

logger = logging.getLogger(__name__)

ListenerPort = 58228
ValidChars = "_-.%s%s" % (string.ascii_letters, string.digits)

manager = Manager()
g_instances = manager.dict()
g_providers = manager.dict()
g_active_instances = manager.dict()


class ClientNotFoundException(Exception):
    pass


class CmdNotFoundException(Exception):
    pass


def get_ip_addresses():
    ipset = IPSet()

    interfaces = netifaces.interfaces()
    for i in interfaces:
        iface = netifaces.ifaddresses(i).get(netifaces.AF_INET)
        if iface:
            for j in iface:
                ipset.add(j['addr'])
    return ipset


# def task_check_and_launch_monitoring(client):
#     logger.debug("Client inside task_check_and_launch_monitoring: %s", client)
#     client.login()
#     # if not client.monitor_status():
#     #     client.start_monitor()
#     # client.logout()

def start_remote_client(client):
    client.init()
    pid = client.run_client()
    client.close()
    return pid


def stop_remote_client(client):
    client.init()
    res = client.stop_client()
    client.close()
    return res


def collect_monitor_file(local_path, client):
    client.init()
    res = client.collect_monitor_file(local_path)
    client.close()
    return res


def run_action(action, client):
    logger.error("run_action is working")
    res = {'results': []}
    try:
        logger.debug("/--- action: %s", getpid())
        logger.debug("run_action args: client=%s, task=%s", client, action)
        res = client.execute_action(**action)
        logger.debug("\--- action: %s", getpid())
    except:
        logger.error("Error on worker: %s", traceback.format_exc())
    return res


class ExpHost(object):
    def check_monitor_status(self):
        logger.info("@%s::check_monitor_status", self.__class__.__name__)
        return False

    def start_monitor(self):
        logger.info("@%s::start_monitor", self.__class__.__name__)

    def init(self):
        pass

    def close(self):
        pass


class RemoteExpHost(ExpHost):
    # defaults
    MaxLoginRetries = 3

    def __init__(self, module, ip, port, max_login_retries=None, user='lama', monitor_period=0, local_pid=None):
        super().__init__()
        self.ip = IPAddress(ip)
        self.login_retries = max_login_retries or RemoteExpHost.MaxLoginRetries
        self.__conn = None
        self.__user = user
        self.pid = None
        self.module = module
        self.port = port
        self._monitor_period = monitor_period
        self._local_pid = local_pid

    def init(self):
        retries = 0
        connected = False
        while not connected and retries < self.login_retries:
            try:
                logger.info("[%s] Login: attempt %d", self.ip, retries + 1)
                self.__conn = SSHClient()
                self.__conn.set_missing_host_key_policy(AutoAddPolicy())
                logger.debug("[%s] Connection with user %s", self.ip, self.__user)
                self.__conn.connect(str(self.ip), username=self.__user, timeout=5)
                logger.debug("[%s] Connected", self.ip)
                connected = True
            except (BadHostKeyException, AuthenticationException, SSHException, socket_error, EOFError) as e:
                logger.warn("[%s] Failed login - Exception: %s", self.ip, e)
                self.__conn.close()
                retries += 1
                sleep(0.5)
            except:
                logger.warn("[%s] Exception not caught: %s", self.ip, traceback.format_exc())
                self.__conn.close()
                retries += 1
                sleep(0.5)

        transport = self.__conn.get_transport()
        if not transport:
            logger.error("[%s] No underlying transport.", self.ip)
        elif not transport.is_active():
            logger.error("[%s] Unable to connect", self.ip)

        return connected

    def close(self):
        self.__conn.close()

    def run_command(self, cmd, super=False, timeout=10):
        if self.__conn:
            cmd = "%s%s" % ("sudo " if super else "", cmd)
            try:
                _, out, err = self.__conn.exec_command(cmd, timeout=10)
                out = out.read()
                # err = err.read()
                # logger.debug("stdout=%s", out)
                # logger.debug("stderr=%s", err)
                return out.decode('utf-8')
            except SSHException as e:
                logger.error("Error running command @ %s (%s): %s", self.ip, cmd, str(e))
                return None

        logger.warn("[%s] Not connected", self.ip)
        return None

    def collect_monitor_file(self, local_path):
        sftp = None
        try:
            filename = "monitor_%s.csv" % self.ip
            local_path = path.join(local_path, filename)
            sftp = self.__conn.open_sftp()
            remote_filename = '/home/lama/monitor.csv'
            logger.info("Retrieve file: %s/%s -> %s", self.ip, remote_filename, local_path)
            sftp.get(remote_filename, local_path)
            return True
        except:
            logger.error("Error collecting monitor file @ %s. Traceback: %s", self.ip, traceback.format_exc())
            return False
        finally:
            if sftp:
                sftp.close()

    def run_client(self):
        self.pid = int(self.run_command("sudo lama_exp -ml -p %s %s &> lama_exp.log & echo $!" % (self._monitor_period, self.module)))
        wait_cmd = "until $(curl --output /dev/null --silent --head " \
                   "--fail http://localhost:%s); do sleep 5 ; done" % ListenerPort
        try:
            self.run_command(wait_cmd)
        except SSHException:
            logger.warn("[%s] SSH exception running lama_exp: timeout launching!", self.ip)
        except PipeTimeout:
            logger.warn("[%s] Error running lama_exp: timeout launching!", self.ip)
        except socket.timeout:
            logger.warn("[%s] Socket timeout running lama_exp: timeout launching!", self.ip)
        except:
            logger.warn("[%s] Error running lama_exp: %s", self.ip, traceback.format_exc())

        # sleep(.5)
        # try:
        #     self.pid = int(self.run_command("pgrep lama_exp"))
        # except:
        #     pass
        return self.pid

    def stop_client(self, max_wait=5):
        logger.debug("[%s] Stopping client", self.ip)
        out = self.run_command("pgrep lama_exp | grep -v grep")
        logger.debug("[%s] Out: %s", self.ip, str(out))
        pids = []
        for pid in str(out).split('\n'):
            if pid:
                try:
                    pids.append(int(pid))
                except ValueError:
                    logger.warn("[%s] Problem parsing: %s", self.ip, pid)
                    logger.warn("Traceback: %s", traceback.format_exc())
                    # return True

        result = True
        for pid in pids:
            if pid != self._local_pid:
                # if pid == self.pid:
                self.run_command("sudo kill %s" % pid)
                wait = 0
                r = False
                while not r and wait < max_wait:
                    out = self.run_command("pgrep lama_exp")
                    if not out:
                        logger.debug("[%s] Process has ended - leaving", self.ip)
                        r = True
                    else:
                        logger.debug("[%s] Process %s still running", self.ip, pid)
                        sleep(1)
                        wait += 1
                result = r
            else:
                logger.debug("[%s] Process %s seems to be the server... ignoring!", self.ip, pid)

        return result
        # return False
        # else:
        #     logger.warn("[%s] Zombie: detected=%s, saved=%s", self.ip, pid, self.pid)
        #     return False

    def execute_action(self, max_attempts=10, **action):
        url = 'http://%s:%s/exp' % (self.ip, self.port)
        logger.debug("[%s] RemoteAction:: ip=%s port=%s : %s", getpid(), self.ip, self.port, action.get('cmd', 'N/A'))
        logger.debug(" * Action=%s", action)
        attempt = 0
        while attempt < max_attempts:
            try:
                logger.debug(" * POST: %s", url)
                r = requests.post(url, json=action)
                # logger.info("status: %s", r.status_code)
                logger.info("result: %s", r)
                return r.json(cls=EnhancedJSONDecoder)
            except requests.ConnectionError:
                logger.debug("Unable to send request to '%s' - retrying in 1 sec", url)
                sleep(1)
                attempt += 1
                # return {'results': []}


class LocalExpHost(ExpHost):
    def __init__(self, executor, monitor=True):
        super().__init__()
        self.executor = executor
        # self.monitor = self.launch_monitor()

    # def launch_monitor(self):
    #     self.monitor =
    def execute_action(self, **kwargs):
        logger.debug("LOCAL::execute_action: %s", kwargs)
        return self.executor.execute_action(**kwargs)


class ObjectPool(object):
    def __init__(self, objects):
        self.objects = objects
        self.index = randint(0, len(self.objects) - 1)
        self.get_next_lock = Lock()

    def get_next(self):
        self.get_next_lock.acquire()
        try:
            i = self.index
            self.index = (i + 1) % len(self.objects)
            return self.objects[i]
        finally:
            self.get_next_lock.release()


def ignore_signal():
    signal.signal(signal.SIGINT, signal.SIG_IGN)


class BaseActionExecutor(object):

    def execute_action(self, *args, **kwargs):
        raise NotImplementedError()
        # return {'success': False, 'msg': 'Function was not implemented!'}


class ExperimentReport(object):

    def __init__(self, module):
        self.module = module

    def before_start(self):
        raise NotImplementedError()

    def after_stop(self):
        raise NotImplementedError()


class ExpModule(object):
    ModuleName = 'generic'
    MaxPoolSize = 100
    LocalIPAddresses = get_ip_addresses()
    Index = 0
    ActionExecutor = BaseActionExecutor
    # Define if report is desired
    ExperimentReport = None

    def __init__(self, name=None, expconfig=None, clients_file=None, collect=True,
                 report=True, monitor=True, listener=False, results_dir=None, monitor_period=10,
                 listener_port=ListenerPort):

        self.name = name
        self.results_dir = results_dir
        # options
        self.collect = collect
        self.report = report
        self.monitor = monitor
        self.listener = None
        self.listener_port = listener_port

        self.sched = None
        self.events = None
        self.max_pool_size = None
        self.pool = None
        self.expconfig = expconfig
        self.collector = None
        self.__stopping = False

        self._users = {}
        self._user_order = []
        self._current_user_index = 0
        self._requested_instances_count = 0

        self._processes_to_monitor = []
        self._monitor = None

        self._ref_timestamp_str = datetime.strftime(datetime.now(), "%Y%m%d%H%M%S")
        self._collector_name = "%s-%s" % (''.join(c for c in self.name if c in ValidChars), self._ref_timestamp_str)
        self._monitor_period = monitor_period

        self.event_publisher = LamaSubscriber()

        if clients_file:
            self.clients = self.load_clients_from_file(clients_file)
        else:
            self.clients = [self._create_client()]
        self.client_pool = ObjectPool(self.clients)

        self.listener = listener

        self.report = self.ExperimentReport(self) if self.ExperimentReport else None

        logger.debug("expconfig=%s", expconfig)
        if expconfig:
            self.sched = EventScheduler(self.process_actions)
            self.events = expconfig['events']
            self.max_pool_size = expconfig.get('distributed') or ExpModule.MaxPoolSize
            logger.debug("Starting %s-Process-Pool", max(self.max_pool_size, len(self.clients)))
            self.pool = Pool(max(self.max_pool_size, len(self.clients)), initializer=ignore_signal)

            users = self.expconfig.get('users', {})
            for uname, details in users.items():
                if uname == 'auto':
                    n = int(details)
                    logger.info("Going to create users: %s", n)
                    for _ in range(n):
                        credentials = self._create_temporary_credentials()
                        username = credentials['username']
                        self._users[username] = {
                            self.ModuleName: credentials
                        }
                        self._user_order.append(username)
                else:
                    self._users[uname] = details
                    self._user_order.append(uname)

    def _create_temporary_credentials(self):
        username = None
        while not username or username in self._users:
            username = "lama-%s" % uuid.uuid4().hex[:10]
        return {
            'username': username
        }

    @staticmethod
    def generate_name():
        ExpModule.Index += 1
        return "inst-%010d" % ExpModule.Index

    def load_clients_from_file(self, filename):
        # local addresses
        clients = []
        with open(filename, 'r') as fp:
            for hline in fp.readlines():
                hline = hline.strip()
                if hline and not hline.startswith(";"):
                    try:
                        fields = hline.split()
                        if len(fields) == 1 or fields[1] == "provider":
                            clients.append(self._create_client(fields[0]))
                    except AddrFormatError:
                        logger.warn("Ignored line - no host recognized: %s", hline)
        return clients

    def _create_client(self, ip=None):
        # if ip and ip in ExpModule.LocalIPAddresses:
        #     return RemoteExpHost(
        #         self.ModuleName, '127.0.0.1', self.listener_port, monitor_period=self._monitor_period,
        #         local_pid=getpid()
        #     )
        # else:
        #     return RemoteExpHost(
        #         self.ModuleName, ip, self.listener_port, monitor_period=self._monitor_period
        #     )
        if ip and ip not in ExpModule.LocalIPAddresses:
            return RemoteExpHost(
                self.ModuleName, ip, self.listener_port, monitor_period=self._monitor_period
            )
        else:
            return LocalExpHost(self.ActionExecutor())

    @property
    def remote_hosts(self):
        return [c for c in self.clients if isinstance(c, RemoteExpHost)]

    def start_process_monitor(self):
        # name = self.name = "%s-%s" % (''.join(c for c in self.name if c in ValidChars),
        #                                       datetime.strftime(datetime.now(), "%Y%m%d%H%M%S"))

        folder_path = getcwd()
        try:
            mkdir(folder_path)
        except OSError as e:
            # logger.debug("exception on mkdir: %s", traceback.format_exc())
            pass
        csv_filename = path.join(folder_path, "monitor.csv")
        logger.debug("Monitoring filename: %s", csv_filename)

        if self._monitor_period:
            logger.debug("Starting process monitor for processes: %s", self._processes_to_monitor)
            self._monitor = Engine(
                plugins=[
                    AllProcMonitorPlugin(proc) for proc in self._processes_to_monitor
                ],
                output_plugins=[CSVOutput(csv_filename)],
                interval=timedelta(seconds=self._monitor_period)
            )
            logger.debug("Started process monitor")
            self._monitor.start()

    def start(self):
        try:
            remotes = self.remote_hosts

            if remotes:
                logger.info("Starting '%s' module", self.__class__.__name__)
                pids = self.run_parallel_task(start_remote_client, remotes, self.pool)
                for r, p in zip(remotes, pids):
                    r.pid = p

            # logger.info("Sleeping for testing")
            # sleep(30)

            self.start_process_monitor()
            logger.debug("Scheduler: %s", self.sched)
            if self.sched:
                # Note: the scheduling of events is relative to the current time and not to the
                # time the scheduler starts. So we add events only before starting the scheduler
                logger.debug("Events: %s", self.events)
                self.sched.add_events(self.events)
                duration = int(self.expconfig.get('duration', 0))
                if not self.expconfig.get('options', {}).get('duration_strict', False):
                    max_events_timestamp = max(e['time'] for e in self.events) + 5
                    if duration and max_events_timestamp:
                        duration = min(duration, max_events_timestamp)
                    else:
                        duration = duration or max_events_timestamp

                if duration:
                    try:
                        self.sched.add_stop_scheduler(duration)
                    except:
                        pass
                else:
                    logger.error("Please configure simulation duration or add events!")
                    return

                try:
                    if self.report:
                        self.report.before_start()
                    logger.info("Starting scheduler (duration: %ss)", duration)
                    self.collector.register_start()
                    self.event_publisher.publish('lama_events:experiments', 'start', datetime.now())
                    self.sched.start()
                except KeyboardInterrupt:
                    logger.debug("Received KeyboardInterrupt... Terminating")

            elif self.listener:
                listener_server(controller=self, port=self.listener_port).start()
        except Exception as e:
            logger.error("Error - %s - Traceback: %s", e, traceback.format_exc())

        finally:
            self.event_publisher.publish('lama_events:experiments', 'end', datetime.now())
            if self.collector:
                try:
                    self.collector.register_end()
                except:
                    logger.warn("No register_end function defined")

                if not self.expconfig.get('options', {}).get('duration_strict', False):
                    self.wait_on_instances()

                logger.debug("Writing results to disk: %s", self.results_dir)
                try:
                    self.collector.write_to_disk(folder_path=self.results_dir, ref_end=datetime.now())
                except:
                    logger.error("Error writing to disk: %s", traceback.format_exc())

            self.stop()

    def wait_on_instances(self, max_idle=60):
        if self.collector and self.collector.wait_on_instances:
            logger.warn("Waiting till complete (or %s seconds without updates)", max_idle)
            max_idle_delta = timedelta(seconds=max_idle)
            last = datetime.now()
            last_active = self.collector.active_count
            last_created = self.collector.create_count
            logger.warn("Current status: created: requested=%s, created=%s, active=%s",
                        self._requested_instances_count, last_created, last_active)
            while (self._requested_instances_count > self.collector.create_count or self.collector.create_count > self.collector.active_count) \
                    and datetime.now() < last + max_idle_delta:
                if self.collector.active_count > last_active or self.collector.create_count > last_created:
                    logger.debug("Changed: waiting %s more seconds", max_idle)
                    last = datetime.now()
                    last_active = self.collector.active_count
                    last_created = self.collector.create_count
                sleep(5)

    def stop(self):
        self.__stopping = True
        # close and wait on pool
        if self.sched:
            self.sched.cancel()

        logger.info("Stopping monitor")
        if self._monitor:
            self._monitor.stop()

        remotes = self.remote_hosts
        if remotes:
            if self._monitor_period:
                self.run_parallel_task(partial(collect_monitor_file, path.join(self.results_dir, self._collector_name)), remotes, self.pool)
            logger.info("Waiting for collecting files")
            self.run_parallel_task(stop_remote_client, remotes, self.pool)

        if self.pool:
            logger.debug("close and wait on pool")
            self.pool.close()
            self.pool.join()
            logger.debug("Pool closed")

        if self.report:
            self.report.after_stop()

    def run_parallel_task(self, func, targets, pool):
        # logger.info("func=%s, clients=%s", func, targets)
        results = pool.map(func, targets)
        logger.info("Results of Pool::map: %s", results)
        return results

    def execute_action(self, actions):
        return {'success': False, 'msg': 'Function was not implemented!'}

    def _get_next_user(self, multiuser):
        user = None
        logger.debug("Get user: [multiuser=%s, index=%s]", multiuser, self._current_user_index)
        if multiuser == "round-robin":
            uname = self._user_order[self._current_user_index]
            logger.info("index=%s, user=%s", self._current_user_index, uname)
            user = self._users[uname]
            self._current_user_index = (self._current_user_index + 1) % len(self._user_order)
        elif multiuser == "random":
            uname = sample(self._user_order, 1)
            user = self._users[uname]
        logger.debug("Get user [username=%s]", user)
        return user

    def process_actions(self, actions):
        logger.info("actions: %s", actions)
        # targets = []
        for action in actions:
            cmd = action.get('cmd')
            if cmd:
                # add defaults
                a = self.expconfig.get('defaults', {}).copy()
                a.update(action)

                # add client
                multiuser = None
                if 'user' in a:
                    username = a.get('user')
                    if username == "round-robin" or username == "random":
                        multiuser = username
                    else:
                        c = self.expconfig.get('users', {}).get(a.get('user'))
                        if not c:
                            raise ClientNotFoundException(a)
                        a['user'] = c

                logger.debug('action n: %s', a.get('n'))
                logger.debug('action split: %s', a.get('split'))
                if 'n' in a and a.get('split'):
                    n = a.pop('n')
                    logger.debug("Spliting %s requests", n)
                    for _ in range(n):
                        acopy = deepcopy(a)
                        if multiuser:
                            acopy['user'] = self._get_next_user(multiuser)
                        logger.debug("applying action: %s", acopy)
                        if cmd == 'add':
                            self._requested_instances_count += 1
                        self.pool.apply_async(run_action, (acopy, self.client_pool.get_next()),
                                              callback=self.process_result)
                else:
                    if multiuser:
                        a['user'] = self._get_next_user(multiuser)
                    logger.debug("applying action: %s", a)
                    n = a.setdefault('n', 1)
                    if cmd == 'add':
                        self._requested_instances_count += int(n)
                    c = self.client_pool.get_next()
                    logger.debug("Before apply async: action=%s, client=%s", a, c)
                    logger.debug("Client is of type %s", type(c))
                    result = self.pool.apply_async(run_action, (a, c), callback=self.process_result)
                    # result.get()
            else:
                logger.error("Error on command")
                raise CmdNotFoundException(action)
        return
        # return self.pool.imap_unordered(run_action, targets, self.process_result)

    def process_result(self, result):
        logger.info("Processing ACTION result: %s", result)

        raise NotImplementedError("Implement in the subclasses")

def create_app(data, flavor):
    result = None
    try:
        app_name = data.get('lama', {}).get('username')
        logger.info("Preprocessing - create app: [app=%s, flavor=%s]", app_name, flavor)
        cli = LamaClient(app_name=app_name)
        result = cli.create_app(flavor=flavor)
    except:
        logger.error("Traceback: %s", traceback.format_exc())
    return result


class LamaActionExecutor(BaseActionExecutor):

    def _create_app(self, user, app_type, app_name, make_unique=False, config=None, **kwargs):
        if config is None:
            config = {}
        results = []
        app_name = app_name or user.get('lama', {}).get('app_name')

        try:
            # printout()
            logger.debug("Connection for app: %s", app_name)
            cli = LamaClient(app_name=app_name, app_type=app_type)
            response = cli.create_app(make_unique=make_unique, config=config)
            logger.debug("Create app response: %s", response)
            results.append({'action': 'create_app', 'success': True, 'provider': response.get('provider'),
                            'app_name': app_name})
                            # 'app_name': response.get('appname')})
        except Exception as e:
            logger.error("Traceback: %s", traceback.format_exc())
            logger.info("Now the rest\n\n\n\n")
            results.append({'action': 'create_app', 'success': False, 'app_name': app_name,
                            'msg': 'Unknown exception: %s' % str(e)})

        return results

    def _launch_client(self, user, app_name, config, **kwargs):
        results = []
        app_name = app_name or user.get('lama', {}).get('app_name')

        try:
            logger.debug("Launch client: app_name=%s, config=%s", app_name, config)
            cli = LamaClient(app_name=app_name)
            response = cli.launch_client(config)
            logger.debug("Launch client response: %s", response)
            results.append({'action': 'launch_client', 'success': True, 'app_name': app_name})

        except Exception as e:
            logger.error("Traceback: %s", traceback.format_exc())
            results.append({'action': 'launch_client', 'success': False, 'app_name': app_name})

        return results

    def _generate_failure(self, failure_type, user=None, app_name=None, params=None,
                          instance_name_prefix='lama',
                          **kwargs):
        params = params or {}
        results = []
        app_name = app_name or user.get('lama', {}).get('app_name')

        logger.debug("GENERATE FAILURE: user=%s, app_name=%s, ft=%s, params=%s, other=%s",
                     user, app_name, failure_type, params, kwargs)

        # rubbos_01-client-0002
        try:
            if failure_type == 'instance_crash':
                choice = params.get('choice', 'random')
                service = params.get('service')
                if choice == 'index':
                    index = int(params.get('index'))
                    instance_name = "{}_{}_{}-{:04}".format(instance_name_prefix, app_name, service, index)
                    ip = g_instances.get(instance_name)
                    if ip:
                        logger.debug("Instance %s is at: %s", instance_name, ip)
                        resp = ThanatosClient.generate_instance_crash(ip, instance_name)
                        logger.debug("Resp of failure: %s", resp)
                        results.append({'action': 'failure', 'success': True, 'type': 'Instance Crash', 'app_name': app_name, 'instance_name': instance_name})
                    else:
                        logger.warn("Could not find where instance '%s' is!", instance_name)
                else:
                    logger.error("Option '%s' not yet supported for generate failure!", choice)
            elif failure_type == 'host_crash':
                ip = kwargs.get('ip')
                if ip.lower() == 'random':
                    # providers = list(k for k, v in g_providers.items() if v)
                    providers = list(g_active_instances.values())
                    logger.debug("selecting random provider from: %s", providers)
                    ip = random.choice(providers)
                    logger.debug("selecting a few more: %s, %s, %s, %s, %s",
                                 random.choice(providers),
                                 random.choice(providers),
                                 random.choice(providers),
                                 random.choice(providers),
                                 random.choice(providers),
                                 )

                logger.debug("generate host failure: %s", ip)
                resp = ThanatosClient.generate_host_crash(ip, kwargs.get('duration'))
                logger.debug("Resp of failure: %s", resp)
                results.append({'action': 'failure', 'success': True, 'type': 'Host Crash', 'host': ip})
            elif failure_type == 'host_disk':
                choice = params.get('choice', 'random')
                service = params.get('service')
                if choice == 'index':
                    index = int(params.get('index'))
                    instance_name = "{}_{}_{}-{:04}".format(instance_name_prefix, app_name, service, index)
                    ip = g_instances.get(instance_name)
                    if ip:
                        logger.debug("Instance %s is at: %s", instance_name, ip)
                        args = {k: params.get(k)
                                for k in ('duration', 'read_ratio', 'random_ratio', 'n_threads', 'ref_path', 'delay')
                                if k in params}
                        timestamp_trigger = datetime.now()
                        resp = ThanatosClient.generate_host_disk_failure(ip, **args)
                        logger.debug("Resp of failure: %s", resp)
                        results.append({'action': 'failure', 'success': True, 'type': 'Host disk',
                                        'app_name': app_name, 'host': ip, 'instance_name': instance_name,
                                        'timestamp_trigger': timestamp_trigger,
                                        'details': args})
                    else:
                        logger.warn("Could not find where instance '%s' is!", instance_name)
                else:
                    logger.error("Option '%s' not yet supported for generete failure!", choice)
        except:
            logger.error("Error generating failure: %s", traceback.format_exc())
        return results

    # noinspection PyMethodOverriding
    def execute_action(self, user, flavor, cmd, n=1, **kwargs):
        logger.debug("@lama - execute_action: %s", kwargs)
        # results = []
        # name = "%s-%s-%s-%s" % (gethostname(), "temp", 0, datetime.strftime(datetime.now(), "%Y%m%d-%H%M%S-%f"))
        # results.append({'success': False, 'name': name, 'ts_start': datetime.now(), 'msg': 'Not implemented'})
        # return {'results': results}
        results = []
        if cmd == 'add':
            app_name = user.get('lama', {}).get('username')

            logger.info("Launching %s instances", n)
            for i in range(n):
                instance = None
                try:
                    s = datetime.now()
                    logger.debug("Connection for app: %s", app_name)
                    cli = LamaClient(app_name=app_name)

                    instance = cli.create_instance(
                        flavor=flavor
                    )
                    logger.debug("Instance received: %s", instance)
                    diff = datetime.now() - s
                    instance_name = instance.get('name')
                    results.append({'success': True, 'app_name': app_name, 'name': instance_name, 'id': instance_name,
                                    'ts_start': s, 'rt_call': diff.total_seconds()})
                except CreateInstanceException as e:
                    results.append({'success': False, 'app_name': app_name, 'name': instance.name, 'ts_start': s, 'msg': str(e)})
                except Exception as e:
                    logger.error("Traceback: %s", traceback.format_exc())
                    logger.info("Now the rest\n\n\n\n")
                    results.append({'success': False, 'app_name': app_name, 'name': instance.name if instance else None,
                                    'ts_start': s, 'msg': 'Unknown exception: %s' % str(e)})
        elif cmd == 'create_app':
            logger.debug("Process create app")
            results = self._create_app(user, **kwargs)
        elif cmd == 'launch_client':
            logger.debug("Process launch client")
            results = self._launch_client(user, **kwargs)
        elif cmd == 'failure':
            logger.debug("Process generate failure")
            results = self._generate_failure(user=user, **kwargs)

        return {'results': results}


class LamaModule(ExpModule):
    ModuleName = 'lama'
    ActionExecutor = LamaActionExecutor
    logger.debug("Set DEFAULT collector")
    EventCollector = Collector

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.subscriber = None
        self._created_apps = 0
        if self.expconfig:
            # add experiment collectors
            if self.collect:
                self.collector = self.EventCollector(name=self._collector_name)
            self.subscriber = LamaSubscriberThread(host='10.1.1.12', callback=self._process_active_notification)
            self.subscriber.start()
            logger.debug("Calling preprocessing for self: %s", type(self))
            self.preprocessing()

        # add processes to monitor
        self._processes_to_monitor = [
            'run_dispatcher',
            'run_provider',
            'run_application'
        ]

    def preprocessing(self):
        logger.info("Preprocessing")
        logger.debug("User: %s", self._users)
        flavor = self.expconfig.get('defaults', {}).get('flavor') or self.expconfig.get('defaults', {}).get('size')

        logger.info("Create %d apps", len(self._users))
        i = 1
        # pool = Pool(20)
        for uname, details in self._users.items():
            app_name = details.get('lama', {}).get('username')
            logger.info("Preprocessing - create app %d/%d [app=%s, flavor=%s]", i, len(self._users), app_name, flavor)
            # pool.apply_async(create_app, (details, flavor), callback=self._process_create_app_result)

            cli = LamaClient(app_name=app_name)
            cli.create_app(flavor=flavor)
            i += 1
        # pool.close()
        # pool.join()

    def _process_create_app_result(self, result):
        self._created_apps += 1
        logger.info("Result of create app: %s #%d", result, self._created_apps)

    def _process_active_notification(self, event_type, **kwargs):
        logger.debug("Process notification: %s : %s", event_type, kwargs)
        # app_name, instance_id, instance_name, ip, active_ts
        if self.collect:
            if event_type == FrameworkEventTypes.active_instance:
                instance_id = "lama_%s_%s" % (kwargs.get('app_name'), kwargs.get('instance_id'))
                instance_name = "lama_%s_%s" % (kwargs.get('app_name'), kwargs.get('instance_name'))
                self.collector.register_active_instance(instance_id, instance_name, kwargs.get('ip'), kwargs.get('active_ts'),
                                                        success=(kwargs.get('active_ts') is not None))
                g_instances[instance_name] = kwargs.get('ip')
                logger.debug("[%s:%s] g_instances: %s %s", current_process().pid, current_thread().name,
                             id(g_instances), g_instances)
                logger.info("Registered instance: %s at %s", instance_name, kwargs.get('ip'))
            elif event_type == FrameworkEventTypes.active_app:
                self.collector.register_active_app(kwargs.get('app_name'), kwargs.get('active_ts'))
            # elif event_type == FrameworkEventTypes.allocated_instance:
            elif event_type == FrameworkEventTypes.ready_instance:
                instance_id = "lama_%s_%s" % (kwargs.get('app_name'), kwargs.get('instance_id'))
                instance_name = "lama_%s_%s" % (kwargs.get('app_name'), kwargs.get('instance_name'))

                ip = kwargs.get('ip')
                if ip:
                    # s = g_providers.setdefault(ip, set())
                    # s.add(instance_name)
                    # logger.debug("provider: %s", g_providers)
                    # for k, v in g_providers.items():
                    #     logger.debug("k=%s, v=%s  %s", k, v, True if v else False)
                    # logger.debug("list=%s", list(k for k, v in g_providers.items()))
                    # logger.debug("list=%s", list(k for k, v in g_providers.items() if v))
                    g_active_instances[instance_name] = ip
                    # logger.info("Processed ready instance - g_providers: %s", g_providers)
                    logger.info("Processed ready instance - g_active_instances: %s", g_active_instances)
                else:
                    logger.warn("Error - ip is none")

                self.collector.register_ready_instance(instance_id, instance_name, kwargs.get('ip'), kwargs.get('ready_ts'),
                                                        success=(kwargs.get('ready_ts') is not None))
            elif event_type == FrameworkEventTypes.removed_instance:
                instance_name = "lama_%s_%s" % (kwargs.get('app_name'), kwargs.get('instance_name'))
                ip = kwargs.get('ip')
                if ip:
                    if instance_name in g_active_instances:
                        del g_active_instances[instance_name]
                    # s = g_providers.setdefault(ip, set())
                    # s.remove(instance_name)
                    # logger.info("Processed ready instance - g_providers: %s", g_providers)
                    logger.info("Processed remove instance - g_active_instances: %s", g_active_instances)
                else:
                    logger.warn("Error - ip is none")
            else:
                logger.debug("Not processed!")

        self.sched.eval_dependency(event_type, kwargs)

    # noinspection PyMethodOverriding
    def execute_action(self, user, flavor, cmd, n=1, **kwargs):
        logger.debug("@lama - execute_action: %s", kwargs)
        # results = []
        # name = "%s-%s-%s-%s" % (gethostname(), "temp", 0, datetime.strftime(datetime.now(), "%Y%m%d-%H%M%S-%f"))
        # results.append({'success': False, 'name': name, 'ts_start': datetime.now(), 'msg': 'Not implemented'})
        # return {'results': results}
        results = []
        if cmd == 'add':
            app_name = user.get('lama', {}).get('username')

            logger.info("Launching %s instances", n)
            for i in range(n):
                instance = None
                try:
                    s = datetime.now()
                    logger.debug("Connection for app: %s", app_name)
                    cli = LamaClient(app_name=app_name)

                    instance = cli.create_instance(
                        flavor=flavor
                    )
                    logger.debug("Instance received: %s", instance)
                    diff = datetime.now() - s
                    instance_name = instance.get('name')
                    results.append({'success': True, 'app_name': app_name, 'name': instance_name, 'id': instance_name,
                                    'ts_start': s, 'rt_call': diff.total_seconds()})
                except CreateInstanceException as e:
                    results.append({'success': False, 'app_name': app_name, 'name': instance.name, 'ts_start': s, 'msg': str(e)})
                except Exception as e:
                    logger.error("Traceback: %s", traceback.format_exc())
                    logger.info("Now the rest\n\n\n\n")
                    results.append({'success': False, 'app_name': app_name, 'name': instance.name if instance else None,
                                    'ts_start': s, 'msg': 'Unknown exception: %s' % str(e)})
        elif cmd == 'create_app':
            logger.debug("Process create app")
            self._create_app(user, **kwargs)

        return {'results': results}

    def process_result(self, results):
        # logger.info("Processing ACTION result: %s", results)
        results = results.get('results', [])
        if self.collect:
            logger.info("Processing %s results", len(results))
            for r in results:
                instance_id = "%s-%s" % (r.get('app_name'), r.get('id'))
                instance_name = "%s-%s" % (r.get('app_name'), r.get('name'))

                if r.get('success'):
                    self.collector.register_create_instance(instance_id, instance_name,
                                                            r.get('ts_start'), r.get('rt_call'))
                else:
                    self.collector.register_error_instance(instance_id, instance_name,
                                                           r.get('ts_start'), r.get('rt_call'))
        logger.info("No Processing %s results", len(results))

    def stop(self):
        super().stop()
        logger.debug("Stopping subscriber.")
        if self.subscriber:
            self.subscriber.stop()
        logger.debug("Done!")
        if self.collect:
            logger.info("Results saved in folder: %s", path.join(self.results_dir, self._collector_name))


class OpenStackModule(ExpModule):
    ModuleName = 'os'

    def __init__(self, *args, **kwargs):
        self._last_user_index = 0
        super().__init__(*args, **kwargs)
        self.subscriber = None
        if self.expconfig:
            if self.collect:
                self.collector = OSCollector(name=self._collector_name, save_os_delays=True)
                # self.subscriber = OSSubscriber(username='openstack', password='l@ma_rabbit', host='controller',
                #                                callback=self.collector.register_active_instance)
            self.subscriber = OSSubscriberThread(username='openstack', password='l@ma_rabbit', host='controller',
                                                 callback=self.collector.register_active_instance)
            self.subscriber.start()
            # self.default_os_client = OSClient(username='lama', password='fmG-qd4-ykT-rEp')

    def _create_temporary_credentials(self):
        self._last_user_index += 1
        username = None
        while not username or username in self._users:
            username = "lama_%s" % self._last_user_index
        return {
            'username': username,
            'password': "l@ma_%s" % username,
            'keypair': "%s_key" % username
        }


    # noinspection PyMethodOverriding
    def execute_action(self, user, image, flavor, network, n=1, **kwargs):

        uname = user.get('os', {}).get('username')
        pword = user.get('os', {}).get('password')
        keypair = user.get('os', {}).get('keypair')

        results = []
        logger.info("Launching %s instances: uname=%s", n, uname)
        for i in range(n):
            name = "%s-%s-%s-%s" % (gethostname(), uname, i, datetime.strftime(datetime.now(), "%Y%m%d-%H%M%S-%f"))
            s = datetime.now()
            try:
                logger.debug("Connection with user: %s", uname)
                cli = OSClient(username=uname, password=pword)
                logger.info("Launch instance [%s]:", name)
                logger.info("\timg=%s, flavor=%s, net=%s, keypair=%s", image, flavor, network, keypair)
                s = datetime.now()
                server = cli.create_instance(
                    name=name,
                    flavor=flavor,
                    image=image,
                    network=network,
                    keypair=keypair
                )
                diff = datetime.now() - s
                results.append({'success': True, 'name': name, 'id': server.id, 'ts_start': s, 'rt_call': diff.total_seconds()})
            except CreateInstanceException as e:
                results.append({'success': False, 'name': name, 'ts_start': s, 'msg': str(e)})
            except Exception as e:
                logger.error("Traceback: %s", traceback.format_exc())
                logger.info("Now the rest\n\n\n\n")
                results.append({'success': False, 'name': name, 'ts_start': s, 'msg': 'Unknown exception: %s' % str(e)})
        return {'results': results}

    def process_result(self, results):
        # logger.info("Processing ACTION result: %s", results)
        results = results.get('results', [])
        logger.info("Processing %s results", len(results))
        for r in results:
            if r.get('success'):
                self.collector.register_create_instance(r.get('id'), r.get('name'), r.get('ts_start'), r.get('rt_call'))
            else:
                self.collector.register_error_instance(r.get('id'), r.get('name'), r.get('ts_start'), r.get('rt_call'))

    def delete_instances(self):
        # for uid, user in self.expconfig.get("users", {}).items():
        i = 1
        for uid, details in self._users.items():
            uname = details.get('os', {}).get('username')
            pword = details.get('os', {}).get('password')

            try:
                logger.debug("Delete instances for user: %s - %s", uid, uname)
                cli = OSClient(username=uname, password=pword)
                instances = cli.list_instances()
                logger.debug("Instances: %s", instances)
                for instance in instances:
                    logger.debug("Delete instance (%3d: %s - %s - %s", i, instance.id, instance.name, instance.status)
                    cli.delete_instance(instance.id)
                    i += 1

            except Exception:
                logger.error("Traceback: %s", traceback.format_exc())

    def stop(self):
        super().stop()
        logger.debug("Stopping subscriber.")
        if self.subscriber:
            self.subscriber.stop()
        logger.debug("Deleting instances.")
        self.delete_instances()
        logger.debug("Done!")


class LamaExpController(object):
    class ExperimentException(Exception):
        pass

    __exp_modules = {
        LamaModule.ModuleName: LamaModule,
        OpenStackModule.ModuleName: OpenStackModule
    }

    def __init__(self, config, module_cls=None, **kwargs):
        config_dict = vars(config)
        config_dict.update(kwargs)

        self.name = LamaExpController.generate_name(config_dict)

        try:
            self.__exp_module = (module_cls or LamaExpController.__exp_modules[config_dict.get('module')])(
                expconfig=config_dict.get('expconfig'),
                clients_file=config_dict.get('clients_filename'),
                collect=config_dict.get('collect'),
                report=config_dict.get('report'),
                listener=config_dict.get('listener'),
                name=self.name,
                results_dir=config_dict.get('results_dir'),
                monitor_period=config_dict.get('monitor_period')
            )
        except KeyError:
            raise LamaExpController.ExperimentException("Invalid module name '%s'" % config_dict.get('module')) from None

    def start(self):
        self.__exp_module.start()

    @staticmethod
    def generate_name(config):
        name = "exp"
        if config.get('config_filename'):
            name = path.splitext(path.basename(config.get('config_filename')))[0]
        if config.get('module'):
            name = "%s_%s" % (name, config.get('module'))
        if config.get('yaml_vars'):
            # name = "%s-%s" % (name, "_".join(["%s=%s" % (k.split(".")[-1], v) for k, v in config.yaml_vars.items()]))
            sorted_keys = sorted([(k, k.split(".")[-1]) for k in config.get('yaml_vars').keys()], key=lambda x: x[1])
            name = "%s-%s" % (name, "_".join(["%s=%s" % (k, config.get('yaml_vars')[k]) for k, _ in sorted_keys]))

        return name

    @staticmethod
    def get_module_names():
        return set(LamaExpController.__exp_modules.keys())


def load_yaml_filename(filename):
    with open(filename, 'r') as fp:
        config = yaml.load(fp)
    return config


def yaml_filename(filename):
    try:
        load_yaml_filename(filename)
    except FileNotFoundError:
        raise ArgumentTypeError("File not found - '%s' is not a valid YAML file." % filename)
    except yaml.ParserError as e:
        raise ArgumentTypeError("Error loading filename: %s details: %s", filename, e)
    return filename


def vars_dict(s):
    try:
        vs = {}
        for pair in s.split(","):
            k, v = pair.split('=')
            vs[k] = v
        return vs
    except:
        raise ArgumentTypeError("Unable to parse variables")


def creatable_or_existing_dir(d):
    if not path.isdir(d):
        mkdir(d)
    return d


def apply_var(config, key, value):
    fields = key.split('.')
    if not len(fields):
        raise Exception("Bad vars key: %s", key)
    if len(fields) == 1:
        try:
            value = literal_eval(value)
        except ValueError:
            pass
        config[fields[0]] = value
    else:
        return apply_var(config[fields[0]], '.'.join(fields[1:]), value)


def main():
    logger.setLevel(logging.DEBUG)
    stream_handler = logging.StreamHandler(stream=sys.stdout)
    stream_handler.setFormatter(
        logging.Formatter("%(asctime)s : %(levelname)5.5s : %(module)14.14s [%(lineno)3.3s] : %(message)s")
    )
    logging.root.addHandler(stream_handler)
    logging.root.setLevel(logging.DEBUG)
    # logger.addHandler(stream_handler)

    ap = ArgumentParser(description="Script to run an experiment in eithter OpenStack or LAMA environments. "
                                    "According to configuration will: start the clients, monitoring services, "
                                    "collect the results and/or generate a report.")
    ap.add_argument('module', choices=LamaExpController.get_module_names(), help="Module to launch.")
    ap.add_argument('-d', dest='clients_filename', type=str,
                    help="Distributed clients: it will launch clients spread out "
                         "through the network instead of using a single central client."
                         "Hosts must ssh-accessible using key for user 'lama'")
    # workflow options
    ap.add_argument('-m', dest='monitor', action='store_true',
                    help='Start up monitoring scripts (with CSV option and listener).')
    ap.add_argument('-c', dest='collect', action='store_true',
                    help='Collect results from all nodes')
    ap.add_argument('-r', dest='report', action='store_true',
                    help='Generate a report (assumes presence of results)')
    ap.add_argument('-t', dest='trace', action='store_true',
                    help='Raise exception for debug')
    ap.add_argument('--vars', dest='yaml_vars', type=vars_dict,
                    help='Define variables to be used in the yaml file.')
    ap.add_argument('--res_dir', dest='results_dir', type=creatable_or_existing_dir, default=path.join(getcwd(), 'results'),
                    help='Results folder.')
    # ap.add_argument('--no-delete', dest='delete', type=vars_dict,
    #                 help='Define variables to be used in the yaml file.')
    group = ap.add_mutually_exclusive_group()
    group.add_argument('-l', dest='listener', action='store_true',
                       help='Run as client/listener (can receive requests from server)')
    group.add_argument('-s', dest='config_filename', type=yaml_filename,
                       help='Path for the YAML filename containing configuration options '
                            '(schedule of events, credentials, instance options, etc.)')
    group.add_argument('-p', dest='monitor_period', type=int,
                       help='Monitor period in seconds', default=0)


    # experiment type
    # will use for several types of experiments
    args = ap.parse_args()

    args.expconfig = None
    if args.config_filename:
        args.expconfig = load_yaml_filename(args.config_filename)
        if args.yaml_vars:
            for k, v in args.yaml_vars.items():
                apply_var(args.expconfig, k, v)

    # logger.warn(args)
    try:
        logger.info("Args: %s", args)
        lef = LamaExpController(args)
        if lef:
            lef.start()
    except LamaExpController.ExperimentException as e:
        if args.trace:
            print("Error during execution of the experiment")
            for line in traceback.format_exc().splitlines():
                print("\tERROR\t%s" % line)
        else:
            print("Error: %s" % e)


if __name__ == '__main__':
    main()