import logging
import sys
import traceback
from argparse import ArgumentParser

from os import path, getcwd
from lama_exp.experiments.base import LamaExpController, creatable_or_existing_dir, yaml_filename, load_yaml_filename, \
    apply_var, vars_dict, LamaModule

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.2'
__email__ = 'tcarvalho@cmu.edu'


logger = logging.getLogger(__name__)


class LamaMonitoringExpModule(LamaModule):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def process_result(self, result):
        logger.error("Not implemented - process_result: %s", result)


def main():
    logger.setLevel(logging.DEBUG)
    stream_handler = logging.StreamHandler(stream=sys.stdout)
    stream_handler.setFormatter(
        logging.Formatter("%(asctime)s : %(levelname)5.5s : %(module)14.14s [%(lineno)3.3s] : %(message)s")
    )
    logger.addHandler(stream_handler)

    ap = ArgumentParser(description="Script to run an experiment in eithter OpenStack or LAMA environments. "
                                    "According to configuration will: start the clients, monitoring services, "
                                    "collect the results and/or generate a report.")
    ap.add_argument('module', choices=LamaExpController.get_module_names(), help="Module to launch.")
    ap.add_argument('-d', dest='clients_filename', type=str,
                    help="Distributed clients: it will launch clients spread out "
                         "through the network instead of using a single central client."
                         "Hosts must ssh-accessible using key for user 'lama'")
    # workflow options
    ap.add_argument('-m', dest='monitor', action='store_true',
                    help='Start up monitoring scripts (with CSV option and listener).')
    ap.add_argument('-c', dest='collect', action='store_true',
                    help='Collect results from all nodes')
    ap.add_argument('-r', dest='report', action='store_true',
                    help='Generate a report (assumes presence of results)')
    ap.add_argument('-t', dest='trace', action='store_true',
                    help='Raise exception for debug')
    ap.add_argument('--vars', dest='yaml_vars', type=vars_dict,
                    help='Define variables to be used in the yaml file.')
    ap.add_argument('--res_dir', dest='results_dir', type=creatable_or_existing_dir,
                    default=path.join(getcwd(), 'results'),
                    help='Results folder.')
    # ap.add_argument('--no-delete', dest='delete', type=vars_dict,
    #                 help='Define variables to be used in the yaml file.')
    group = ap.add_mutually_exclusive_group()
    group.add_argument('-l', dest='listener', action='store_true',
                       help='Run as client/listener (can receive requests from server)')
    group.add_argument('-s', dest='config_filename', type=yaml_filename,
                       help='Path for the YAML filename containing configuration options '
                            '(schedule of events, credentials, instance options, etc.)')
    group.add_argument('-p', dest='monitor_period', type=int,
                       help='Monitor period in seconds', default=0)

    # experiment type
    # will use for several types of experiments
    args = ap.parse_args()

    args.expconfig = None
    if args.config_filename:
        args.expconfig = load_yaml_filename(args.config_filename)
        if args.yaml_vars:
            for k, v in args.yaml_vars.items():
                apply_var(args.expconfig, k, v)

    # logger.warn(args)
    try:
        logger.info("Args: %s", args)
        lef = LamaExpController(args, module_cls=LamaMonitoringExpModule)
        if lef:
            lef.start()
    except LamaExpController.ExperimentException as e:
        if args.trace:
            print("Error during execution of the experiment")
            for line in traceback.format_exc().splitlines():
                print("\tERROR\t%s" % line)
        else:
            print("Error: %s" % e)


if __name__ == '__main__':
    main()

    # ====== Ideas/Notes ======
    # TODO] The report should also include information about the machines (e.g. HW specs, OS, framwework version)
    # TODO] Change the ExpClient to detect if it is a remote host or not and initialize the proper object
