# import time
from datetime import datetime, timedelta
import logging
import traceback
# from sched import scheduler
from apscheduler.schedulers import SchedulerNotRunningError
from apscheduler.schedulers.blocking import BlockingScheduler
from lama_exp.subscribers import FrameworkEventTypes

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


logger = logging.getLogger('lama_exp')


class SchedulerConfigurationError(Exception):
    pass


class EventScheduler(object):

    def __init__(self, callback):
        self.callback = callback
        # self.events = BinomialHeap()
        self.__last_event = None
        self.__scheduler = BlockingScheduler()
        # self.__scheduler = scheduler(time.time, time.sleep)
        self.__simulation_start = None
        self.__simulation_stop = None
        self.__dependent_events = {}

        self.__events = []

    def add_events(self, events):
        self.__events += events

    def __schedule_events(self):
        for event in self.__events:
            logger.debug("event: %s", event)
            logger.debug("Schedule event: %s => %s", event["time"], event["actions"])
            dependencies = event.get('dependencies')
            logger.debug("dependencies: %s", dependencies)
            repeat = event.get('repeat')
            if repeat:
                if repeat.get('n') and repeat.get('period'):
                    logger.debug("event to be repeat: %s", dependencies)
                else:
                    raise SchedulerConfigurationError(
                        "To repeat an event please set 'n' and 'period' parameters (event @ %s)",
                        event.get('time')
                    )

            if not dependencies:
                self._schedule_event(self.__simulation_start, event)
                # self.__scheduler.add_job(self.callback, 'date', args=(event["actions"],),
                #                          run_date=(self.__simulation_start + timedelta(seconds=repeat_time)))
            else:
                for dependency in dependencies:
                    for event_type_name, conditions in dependency.items():
                        try:
                            event_type = FrameworkEventTypes[event_type_name]
                        except KeyError:
                            logger.warning("Unsupported event type: %s", event_type_name)
                        else:
                            for key, value in conditions.items():
                                self.__dependent_events.setdefault(event_type, {}).setdefault((key, value), []).append(event)

        logger.debug("dependent events: %s", self.__dependent_events)

    def _schedule_event(self, ref_time, event):
        repeat_n = event.get('repeat', {}).get('n', 1)
        repeat_period = event.get('repeat', {}).get('period', 60)
        event_time = event.get('time')
        for _ in range(repeat_n):
            self.__scheduler.add_job(self.callback, 'date', args=(event["actions"],),
                                     run_date=(ref_time + timedelta(seconds=event_time)))
            event_time += repeat_period

    def eval_dependency(self, event_type, details):
        logger.debug("Check dependency: %s -> %s", event_type, details)
        # logger.debug("dependent events: %s", self.__dependent_events)
        if event_type in self.__dependent_events:
            events_by_event_type = self.__dependent_events.get(event_type, {})
            # logger.debug("events_by_event_type: %s", events_by_event_type)
            if events_by_event_type:
                for key, value in details.items():
                    logger.debug("key value: %s, %s", key, value)
                    events = events_by_event_type.get((key, value))
                    logger.debug("events by key value: %s", events)
                    if events:
                        del events_by_event_type[(key, value)]
                        for event in events:
                            logger.debug("Considering event: %s", event)
                            dependencies = event.get('dependencies', [])
                            logger.debug("Previous dependencies: %s", dependencies)
                            new_deps = []
                            # TODO: replace by filter?
                            for eve in dependencies:
                                new_eve = {k: v for k, v in eve.get(event_type.name, {}).items() if k not in details or details[k] != v}
                                if new_eve:
                                    new_deps.append({event_type.name: new_eve})

                            # dependencies[event_type.name] = {k: v for ev in dependencies for k, v in ev.get(event_type.name, {}).items() if k not in details or details[k] != v}
                            event['dependencies'] = new_deps
                            logger.debug("Current dependencies: %s", event.get('dependencies'))

                            # if event_type.name in dependencies and not len(dependencies[event_type.name]):
                            #     del dependencies[event_type.name]
                            if not len(new_deps):
                                for action in (action for action in event.get('actions', []) if action.get('cmd').startswith('experiment_')):
                                    if action.get('cmd') == 'experiment_end':
                                        self._add_stop_event(datetime.now() + timedelta(seconds=event["time"]))
                                    else:
                                        logger.warn("Experiment Action not supported: %s", action)

                                actions = [action for action in event.get('actions', []) if not action.get('cmd').startswith('experiment_')]
                                if actions:
                                    logger.debug("Schedule event in %ss from now", event.get('time'))
                                    # self.__scheduler.enter(event['time'], 1, self.callback, argument=(event["actions"],))
                                    self._schedule_event(datetime.now(), event)
                                    # self.__scheduler.add_job(self.callback, 'date', args=(event["actions"],),
                                    #                          run_date=(datetime.now() + timedelta(seconds=event["time"])))

                if not len(events_by_event_type):
                    del self.__dependent_events[event_type]
        # logger.debug("dependent events: %s", self.__dependent_events)

    def add_stop_scheduler(self, ts):
        self.__simulation_stop = ts
        # self.__scheduler.enter(ts, 1, self.on_stop_scheduler)

    def on_stop_scheduler(self):
        logger.info("Stopping scheduler")
        self.cancel()

    def _add_stop_event(self, timestamp):
        logger.debug("Schedule END OF EXP at %s", timestamp)
        self.__scheduler.add_job(self.on_stop_scheduler, 'date',
                                 run_date=timestamp)

    def start(self):
        self.__simulation_start = datetime.now()
        if self.__simulation_stop:
            self._add_stop_event(self.__simulation_start + timedelta(seconds=self.__simulation_stop))

        self.__schedule_events()
        logger.info("Start scheduler")
        # self.__scheduler.run()
        self.__scheduler.start()
        logger.info("Scheduler ended")

    def cancel(self, wait=False):
        self.__scheduler.remove_all_jobs()
        try:
            self.__scheduler.shutdown(wait=wait)
        except SchedulerNotRunningError:
            pass
        # try:
        #     map(self.__scheduler.cancel, self.__scheduler.queue)
        #
        # except:
        #     logger.error("Error while canceling: %s", traceback.format_exc())

