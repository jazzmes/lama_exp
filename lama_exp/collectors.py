import logging
import string
import csv
import numpy as np
from math import ceil, floor

from lama_exp.drill.os_instance import ResearchOSInstance, OSInstanceProfile
from os import getcwd, path, mkdir
from datetime import datetime
from operator import itemgetter

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'


logger = logging.getLogger('lama_exp')


class DuplicateInstance(Exception):
    pass


class Collector(object):

    def __init__(self, name=None, ref_start=None):
        self.instances = {}
        self.errors = []

        self.name = name or "exp-%s" % datetime.strftime(datetime.now(), "%Y%m%d%H%M%S")
        self.ref_start = ref_start or datetime.now()
        self.ref_end = None

        self.create_count = 0
        self.create_success = 0
        self.active_count = 0
        self.active_success = 0

    @property
    def wait_on_instances(self):
        return True

    def register_create_instance(self, id, name, ts_start, rt_call=None, fail=False):
        logger.debug("register_create_instance: id=%s, ts_start=%s, rt_call=%s, %s",
                     id, ts_start, rt_call, "FAIL" if fail else "SUCCESS")
        self.create_count += 1
        if not fail:
            self.create_success += 1
        if id in self.instances:
            logger.warn("Instance already exists: %s", id)
            # return

        instance = self.instances.setdefault(id, {'id': id})
        if 'rt_call' in instance:
            logger.warn("Instance %s already has called result: %s - NOT saving", id, instance['rt_call'])
            return

        instance.update({
            'name': name,
            'ts_start': ts_start
        })
        if rt_call:
            instance['rt_call'] = rt_call

        if fail:
            instance["success"] = False
        logger.debug("Registered instance: %s=%s", id, instance)
        logger.debug("Total requests: %s / %s", self.create_success, self.create_count)

    def register_active_instance(self, id, name, node, timestamp, success=True):
        logger.debug("register_active_instance: id=%s, timestamp=%s, success=%s", name, timestamp, success)
        if id not in self.instances:
            logger.warn("Unknown instance: %s - Saving result anyway", id)
            # return

        instance = self.instances.setdefault(id, {'id': id})
        if 'ts_active' in instance:
            logger.warn("Instance %s already was active: %s + %s - NOT saving", id, instance.get('ts_start'),
                        instance['ts_active'])
            return

        instance.update({
            'name': name,
            'host': node,
            'ts_active': timestamp,
            'success': success
        })
        self.active_count += 1
        if success:
            self.active_success += 1
        logger.debug("Instance '%s' is NOW active %s", id, instance)
        logger.debug("Total active: %s / %s", self.active_success, self.active_count)
        # for id, instance in self.instances.items():
        #     logger.debug("instance: %s=%s", id, instance)

    def register_active_app(self, app_name, active_ts):
        logger.debug("NotImplemented::register_active_app")

    def register_error_instance(self, id, name, ts_start, rt_call=None):
        logger.debug("register_active_instance: id=%s, ts_start=%s, rt_call=%s", id, ts_start, rt_call)
        self.register_create_instance(id, name, ts_start, rt_call=rt_call, fail=True)

    def write_to_disk(self, folder_path=getcwd(), ref_end=None):

        logger.debug("Write instances: \n\t%s", '\n\t'.join(["%s: %s" % (k, v) for k, v in self.instances.items()]))

        # create folder with name of experiment
        folder_path = path.join(folder_path, self.name)
        try:
            mkdir(folder_path)
        except OSError as e:
            logger.debug("OSError on mkdir: %s", e)

        # create raw files:
        #  - errors: list of timestamps
        filename = "%s.errors.raw.csv" % self.name
        filepath = path.join(folder_path, filename)
        sorted_errors = sorted(self.errors, key=itemgetter(0))
        with open(filepath, "w") as f:
            w = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
            w.writerow(['timestamp', 'instance'])
            w.writerows(sorted_errors)
        logger.info("wrote: %s", filename)

        for v in self.instances.values():
            s = v.get('ts_start')
            a = v.get('ts_active')
            if s and a:
                v['rt_active'] = (a - s).total_seconds()

        #  - instances: timestamp, rt_call, rt_active
        filename = "%s.response_times.raw.csv" % self.name
        filepath = path.join(folder_path, filename)
        sorted_instances = sorted([v for v in self.instances.values()], key=lambda x: x.get('ts_start', datetime.now()))
        # logger.debug("instances: %s", self.instances)
        # logger.debug("sorted_instances: %s", sorted_instances)
        with open(filepath, "w") as f:
            w = csv.DictWriter(f, fieldnames=['ts_start', 'id', 'name', 'host', 'rt_call', 'ts_active', 'rt_active', 'success'], quoting=csv.QUOTE_MINIMAL)
            w.writeheader()
            w.writerows(sorted_instances)
        logger.info("wrote: %s", filename)

        # for charts?
        #  - errors per sec
        #  - rt_call per second
        #  - rt_active_per_second
        filename = "%s.metrics.sec.csv" % self.name
        filepath = path.join(folder_path, filename)
        if not ref_end:
            ref_end = max(max(self.errors, key=itemgetter(0)), max(self.instances, key=itemgetter('ts_start')))
        nbins = ceil((ref_end - self.ref_start).total_seconds())
        # error_hist = np.histogram(
        #     [(e[0] - self.ref_start).total_seconds() for e in self.errors if e[0] > self.ref_start and e[0] <= ref_end],
        #     bins=nbins
        # )
        # logger.debug("errors hist: %s", error_hist)
        data = {}
        for _, i in self.instances.items():
            rel_ts = int(floor((i['ts_start'] - self.ref_start).total_seconds()))
            entry = data.setdefault(rel_ts, {'rt_call': [], 'rt_active': [], 'errors': 0})
            if i.get('rt_call'):
                entry['rt_call'].append(i['rt_call'])
            if i.get('rt_active'):
                entry['rt_active'].append(i.get('rt_active'))

        for e in self.errors:
            rel_ts = int(floor((e[0] - self.ref_start).total_seconds()))
            entry = data.setdefault(rel_ts, {'rt_call': [], 'rt_active': [], 'errors': 0})
            entry['errors'] += 1

        logger.debug("data=%s", data)
        with open(filepath, "w") as f:
            w = csv.DictWriter(f, fieldnames=['sec_since_start', 'name', 'total', 'errors', 'rt_call', 'rt_active', 'success'], quoting=csv.QUOTE_MINIMAL)
            w.writeheader()
            for b in range(nbins):
                total = errors = 0
                name = rt_call = rt_active = success = None
                if b in data:
                    d = data.get(b)
                    name = data.get('name')
                    errors = d.get('errors')
                    total = len(d.get('rt_call')) + errors
                    rt_call = np.mean(d.get('rt_call')) if d.get('rt_call') else None
                    rt_active = np.mean(d.get('rt_active')) if d.get('rt_active') else None
                    success = data.get('success', True)
                w.writerow({
                    'sec_since_start': b,
                    'name': name,
                    'total': total,
                    'errors': errors,
                    'rt_call': rt_call,
                    'rt_active': rt_active,
                    'success': success
                })
        logger.info("wrote: %s", filename)
        return folder_path,


class OSCollector(Collector):

    def __init__(self, name, save_os_delays=False):
        super().__init__(name)
        self._save_os_delays = save_os_delays

    def write_to_disk(self, folder_path=getcwd(), ref_end=None):
        super().write_to_disk(folder_path, ref_end)

        if self._save_os_delays:
            # save delays:
            folder_path = path.join(folder_path, self.name)

            headers = OSInstanceProfile.Headers
            details_headers = OSInstanceProfile.DetailsHeaders

            results = []
            details_results = []
            for instance_id, instance in self.instances.items():
                roi = ResearchOSInstance(
                    instance_id,
                    instance.get('ts_start'),
                    instance.get('ts_active')
                )
                result = roi.run()
                results.append([ts.total_seconds() for ts in result.get_delta_array()])
                details_results.append(result.get_details_array())

            filename = "%s.os_delays.csv" % self.name
            filepath = path.join(folder_path, filename)
            with open(filepath, "w") as f:
                w = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
                w.writerow(headers)
                w.writerows(results)
            logger.info("wrote: %s", filename)

            filename = "%s.os_details_delays.csv" % self.name
            filepath = path.join(folder_path, filename)
            with open(filepath, "w") as f:
                w = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
                w.writerow(details_headers)
                w.writerows(details_results)
            logger.info("wrote: %s", filename)




