import logging
import traceback
import re
from datetime import datetime

from paramiko import SSHClient, AutoAddPolicy


__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.2'
__email__ = 'tcarvalho@cmu.edu'


logger = logging.getLogger(__name__)


class OSInstanceProfile(object):

    Headers = [
        "receive", "unknown_setup", "scheduling", "controller_to_compute",
        "local_claim", "before_creating", "create_image", "lifecycle", "notify"
    ]

    DetailsHeaders = [
        "selected_hosts"
    ]

    def __init__(self, id, start, active):
        self.instance_id = id
        self.host = None

        # timestamps
        self.ts_start = start
        self.ts_active = active
        self.ts_received_at_controller = None
        self.ts_received_at_scheduler = None
        self.ts_selected_host = None
        self.ts_received_at_compute = None
        self.ts_reserved_local_resources = None
        self.ts_creating_image = None
        self.ts_started = None
        self.ts_resumed = None

        # other metrics
        self.selected_hosts = 0

    def get_receive_time(self):
        return self.ts_received_at_controller - self.ts_start

    def get_unknown_setup_time(self):
        return self.ts_received_at_scheduler - self.ts_received_at_controller

    def get_scheduling_time(self):
        return self.ts_selected_host - self.ts_received_at_scheduler

    def get_controller_to_compute_time(self):
        # TODO: could be negative due to clock desynch
        return self.ts_received_at_compute - self.ts_selected_host

    def get_local_claim_time(self):
        return self.ts_reserved_local_resources - self.ts_received_at_compute

    def get_time_before_creating(self):
        return self.ts_creating_image - self.ts_reserved_local_resources

    def get_creating_time(self):
        return self.ts_started - self.ts_creating_image

    def get_lifecycle_time(self):
        return self.ts_resumed - self.ts_started

    def get_time_to_notify(self):
        return self.ts_active - self.ts_resumed

    def __str__(self):
        s = "Instance %s @ %s" % (self.instance_id, self.host)
        if not self.ts_received_at_controller:
            return s + "\t...Not received at controller"
        s += "\n\tAt controller:    %s" % self.get_receive_time()

        if not self.ts_received_at_scheduler:
            return s + "\t...Not received by scheduler"
        s += "\n\tSome Setup:       %s" % self.get_unknown_setup_time()

        if not self.ts_selected_host:
            return s + "\t...Not scheduled"
        s += "\n\tScheduling:       %s" % self.get_scheduling_time()

        if not self.ts_received_at_compute:
            return s + "\t...Not received at compute"
        s += "\n\tTo compute:       %s" % self.get_controller_to_compute_time()

        if not self.ts_reserved_local_resources:
            return s + "\t...Not received at compute"
        s += "\n\tLocal claim:      %s" % self.get_local_claim_time()

        if not self.ts_creating_image:
            return s + "\t...Not Creating image"
        s += "\n\tBefore image:     %s" % self.get_time_before_creating()

        if not self.ts_started:
            return s + "\t...Not started"
        s += "\n\tImage creation:   %s" % self.get_creating_time()

        if not self.ts_resumed:
            return s + "\t...Not resumed"
        s += "\n\tShould be active: %s" % self.get_lifecycle_time()

        if not self.ts_active:
            return s + "\t...Not active"
        s += "\n\tNotify time:      %s" % self.get_time_to_notify()

        s += "\n\n\tTotal:            %s" % self.total_time()

        return s

    def total_time(self):
        return self.ts_active - self.ts_start

    def get_delta_array(self):
        return [
            self.get_receive_time(), self.get_unknown_setup_time(), self.get_scheduling_time(),
            self.get_controller_to_compute_time(), self.get_local_claim_time(), self.get_time_before_creating(),
            self.get_creating_time(), self.get_lifecycle_time(), self.get_time_to_notify()
        ]

    def get_details_array(self):
        return [
            self.selected_hosts
        ]

class Connection(object):

    def __init__(self, ip, username='lama'):
        self._ip = ip
        self._user = username
        self._conn = SSHClient()
        self._connected = False

        self._login(self._ip)

        self._init_req_id = None

    def _login(self, ip):
        self._conn.set_missing_host_key_policy(AutoAddPolicy())
        self._conn.connect(ip, username=self._user)
        self._connected = True

    def _exec_command(self, cmd):
        logger.debug("Executing @ %s: '%s'", self._ip, cmd)
        _, out, err = self._conn.exec_command(cmd)
        return out.read().decode('utf-8'), err.read().decode('utf-8')

    def _reliable_func(self, method, *args, **kwargs):
        try:
            return method(*args, **kwargs)
        except AttributeError:
            if not self._conn.get_transport():
                logger.error("No transport. Traceback: %s", traceback.format_exc())
            else:
                logger.error("Exception executing function. Traceback: %s", traceback.format_exc())
        except:
            logger.error("Exception executing function. Traceback: %s", traceback.format_exc())

    def exec(self, cmd):
        return self._reliable_func(self._exec_command, cmd)

    def close(self):
        self._conn.close()


class ResearchOSInstance(object):

    def __init__(self, instance_id, start, active):
        self._instance_id = instance_id
        self._connection_controller = Connection("10.1.1.40")
        self._connection_compute = None
        self._result = OSInstanceProfile(instance_id, start=start, active=active)

    def run(self):
        self._get_initial_req_id()
        self._selected_host()
        self._compute_times()

        self._connection_controller.close()
        if self._connection_compute:
            self._connection_compute.close()

        return self._result

    def _get_initial_req_id(self):
        out, err = self._connection_controller.exec("sudo grep %s /var/log/nova/nova-api.log" % self._instance_id)
        if err:
            logger.info("_received_at_controller:: got error on grep: %s", err)
        else:
            # logger.debug("Query by instance_id: %s", out)
            regex = re.compile('^(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d*) .*\[(req-[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}) .*%s' % self._instance_id,
                               flags=re.MULTILINE)
            matches = re.findall(regex, out)
            self._result.ts_received_at_controller = datetime.strptime(matches[0][0], "%Y-%m-%d %H:%M:%S.%f")
            logger.info("First seen at controller: %s", self._result.ts_received_at_controller)

            self._init_req_id = matches[0][1]
            logger.info("Maps to request ID: %s", self._init_req_id)

    def _selected_host(self):
        out, err = self._connection_controller.exec("sudo grep %s /var/log/nova/nova-scheduler.log" % self._init_req_id)
        if err:
            logger.info("_selected_host:: got error on grep: %s", err)
        else:
            # logger.debug("SCHEDULER Query by req-id: %s", out)
            regex = re.compile('^(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d*) .*%s' % self._init_req_id,
                               flags=re.MULTILINE)
            matches = re.findall(regex, out)
            self._result.ts_received_at_scheduler = datetime.strptime(matches[0], "%Y-%m-%d %H:%M:%S.%f")
            logger.info("First received at scheduler: %s", self._result.ts_received_at_scheduler)

            regex = re.compile('^(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d*) .*%s.*Selected host.*host: \((.*)\)' % self._init_req_id,
                               flags=re.MULTILINE)
            matches = re.findall(regex, out)
            logger.info("Selected host matches: %s", matches)
            self._result.selected_hosts = len(matches)

            self._result.ts_selected_host = datetime.strptime(matches[-1][0], "%Y-%m-%d %H:%M:%S.%f")
            host = matches[-1][1].split(",")[0]
            self._result.host = "10.1.1.%s" % int(host.split('-')[-1])
            logger.info("Selected host at scheduler: %s", self._result.ts_selected_host)
            logger.info("Host: %s", self._result.host)

    def _compute_times(self):
        self._connection_compute = Connection(self._result.host)

        out, err = self._connection_compute.exec("sudo grep %s /var/log/nova/nova-compute.log" % self._init_req_id)
        if err:
            logger.info("_compute_times:: got error on grep: %s", err)
        else:
            regex = re.compile('^(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d*) .*%s' % self._init_req_id,
                               flags=re.MULTILINE)
            matches = re.findall(regex, out)
            self._result.ts_received_at_compute = datetime.strptime(matches[0], "%Y-%m-%d %H:%M:%S.%f")
            logger.info("First received at compute: %s", self._result.ts_received_at_compute)

            regex = re.compile('^(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d*) .*%s.*Claim successful' % self._init_req_id,
                               flags=re.MULTILINE)
            matches = re.findall(regex, out)
            self._result.ts_reserved_local_resources = datetime.strptime(matches[0], "%Y-%m-%d %H:%M:%S.%f")
            logger.info("Locally claimed: %s", self._result.ts_reserved_local_resources)

            regex = re.compile('^(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d*) .*%s.*Creating image' % self._init_req_id,
                               flags=re.MULTILINE)
            matches = re.findall(regex, out)
            self._result.ts_creating_image = datetime.strptime(matches[0], "%Y-%m-%d %H:%M:%S.%f")
            logger.info("Creating image: %s", self._result.ts_creating_image)

        out, err = self._connection_compute.exec("sudo grep %s /var/log/nova/nova-compute.log" % self._instance_id)
        if err:
            logger.info("_compute_times:: got error on grep: %s", err)
        else:
            regex = re.compile('^(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d*) .*%s.*VM Started' % self._instance_id,
                               flags=re.MULTILINE)
            matches = re.findall(regex, out)

            # logger.info(out)
            self._result.ts_started = datetime.strptime(matches[0], "%Y-%m-%d %H:%M:%S.%f")

            regex = re.compile('^(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}.\d*) .*%s.*VM Resumed' % self._instance_id,
                               flags=re.MULTILINE)
            matches = re.findall(regex, out)
            self._result.ts_resumed = datetime.strptime(matches[0], "%Y-%m-%d %H:%M:%S.%f")
            logger.info("VM Started: %s", self._result.ts_started)
            logger.info("VM Resumed: %s", self._result.ts_resumed)


# connect to controller and get info by instance name


# how long in authentication?


# how long

def main():
    logger.setLevel(logging.DEBUG)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(
        logging.Formatter("%(asctime)s : %(levelname)5.5s : %(module)14.14s [%(lineno)3.3s] : %(message)s")
    )
    logger.addHandler(stream_handler)

    roi = ResearchOSInstance(
        '255af742-98e9-4629-9ee6-46c7f99f2f4d',
        datetime.strptime("2016-04-26 17:48:06.514046", "%Y-%m-%d %H:%M:%S.%f"),
        datetime.strptime("2016-04-26 17:49:44.331246", "%Y-%m-%d %H:%M:%S.%f")
    )
    result = roi.run()
    logger.info("Profile: %s", result)

    logger.info("Check total time: %s", sum(float(ts.total_seconds()) for ts in result.get_delta_array()))

if __name__ == "__main__":
    main()