from os import path, W_OK, access
from argparse import ArgumentTypeError

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.2'
__email__ = 'tcarvalho@cmu.edu'


class ArgTypes(object):

    @staticmethod
    def obj_list(l, obj_type):
        try:
            return [obj_type(v) for v in l.split(",")]
        except:
            raise ArgumentTypeError("Unable to parse as list of %ss: %s" % (obj_type.__name__, l))

    @staticmethod
    def int_list(l):
        # noinspection PyTypeChecker
        return ArgTypes.obj_list(l, int)

    @staticmethod
    def str_list(l):
        # noinspection PyTypeChecker
        return ArgTypes.obj_list(l, str)

    @staticmethod
    def writeable_filename(filepath):
        if not access(path.dirname(filepath), W_OK):
            raise ArgumentTypeError("Filename '%s' cannot be created!" % filepath)
        return filepath

    @staticmethod
    def writeable_folder(folder_path):
        if not access(path.dirname(folder_path), W_OK):
            raise ArgumentTypeError("Folder '%s' is not writeable!" % folder_path)
        return folder_path
