import logging
from threading import Thread
from flask import Flask, request, jsonify
from lama_exp.utils.json import EnhancedJSONDecoder, EnhancedJSONEncoder

__author__ = 'Tiago Carvalho'
__copyright__ = 'Copyright 2016, Tiago Carvalho'
__version__ = '0.1'
__email__ = 'tcarvalho@cmu.edu'

logger = logging.getLogger(__name__)


def listener_server(controller, port, processes=20):
    logger.info("Starting webserver!")

    app = Flask(__name__)
    app.json_encoder = EnhancedJSONEncoder
    app.json_decoder = EnhancedJSONDecoder

    def shutdown_server():
        func = request.environ.get('werkzeug.server.shutdown')
        if func is None:
            raise RuntimeError('Not running with the Werkzeug Server')
        func()

    @app.route("/")
    def hello():
        return "Hello World!"

    # @crossdomain(origin='*')
    @app.route('/exp', methods=['POST'])
    def exp_request():
        logger.debug("Received action request")
        action = request.get_json(silent=True)
        logger.debug("json_req: %s", action)
        res = controller.execute_action(**action)
        logger.debug("Result: %s", res)
        return jsonify(**res)

    @app.route('/results', methods=['POST'])
    def results():
        raise NotImplementedError()

    @app.route('/shutdown', methods=['POST'])
    def shutdown():
        shutdown_server()
        return 'Server shutting down...'

    app.run(host='0.0.0.0', port=port, processes=processes)


class Listener(Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition."""

    def __init__(self, controller, port):
        super().__init__(target=listener_server, args=(controller, port))

    def stop(self):
        pass
    #     func = request.environ.get('werkzeug.server.shutdown')
    #     if func is None:
    #         raise RuntimeError('Not running with the Werkzeug Server')
    #     func()

