DELAY_EXP=5
DELAY_REPORT_END=10
METRICS=rt # vcpu,vdisk

# reset lama
echo "Reset LAMA..."
#(cd ../lama/ ; python3 -m manage.manage_lama -a reset -f manage/lama_hosts.ini) &> logs/lama_reset.log
(cd ../lama/ ; python3 -m manage.manage_lama -a reset -f manage/lama_hosts.ini)

# start lama monitor:
# process monitor for: (1) provider, (2) app agent, (3) redis
# Possibly in the fututre: (*) collectd, ...
hosts_file='../lama/manage/lama_hosts.ini'
grep "^1" $hosts_file | while read -r line; do
    [[ "$line" =~ "^\;.*" ]] && continue
    IP=`echo $line | awk '{print $1;}'`
    echo "$IP : Launching lama_monitor"
    ssh lama@$IP "sudo killall lama_monitor" < /dev/null
    # connect to IP
    ssh lama@$IP "sudo nohup lama_monitor -m 'AllAppAgentProcMonitorPlugin;ProcByNameMonitorPlugin:redis-server' -i 10 -j /tmp/lama_monitor.json_events &>/var/log/lama/lama_monitor.log &" < /dev/null
done;

# start live report
echo "Start live report..."
python3 -m lama_exp.reports.scenario.live_profile multiple_rubbos --name multiple_rubbos --metrics $METRICS &> logs/lama_exp_live_profile.log &
REPORT_PID=$!
echo "Live report process pid: $REPORT_PID"

# run experiment
echo "Start experiment in $DELAY_EXP seconds..."
sleep ${DELAY_EXP}
date
echo "Start experiment..."
python3 -m lama_exp.experiments.monitoring -s etc/multiple_rubbos.yaml &> logs/lama_exp_multiple_rubbos.log
echo "Experiment done!"

echo "Terminating live report in $DELAY_REPORT_END seconds."
sleep ${DELAY_REPORT_END}
#echo "kill ${REPORT_PID}"
#kill ${REPORT_PID}
curl -X POST http://127.0.0.1:40900/shutdown
echo "Done!"

# stop lama_monitor
mkdir reports/last_lama_monitor/
grep "^1" $hosts_file | while read -r line; do
    IP=`echo $line | awk '{print $1;}'`
    echo "$IP : Get lama_monitor data"
    # connect to IP
    echo "   kill..."
    ssh lama@$IP "sudo killall lama_monitor" < /dev/null
    echo "   copy..."
    scp lama@$IP:/tmp/lama_monitor.json_events reports/last_lama_monitor/lama_monitor_$IP.json_events
    echo "   delete..."
    ssh lama@$IP "sudo rm /tmp/lama_monitor.json_events" < /dev/null
done;

