#!/usr/bin/env bash

rm -f /Users/tiago/PycharmProjects/lama_exp/run.log

if [ "$1" != "lama" ] && [ "$1" != "os" ]
then
    echo "Invalid FRAMEWORK option (options: lama, os): $1"
    exit 1
fi

FRAMEWORK="LAMA"
if [ "$1" != "os" ]
then
    FRAMEWORK="OpenStack"
fi

#for num_inst in 90
#for num_inst in 1 5
#for num_inst in 1 5 10 20 30 40 50 60 70 80 90
do
    if [ "$2" == "pause" ]
    then
        say -v Joana "Carregue em ENTER para continuar"
        read -p "Press ENTER to run next experiment (sequential: ${num_inst})"
    fi
    if [ "$1" == "lama" ]
    then
        echo "Resetting LAMA..."
        say "Reset LAMA"
        cd /Users/tiago/PycharmProjects/lama
        ./manage/manage_lama.py -a reset -f manage/lama_hosts.ini >> /Users/tiago/PycharmProjects/lama_exp/run.log
        for i in 1 2 3 4 6 8 9 13 14 16 17 18 19 26 27 31 38 41 42 44 45 49; do ssh lama@10.1.1.$i sudo pkill lama_exp; done
    fi
    sleep 3

    echo "Run sequential experiment for num of instances: $num_inst"
    say -v Joana "Início da experiência em sequência para $num_inst instâncias"
    cd /Users/tiago/PycharmProjects/lama_exp
    python3 -m lama_exp.exp -d hosts.ini -m $1 -s etc/sequential.yaml --vars defaults.n=$num_inst >> /Users/tiago/PycharmProjects/lama_exp/run.log
    python3 -m lama_exp.reports.provisioning.framework sequential_lama 1,5,10,20,30,40,50,60,70,80,90 --results-folder=results

    if [ "$2" == "pause" ]
    then
        say -v Joana "Carregue em ENTER para continuar"
        read -p "Press ENTER to run next experiment (parallel: ${num_inst})"
    fi
    if [ "$1" == "lama" ]
    then
        echo "Resetting LAMA..."
        say "Reset LAMA"
        cd /Users/tiago/PycharmProjects/lama
        ./manage/manage_lama.py -a reset -f manage/lama_hosts.ini >> /Users/tiago/PycharmProjects/lama_exp/run.log
        for i in 1 2 3 4 6 8 9 13 14 16 17 18 19 26 27 31 38 41 42 44 45 49; do ssh lama@10.1.1.$i sudo pkill lama_exp; done
    fi
    sleep 3

    echo "Run parallel experiment for num of instances: $num_inst"
    say -v Joana "Início da experiência em paralelo para $num_inst instâncias"
    cd /Users/tiago/PycharmProjects/lama_exp
    python3 -m lama_exp.exp -d hosts.ini -m $1 -s etc/parallel.yaml --vars defaults.n=$num_inst >> /Users/tiago/PycharmProjects/lama_exp/run.log
    python3 -m lama_exp.reports.provisioning.framework parallel_$1 1,5,10,20,30,40,50,60,70,80,90 --results-folder=results

    if [ "$2" == "pause" ]
    then
        say -v Joana "Carregue em ENTER para continuar"
        read -p "Press ENTER to run next experiment (parallel-multiuser: ${num_inst})"
    fi

    if [ "$1" == "lama" ]
    then
        echo "Resetting LAMA..."
        say "Reset LAMA"
        cd /Users/tiago/PycharmProjects/lama
        ./manage/manage_lama.py -a reset -f manage/lama_hosts.ini >> /Users/tiago/PycharmProjects/lama_exp/run.log
        for i in 1 2 3 4 6 8 9 13 14 16 17 18 19 26 27 31 38 41 42 44 45 49; do ssh lama@10.1.1.$i sudo pkill lama_exp; done
    fi
    sleep 3

    echo "Run parallel multiuser experiment for num of instances: $num_inst"
    say -v Joana "Início da experiência em paralelo para múltiplos utilizadores para $num_inst instâncias"
    cd /Users/tiago/PycharmProjects/lama_exp
    python3 -m lama_exp.exp -d hosts.ini -m $1 -s etc/parallel_multiuser.yaml --vars defaults.n=$num_inst,users.auto=$num_inst >> /Users/tiago/PycharmProjects/lama_exp/run.log
    python3 -m lama_exp.reports.provisioning.framework parallel_multiuser_$1 1,5,10,20,30,40,50,60,70,80,90 --results-folder=results

done

if [ "$1" != "lama" ]
then
    echo "Final clean up - Resetting LAMA..."
    cd /Users/tiago/PycharmProjects/lama
    ./manage/manage_lama.py -a reset -f manage/lama_hosts.ini >> /Users/tiago/PycharmProjects/lama_exp/run.log
fi

say -v Joana "Fim das experiências $FRAMEWORK"