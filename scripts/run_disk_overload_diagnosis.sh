DELAY_EXP=5
DELAY_REPORT_END=10
FAILURE=disk_overload_diagnosis
METRICS=rt,vcpu,vdisk,cpu,disk

$(workon lama_py3)
# reset lama
echo "Reset LAMA..."
#(cd ../lama/ ; python3 -m manage.manage_lama -a reset -f manage/lama_hosts.ini) &> logs/lama_reset.log
(cd ../lama/ ; python3 -m manage.manage_lama -a reset -f manage/lama_hosts.ini)

echo "Start LAMA Thanatos..."
#(cd ../lama/ ; python3 -m manage.manage_lama -a reset -f manage/lama_hosts.ini) &> logs/lama_reset.log
(cd ../lama/ ; python3 -m manage.manage -f manage/lama_hosts.ini thanatos restart)

# start live report
echo "Start live report..."
#python3 -m lama_exp.reports.scenario.live_profile rubbos_failure_$FAILURE --name rubbos_failure_$FAILURE --metrics rt,vcpu,vdisk &> logs/lama_exp_live_profile.log &
python3 -m lama_exp.reports.scenario.live_profile rubbos_failure_$FAILURE --name rubbos_failure_$FAILURE --metrics $METRICS &> logs/lama_exp_live_profile.log &
REPORT_PID=$!
echo "Live report process pid: $REPORT_PID"

# run experiment
echo "Start experiment in $DELAY_EXP seconds..."
sleep ${DELAY_EXP}
date
echo "Start experiment..."
python3 -m lama_exp.experiments.monitoring -s etc/rubbos_failure_$FAILURE.yaml &> logs/lama_exp_rubbos_failure_$FAILURE.log
echo "Experiment done!"

echo "Stop LAMA Thanatos..."
#(cd ../lama/ ; python3 -m manage.manage_lama -a reset -f manage/lama_hosts.ini) &> logs/lama_reset.log
(cd ../lama/ ; python3 -m manage.manage -f manage/lama_hosts.ini thanatos stop)

echo "Terminating live report in $DELAY_REPORT_END seconds."
sleep ${DELAY_REPORT_END}
#echo "kill ${REPORT_PID}"
#kill ${REPORT_PID}
curl -X POST http://127.0.0.1:40900/shutdown
echo "Done!"


