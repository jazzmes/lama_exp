from argparse import ArgumentParser, ArgumentTypeError
from math import ceil


def positive_int(value):
    try:
        val = int(value)
        if val <= 0:
            raise ArgumentTypeError("Value is not positive: {}".format(value))
        return val
    except Exception as e:
         raise ArgumentTypeError("Invalid value: {}".format(value))


def main():

    ap = ArgumentParser(description="Monitor for LAMA projects.")
    ap.add_argument('-n', type=int, help='Number of apps to create.', required=True)
    ap.add_argument('-d', dest='delay', type=int, default=1,
                    help='Delay (in seconds) between finish one app deployment and the start of the next app.')
    ap.add_argument('-f', dest='filename', type=str, help='Filename to write confiugration to.', default=None)
    ap.add_argument('-p', dest='parallel', action='store_true',
                    help='Parallel launch instead of sequential (delay applies only to the begining).')
    ap.add_argument('-g', dest='group_size', type=positive_int, default=1,
                    help='Number of instances in parallel (launch at the same time). '
                         'Only works for parallel option and groups are separated according to the specified delay. ')
    args = ap.parse_args()

    if not args.filename:
        args.filename = "multiple_rubbos_{:03d}_{}.yaml".format(args.n, 'par' if args.parallel else 'seq')

    with open(args.filename, "w") as f:
        f.write("duration: {}\n".format(
            (ceil(args.n / args.group_size) * (args.group_size * 120 + args.delay)) if args.parallel else (args.n * (200 + args.delay)))
        )
        f.write("options:\n")
        f.write("  duration_strict: true\n")
        f.write("users:\n")
        f.write("  user1:\n")
        f.write("    lama:\n")
        f.write("      username: rubbos_01\n")
        f.write("defaults:\n")
        f.write("  user: user1\n")
        f.write("  flavor: m1.tiny\n")
        f.write("  size: m1.tiny\n")
        f.write("  secgroup: default\n")
        f.write("  network: private\n")
        f.write("events:\n")
        last_dependency = []
        next_dependencies = []
        for i in range(1, args.n + 1):
            f.write("- time: {}\n".format(args.delay))
            f.write("  actions:\n")
            f.write("    - cmd: create_app\n")
            f.write("      app_name: rubbos-{:03d}\n".format(i))
            f.write("      app_type: rubbos-mysql\n")
            if last_dependency and args.parallel:
                f.write("  dependencies:\n")
                for j in last_dependency:
                    f.write("    - active_app:\n")
                    f.write("        app_name: rubbos-{:03d}\n".format(j))
            if args.parallel:
                next_dependencies.append(i)
                if i % args.group_size == 0:
                    last_dependency = next_dependencies
                    next_dependencies = []


if __name__ == '__main__':
    main()
