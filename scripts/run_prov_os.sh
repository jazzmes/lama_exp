#!/usr/bin/env bash

rm -f /Users/tiago/PycharmProjects/lama_exp/run.log

for num_inst in 1 5 10 20 30 40 50 60 70 80 90
#for num_inst in 40 50 60 70 80 90
do
    echo "Run experiment for num of instances: $num_inst"
    say -v Joana "Início da experiência em sequência para $num_inst instâncias"
    cd /Users/tiago/PycharmProjects/lama_exp
    python3 -m lama_exp.exp -d hosts.ini -m os -s etc/sequential.yaml --vars defaults.n=$num_inst >> /Users/tiago/PycharmProjects/lama_exp/run.log
    sleep 5

    echo "Run experiment for num of instances: $num_inst"
    say -v Joana "Início da experiência em paralelo para $num_inst instâncias"
    cd /Users/tiago/PycharmProjects/lama_exp
    python3 -m lama_exp.exp -d hosts.ini -m os -s etc/parallel.yaml --vars defaults.n=$num_inst >> /Users/tiago/PycharmProjects/lama_exp/run.log
    sleep 5

    echo "Run experiment for num of instances: $num_inst"
    say -v Joana "Início da experiência em paralelo para múltiplos utilizadores para $num_inst instâncias"
    cd /Users/tiago/PycharmProjects/lama_exp
    python3 -m lama_exp.exp -d hosts.ini -m os -s etc/parallel_multiuser.yaml --vars defaults.n=$num_inst,users.auto=$num_inst >> /Users/tiago/PycharmProjects/lama_exp/run.log
    sleep 5
done

say -v Joana "Fim das experiências OpenStack"