#!/bin/bash

while [[ $# -gt 1 ]]
do
    key="$1"

    case $key in
        -e|--exp)
        EXP_NAME="$2"
        shift # past argument
        ;;
        -d|--delay)
        DELAY_EXP="$2"
        shift # past argument
        ;;
        -r|--report_delay)
        DELAY_REPORT="$2"
        shift # past argument
        ;;
        -m|--metrics)
        METRICS="${2}"
        shift # past argument
        ;;
        -y|--yaml_file)
        YAML_FILE="${2}"
        shift # past argument
        ;;
        *)
        # unknown option
        ;;
    esac
    shift # past argument or value
done

# default values
DELAY_EXP=${DELAY_EXP:-5}
DELAY_REPORT=${DELAY_REPORT:-10}
METRICS=${METRICS:-rt} # rt,vcpu,vdisk,cpu,disk
YAML_FILE=${YAML_FILE:-etc/$EXP_NAME.yaml}

if [ -z ${EXP_NAME} ]
then
    echo "Please set the experiment name (-e)"
    exit
fi

if [ ! -f $YAML_FILE ]; then
    echo "YAML file not found: $YAML_FILE"
    exit
fi

echo ""
echo "Run Experiment:"
echo -e "\tName         = ${EXP_NAME}"
echo -e "\tYAML File    = ${YAML_FILE}"
echo -e "\tDelay Exp    = ${DELAY_EXP}"
echo -e "\tDelay Report = ${DELAY_REPORT}"
echo -e "\tMetrics      = ${METRICS}"
echo ""


#workon lama_exp_py3
# reset lama
echo "Reset LAMA..."
(cd ../lama/ ; python3 -m manage.manage_lama -a reset -f manage/lama_hosts.ini) &> logs/lama_exp_setup.log

echo "Start LAMA Thanatos..."
(cd ../lama/ ; python3 -m manage.manage -f manage/lama_hosts.ini thanatos restart) &> logs/lama_exp_setup.log

# start live report
echo "Start live report..."
python3 -m lama_exp.reports.scenario.live_profile $EXP_NAME --name $EXP_NAME --metrics $METRICS &> logs/lama_exp_live_profile.log &
REPORT_PID=$!
echo "Live report process pid: $REPORT_PID"

# run experiment
echo "Start experiment in $DELAY_EXP seconds..."
sleep ${DELAY_EXP}
date
echo "Start experiment..."
python3 -m lama_exp.experiments.monitoring -s $YAML_FILE &> logs/lama_exp_$EXP_NAME.log
echo "Experiment done!"

echo "Stop LAMA Thanatos..."
(cd ../lama/ ; python3 -m manage.manage -f manage/lama_hosts.ini thanatos stop) &> logs/lama_exp_setup.log

echo "Terminating live report in $DELAY_REPORT seconds."
sleep ${DELAY_REPORT}
#echo "kill ${REPORT_PID}"
#kill ${REPORT_PID}
curl -X POST http://127.0.0.1:40900/shutdown
date
echo "Done!"


