#!/usr/bin/env bash

N=100

# DO NOT RUN THIS SCRIPT TWICE WITHOUT REMOVING PREVIOUS INSERTIONS
# Non-safe: network and router creation

for num_user in `seq 1 $N`
do
    USER=lama_$num_user
    PROJ=lama_$num_user
    PASS=l@ma_$USER

    cmd="openstack project create --domain default --description 'Lama $USER Project' $PROJ"
    echo "$cmd ... "
    eval $cmd
    echo "$?"

    cmd="openstack user create --domain default --password $PASS $USER"
    echo "$cmd ... "
    eval $cmd
    echo "$?"

    cmd="openstack user set --project $PROJ $USER"
    echo "$cmd ... "
    eval $cmd
    echo "$?"

    cmd="openstack role add --project $PROJ --user $USER user"
    echo "$cmd ... "
    eval $cmd
    echo "$?"
done

for num_user in `seq 1 $N`
do
    USER=lama_$num_user
    PROJ=lama_$num_user
    PASS=l@ma_$USER

    export OS_PROJECT_DOMAIN_ID=default
    export OS_USER_DOMAIN_ID=default
    export OS_PROJECT_NAME=$PROJ
    export OS_TENANT_NAME=$PROJ
    export OS_USERNAME=$USER
    export OS_PASSWORD=$PASS
    export OS_AUTH_URL=http://controller:35357/v3
    export OS_IDENTITY_API_VERSION=3

    # create a private network
    cmd="neutron net-create private"
    echo "$cmd ... "
    eval $cmd
    echo "$?"

    cmd="neutron subnet-create private 192.168.$num_user.0/24 --name private --dns-nameserver 10.1.0.3 --gateway 192.168.$num_user.1"
    echo "$cmd ... "
    eval $cmd
    echo "$?"

    cmd="neutron router-create router"
    echo "$cmd ... "
    eval $cmd
    echo "$?"

    cmd="neutron router-interface-add router private"
    echo "$cmd ... "
    eval $cmd
    echo "$?"

    # create the key pair
    cmd="nova keypair-add --pub-key ~/.ssh/lama_rsa.pub $USER_key"
    echo "$cmd ... "
    eval $cmd
    echo "$?"
done
